<?php

namespace Mayral\Classes\Autoloader;

class Autoloader
{

    public static function Register(Handler $_handler)
    {
        spl_autoload_register($_handler->GetHandler());
    }

}
