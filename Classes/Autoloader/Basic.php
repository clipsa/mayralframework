<?php

namespace Mayral\Classes\Autoloader;

class Basic extends Handler
{

    public function Load($_className)
    {
        $_className=str_replace('Mayral\\', '', $_className);
        $ClassFullPath=__DIR__.'/../../'.str_replace('\\', '/', $_className).'.php';
        if(file_exists($ClassFullPath))
        {
            require_once($ClassFullPath);
        }
        else
            
        {
            echo "";
        }
    }

}
