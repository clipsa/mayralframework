<?php

namespace Mayral\Classes\Autoloader;

abstract class Handler
{

    final public function GetHandler()
    {
        return array($this, 'Load');
    }

    abstract public function Load($_className);
}