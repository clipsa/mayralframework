<?php

/* * **************************************************************************
  Description: базовый класс от которого должны быть унаследованны все классы mayral,
  которые должны иметь возможность хранить данные в хранилище объектов
  Author: Ivanov Kirill
  Created: 08.11.2007
  Version: 1.1.0

  Changes author: -
  Changes info:
  = 08.11.2007    создан
  + 22.06.2009    (Ivanov Kirill):	Kill(), KillMark, MarkToKill()
  - 23.06.2009    (Ivanov Kirill):	-Kill() =)
  + 02.07.2009    (Ivanov Kirill):	ProcessGlobalEvent, SendGlobalEvent
  + 04.02.2010    (Ivanov Kirill):	IsChanged
  = 17.06.2012    (Zinchenko Sergey): появился родитель OMayralObject
  - 18.08.2012    (Zinchenko Sergey): ProcessGlobalEvent
 * ************************************************************************** */

namespace Mayral\Classes\Basic;

class BasicClass extends MayralObject
{

    public $Name;
    public $Parent;
    public $IsChanged = false;

    public function __construct($_name, $_parent)
    {
        $this->Name = $_name;
        $this->Parent = $_parent;
    }

    public function RemoveChild($_component_name)
    {
        if (isset($this->$_component_name))
        {
            unset($this->$_component_name);
        }
        if (isset($this->GetParentForm()->$_component_name))
        {
            unset($this->GetParentForm()->$_component_name);
        }
    }

    /*
      функция возвращает уникальное (полное) имя объекта
      первоначально планировалось, что это имя это список всех родительский объектов через "_"
      теперь это имя это уникальный идентификатор, который выдается хранилищем объектов
     */

    public function FullName()
    {
        return $this->ClientName;
    }

    public function FirstParent()
    {
        $result = $this;
        if ($this->Parent != null)
        {
            $result->Parent->FirstParent();
        }
        return $result;
    }

    public function ClientAccessible()
    {
        return true;
    }

    /*
      функция сообщает системе о событии
     */

    public function SendGlobalEvent($_event_name, $_event_argument = '')
    {
        return \Mayral\Classes\Basic\ObjectsPool::Create()->ProcessGlobalEvent($_event_name, $_event_argument);
    }

}

?>