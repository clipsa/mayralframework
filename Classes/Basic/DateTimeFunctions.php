<?php

namespace Mayral\Classes\Basic;

class DateTimeFunctions
{

    public static function DatetimeMysqlToPhp($_mysql_date_time, $_format="d.m.Y")
    {
        $Resule=strtotime($_mysql_date_time);
        if($_format!=="")
        {
            $Resule=date($_format, $Resule);
        }
        return $Resule;
    }

    public static function DatetimePhpToMysql($_date_time)
    {
        $Result=strtotime($_date_time);
        $Result=date('Y-m-d H:i:s', $Result);
        return $Result;
    }

}

?>