<?php

/* * **************************************************************************
  Description: базовый класс базового объекта
  Author: Zinchenko Sergey
  Created: 17.06.2012
  Version: 1.0.0

  Changes info:
  = 17.06.2012	(Zinchenko Sergey):	создан
 * ************************************************************************** */

namespace Mayral\Classes\Basic;

class MayralObject
{

    public $ClientName = '';
    public $KillMark = false;

    public function MarkToKill()
    {
        $this->KillMark = true;
    }

    public function AddToObjectsPool()
    {
        ObjectsPool::Create()->Add($this);
    }

    /*
      функция возвращает уникальное (полное) имя объекта
      первоначально планировалось, что это имя это список всех родительский объектов через '_'
      теперь это имя это уникальный идентификатор, который выдается хранилищем объектов
     */

    public function FullName()
    {
        return $this->ClientName != '' ? $this->ClientName : get_class();
    }

    /*
      функция вызывается перед обработкой рабочего пространства
      используется например в соединениях
     */

    public function Refresh()
    {
        
    }

}

?>