<?php

/* * **************************************************************************
  Description: хранилище объектов
  Author: Ivanov Kirill
  Created: 08.11.2007
  Version: 1.1.0

  Changes info:
  = 08.11.2007	(Ivanov Kirill):	создан
  + 09.07.2008	(Ivanov Kirill):	GetObjectFromObjectsPool
  + 04.02.2010:	(Ivanov Kirill):	изменен принцип работы с измененными компонентами
 * ************************************************************************** */

namespace Mayral\Classes\Basic;

class ObjectsPool
{

    protected $ObjectCounter = 0;
    public $Objects;
    public $GlobalListener;

    /*
      Конструктор хранилища
     */

    private function __construct()
    {
        $this->Objects = array();
        $this->GlobalListener=new \Mayral\Classes\Listeners\GlobalListener();
    }

    public static function Create($_name = 'ObjectsPool')
    {
        if (!isset($_SESSION[$_name]))
        {
            $_SESSION[$_name] = new ObjectsPool();
        }
        return $_SESSION[$_name];
    }

    /*
      Функция проверяет наличие объекта в хранилище по его имени
     */

    public function IsExist($Name)
    {
        return isset($this->Objects[$Name]);
    }

    /*
      Функция возвращает объект из хранилища по его имени, если он присутствует
     */

    public function Object($Name)
    {
        if ($this->IsExist($Name))
        {
            return $this->Objects[$Name];
        }

        return null;
    }

    /*
      Функция возвращает объект из хранилища по имени передаваемого объекта
      Используется для следующей конструкции
      $object = new OTestObject;
      $object = ReplaceObjectIfExist($object);

      пока не стоит пользовать
     */

    public function ReplaceObjectIfExist($object)
    {
        $Name = $object->Name;
        if ($this->IsExist())
        {
            return $this->Objects[$Name];
        }
        return $object;
    }

    /*
      Функция добавляет объект в хранилище.
      при это объекту присваивается уникальный идентификатор ClientName
     */

    public function Add($object)
    {
        $this->ObjectCounter++;
        $object->ClientName = 'c' . $this->ObjectCounter;
        $objectFullName = $object->FullName();
        if (!$this->IsExist($objectFullName))
        {
            $this->Objects[$objectFullName] = $object;

            if ($object instanceof \Mayral\Classes\Interfaces\IActionListener)
            {
                $this->GlobalListener->AddListener($object);
            }
        }
    }

    public function AddToChangedComponents($_object)
    {
        $_object->IsChanged = true;
    }

    public function IsExistInObjectsPool($Name)
    {
        return $this->IsExist($Name);
    }

    public function GetObjectFromObjectsPool($_name)
    {
        return $this->Object($_name);
    }

    public function Delete($Name)
    {
        if ($this->IsExist($Name))
        {
            $Object = $this->Objects[$Name];

            if ($Object instanceof \Mayral\Classes\Interfaces\IActionListener)
            {
                $this->GlobalListener->RemoveListener($Object);
            }

            if ($Object->Parent !== '' && !is_null($Object->Parent))
            {
                $Object->Parent->RemoveChild($Object->Name);

                $this->DeleteByParent($Object);
            }
            unset($this->Objects[$Name]);
        }
    }

    public function DeleteMarkedToKill()
    {
        foreach ($this->Objects as $key => $Object)
        {
            if (isset($Object->KillMark) && $Object->KillMark)
            {
                $this->Delete($Object->FullName());
            }
        }
        $this->GlobalListener->DeleteMarkedToKill();
    }

    protected function DeleteByParent($_parent_component)
    {
        foreach ($this->Objects as $key => $Object)
        {
            if ($Object instanceof \Mayral\Classes\Interfaces\IComponent &&
                    isset($Object->Parent) && $Object->Parent !== '' &&
                    $Object->Parent === $_parent_component)
            {
                $this->Delete($Object->FullName());
            }
        }
    }

    protected function SetIsChangedByParent($_parent_component, $_value)
    {
        foreach ($this->Objects as $key => $Object)
        {
            if ($Object instanceof \Mayral\Classes\Interfaces\IComponent && isset($Object->Parent) && $Object->Parent !== '' && $Object->Parent === $_parent_component)
            {
                $Object->IsChanged = $_value;
                $this->SetIsChangedByParent($Object, $_value);
            }
        }
    }

    public function OptimizeChanges()
    {
        foreach ($this->Objects as $key => $Object)
        {
            if ($Object instanceof \Mayral\Classes\Interfaces\IComponent && $Object->IsChanged)
            {
                $this->SetIsChangedByParent($Object, false);
            }
        }
    }

    public function Refresh()
    {
        foreach ($this->Objects as $key => $Object)
        {
            $Object->Refresh();
        }
    }

    public function ProcessGlobalEvent($_event_name, $_event_argument)
    {
        return $this->GlobalListener->ProcessGlobalEvent($_event_name, $_event_argument);
    }

}

?>