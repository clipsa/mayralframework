<?php

/* * **************************************************************************
  Description: класс для работы со строками
  Author: Ivanov Kirill
  Created: 08.11.2007
  Version: 1.0.7

  Changes info:
  = 08.11.2007	(Ivanov Kirill):	создан
  = 09.07.2008	(Ivanov Kirill):	Replace
  + 11.07.2008	(Ivanov Kirill):	AddStringWithSeparator - добавляет к строке символы и вставляет перед ними разделитель, если строка была не пустая
  + 03.06.2009	(Ivanov Kirill):	TemplateReplace
  + 12.01.2010	(Ivanov Kirill):	WriteToFile теперь возвращает результат. (true/false)
 * ************************************************************************** */

namespace Mayral\Classes\Basic;

class String extends BasicClass
{

    public $Text;

    public function __construct($_parent, $_text = "")
    {
        parent::__construct("", $_parent);
        $this->Text = $_text;
    }

    // замена подстроки

    public function Replace($_find, $_replace)
    {
        $this->Text = str_replace($_find, $_replace, $this->Text);
    }

    public function GetChangeTextBetween($item_start_text, $item_end_text, $new_text)
    {
        $result = $this->GetTextBetween($item_start_text, $item_end_text);
        $this->Text = str_replace($item_start_text . $result . $item_end_text, $new_text, $this->Text);

        return $result;
    }

    public function CutTextBetween($item_start_text, $item_end_text)
    {
        $result = $this->GetTextBetween($item_start_text, $item_end_text);
        $this->DeleteSubstring($item_start_text . $result . $item_end_text);

        return $result;
    }

    public function GetTextBetween($item_start_text, $item_end_text)
    {
        $s = $this->Text;

        $result = "";

        $item_start_pos = strpos($s, $item_start_text);
        $item_end_pos = strpos($s, $item_end_text, $item_start_pos);

        if ($item_start_pos !== false && $item_end_pos !== false)
        {
            $item_start_pos = $item_start_pos + strlen($item_start_text);
            $result = substr($s, $item_start_pos, $item_end_pos - $item_start_pos);
        }
        return $result;
    }

    public function DeleteSubstring($substring)
    {
        $this->Text = str_replace($substring, "", $this->Text);
    }

    public function LoadFromFile($file_name)
    {
        if (file_exists($file_name))
        {
            $template_file = fopen($file_name, "r");
            $result = fread($template_file, filesize($file_name));
            fclose($template_file);

            $this->Text = $result;
        }
    }

    public function WriteToFile($file_name)
    {
        $template_file = fopen($file_name, "w");
        if ($template_file && fwrite($template_file, $this->Text))
        {
            fclose($template_file);
            return true;
        }
        return false;
    }

    /*
      функция добавляет к строке символы $_string
      и вставляет перед ними $_separator, если начальная строка была не пустой
     */

    public function AddStringWithSeparator($_string, $_separator)
    {
        if ($this->Text != "")
        {
            $this->Text = $this->Text . $_separator;
        }
        $this->Text = $this->Text . $_string;
    }

    public function TemplateReplace($_items)
    {
        foreach ($_items as $key => $value)
        {
            $TemplateText = "{" . $key . "}";
            $this->Replace($TemplateText, $value);
        }
    }

}

?>