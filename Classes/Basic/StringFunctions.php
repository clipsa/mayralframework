<?php

namespace Mayral\Classes\Basic;

class StringFunctions
{

    public static function rus2translit($string)
    {
        $converter = array(
            'а' => 'a', 'б' => 'b', 'в' => 'v',
            'г' => 'g', 'д' => 'd', 'е' => 'e',
            'ё' => 'e', 'ж' => 'zh', 'з' => 'z',
            'и' => 'i', 'й' => 'y', 'к' => 'k',
            'л' => 'l', 'м' => 'm', 'н' => 'n',
            'о' => 'o', 'п' => 'p', 'р' => 'r',
            'с' => 's', 'т' => 't', 'у' => 'u',
            'ф' => 'f', 'х' => 'h', 'ц' => 'c',
            'ч' => 'ch', 'ш' => 'sh', 'щ' => 'sch',
            'ь' => '', 'ы' => 'y', 'ъ' => '',
            'э' => 'e', 'ю' => 'yu', 'я' => 'ya',
            ' ' => '_', '/' => '', '\\' => '',
            '\'' => '', '"' => '',
            'А' => 'a', 'Б' => 'b', 'В' => 'v',
            'Г' => 'g', 'Д' => 'd', 'Е' => 'e',
            'Ё' => 'e', 'Ж' => 'zh', 'З' => 'z',
            'И' => 'i', 'Й' => 'y', 'К' => 'k',
            'Л' => 'l', 'М' => 'm', 'Н' => 'n',
            'О' => 'o', 'П' => 'p', 'Р' => 'r',
            'С' => 's', 'Т' => 't', 'У' => 'u',
            'Ф' => 'f', 'Х' => 'h', 'Ц' => 'c',
            'Ч' => 'ch', 'Ш' => 'sh', 'Щ' => 'sch',
            'Ь' => '', 'Ы' => 'y', 'Ъ' => '',
            'Э' => 'e', 'Ю' => 'yu', 'Я' => 'ya'
        );
        return strtr($string, $converter);
    }

    public static function encodestring($str)
    {

        // в нижний регистр
        $str = mb_strtolower($str, "utf-8");
        // переводим в транслит
        $str = self::rus2translit($str);
        // удаляем все ненужное
        $str = preg_replace('~[^a-z0-9_]+~', '', $str);
        // удаляем начальные и конечные '-'
        $str = trim($str, " -_");
        return $str;
    }

    public static function json_to_object($_json_str, $_object_victim)
    {
        $result = $_object_victim;

        $result_arr = json_decode($_json_str);
        $vars = get_object_vars($result_arr);
        //for($i=0; $i<count($vars); $i++)
        foreach ($vars as $name => $data)
        {
            $mname = 'Set' . $name;
            if (
                    is_array($result_arr) && isset($result_arr[$name]) && method_exists($result, $mname) ||
                    is_object($result_arr) && isset($result_arr->$name) && method_exists($result, $mname)
            )
            {
                $result->$mname($data);
            }
        }

        return $result;
    }

    public static function PrepareForQuery($_string)
    {
        $result = $_string;

        $result = htmlspecialchars($result);
        $result = addslashes($result);

        return $result;
    }

    public static function GetHash($_string, $_iterations = 3)
    {
        $result = $_string;

        for ($i = 0; $i < $_iterations; $i++)
        {
            $result = md5($result);
        }

        return $result;
    }

    public static function GetEnding($_count)
    {
        $result = "";

        if (preg_match("/^\d*1\d{1}$/", $_count))
        {
            $result = "ов";
        }
        else
        {
            if (preg_match("/^\d*1$/", $_count))
            {
                $result = "";
            }
            elseif (preg_match("/^\d*[2|3|4]$/", $_count))
            {
                $result = "а";
            }
            else
            {
                $result = "ов";
            }
        }

        return $result;
    }

}

?>