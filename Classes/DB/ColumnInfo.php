<?php

namespace Mayral\Classes\DB;

class ColumnInfo extends \Mayral\Classes\Basic\BasicClass
{

    public $Index;
    public $Meta;

    public function __construct($_name, $_parent, $_index, $_meta=null)
    {
        $this->Index=$_index;
        if(isset($_meta))
        {
            $this->Meta=$_meta;
        }
        parent::__construct($_name, $_parent);
    }

}

?>