<?php

/* * **************************************************************************
  Description: класс хранящий данные о соединении с базой данных,
  можно передавать все параметры в виде строки, аналогично синтаксису get - разделением &

  Author: kecheor
  Created: 10.11.2008
  Version: 1.0.0

  Changes author: -
  Changes info:
  = 08.11.2007:	создан
 * ************************************************************************** */

namespace Mayral\Classes\DB;

abstract class DBConnection extends \Mayral\Classes\Basic\BasicClass
{

    //адрес SQL сервера
    var $Host;
    //Логин
    var $Login;
    //Пароль
    var $Pass;
    //Имя базы
    var $DBName;
    //Префикс
    var $DBPrefix;
    //
    var $ConnectionString;
    var $Connection;
    public $Connected=false;

    //Конструктор
    function __construct($_name, $_parent, $DBHost='', $DBLogin='', $DBPass='', $DBName='', $DBPrefix='')
    {
        parent::__construct($_name, $_parent);

        $this->Host=$DBHost;
        $this->Login=$DBLogin;
        $this->Pass=$DBPass;
        $this->DBName=$DBName;
        $this->DBPrefix=$DBPrefix;

        $this->AddToObjectsPool();
    }

    public function Refresh()
    {
        if(!$this->Connected)
        {
            $this->sql_connect();
            $this->Connected=true;
        }
    }

    abstract function sql_connect();

    abstract function sql_attected_rows();

    abstract function sql_error();

    abstract function sql_escape_string($s);

    abstract function sql_fetch_array($query_result, $t=MYSQL_BOTH);

    abstract function sql_fetch_field($query_result, $i);

    abstract function sql_field_name($query_result, $i);

    abstract function sql_field_type($query_result, $i);

    abstract function sql_free_result($query_result);

    abstract function sql_insert_id($query_result=null);

    abstract function sql_num_rows($query_result);

    abstract function sql_num_fields($query_result);

    abstract function sql_result($query_result, $r, $f);

    abstract function sql_query($query_result, $l=false);

    abstract function sql_select_db($db_name);

    abstract function sql_close();

    abstract function sql_data_seek($_result, $_row_num);
}

?>