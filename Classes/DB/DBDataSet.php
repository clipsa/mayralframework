<?php

/* * **************************************************************************
  Description: класс для DataSet заполняемого из базы данных

  Author: kecheor
  Created: 11.11.2008
  Version: 1.0.1

  Changes author: -
  Changes info:
  = 11.11.2008:	создан
  = 18.11.2008:   флаги об изменении теперь глобальны для все строки, для удобства обработки новых и удаленных строк
 * ************************************************************************** */

namespace Mayral\Classes\DB;

class DBDataSet extends TitledDataSet
{

    public $Connection;
    public $TableInfo;
    public $SQLText;
    public $StartRowIndex;
    public $RowsInResult;
    protected $QueryResult;

    public function __construct($_name, $_parent, $_connection)
    {
        $this->Connection = $_connection;
        $this->TableInfo = new \Mayral\Classes\Lists\NamedItemList('TableInfo', $this);
        parent::__construct($_name, $_parent);

        $this->StartRowIndex = 0;
    }

    //Выполняет запрос к базе данных по SQLText
    public function Open($q = '')
    {
        if ($q != '')
        {
            $this->SQLText = $q;
        }
        $this->PrepareQuery();
        $this->QueryResult = $this->Connection->sql_query($this->SQLText);
        if ($this->QueryResult !== false)
        {
            $this->RowCount = $this->Connection->sql_num_rows($this->QueryResult);
            $this->ColCount = $this->Connection->sql_num_fields($this->QueryResult);
            $this->CreateFieldsInfo();
            $this->FillTable();
            $this->FirstRow();
            return true;
        }
        else
        {
            return false;
        }
    }

    public function Close()
    {
        //	надо добавить закрытие и освобождение ресурсов наверное
    }

    //Заполняет информацию о столбцах и таблицах из запроса
    protected function CreateFieldsInfo()
    {
        for ($i = 0; $i < $this->ColCount; $i++)
        {
            $field_meta = $this->Connection->sql_fetch_field($this->QueryResult, $i);
            $FieldName = $field_meta->name;
            if ($field_meta->table != "")
            {
                $FieldName = $field_meta->table . '.' . $FieldName;
            }
            $field_info = new ColumnInfo($FieldName, $this->ColumnInfo, $i, $field_meta);
            $this->ColumnInfo->Add($field_info);
            if ($this->TableInfo->ItemByName($field_meta->table) === false)
            {
                $table = new TableInfo($field_meta->table, $this);
                $this->TableInfo->Add($table);
            }
        }
    }

    //Заполняет Table из результатов запроса
    protected function FillTable()
    {
        $row_cnt = $this->StartRowIndex;
        if ($row_cnt > 0)
        {
            $this->Connection->sql_data_seek($this->QueryResult, $row_cnt);
        }
        while (($row = $this->Connection->sql_fetch_array($this->QueryResult, MYSQL_BOTH)) && ($this->RowsInResult > 0 || is_null($this->RowsInResult)))
        {
            $this->Table[] = $row;
            $row_cnt++;
            $this->RowsInResult--;
        }
    }

    //Вызывается перед исполнением запроса для форматирования строки запроса
    protected function PrepareQuery()
    {
        
    }

    //Возвращает значение из таблицы, добавлено автоматическое добавление имени таблицы, если запрос 
    //только по одной таблице
    public function GetFieldValue($col, $row = -1)
    {
        $result = null;

        if (isset($this->Table[$row == -1 ? $this->CurrentRow : $row][$col]))
        {
            $result = $this->Table[$row == -1 ? $this->CurrentRow : $row][$col];
        }
        else
        {
            $result = parent::GetFieldValue($col, $row);
        }

        return $result;
    }

    public function GetColumnByName($col)
    {
        return parent::GetColumnByName($col);
    }

    //Записывает значение в ячейку таблицы, добавлено автоматическое добавление имени таблицы, если запрос 
    //только по одной таблице
    public function SetFieldValue($value, $col, $row = -1)
    {
        return parent::SetFieldValue($value, $col, $row);
    }

    //Удаляет строку из таблицы и переиндексирует ее, если $row не указан, то берется текущий
    //сохраняет копию строки в DeletedRows для последующего сохранения изменений
    public function DeleteRow($row = -1)
    {
        if ($row == -1)
        {
            $row = $this->CurrentRow;
        }
        else
        {
            if ($row > $this->RowCount)
            {
                return false;
            }
        }
        $DeletedRows[] = $this->Table[$row];
        unset($this->Table[$row]);
        $this->Table = array_values($this->Table);
        return true;
    }

    public static function GetRESULT($_query_text)
    {
        return self::GetQueryFieldValue($_query_text, 0);
    }

    public static function GetQueryFieldValue($_query_text, $_field_name)
    {
        $Result = "";
        $ds = new DBDataSet('', '', SQLConnection::GetInstance());
        $ds->Open($_query_text);
        if ($ds->RowCount > 0)
        {
            $Result = $ds->GetFieldValue($_field_name);
        }

        return $Result;
    }

    public function Row($_index = -1)
    {
        if ($_index == -1)
        {
            $_index = $this->CurrentRow;
        }
        $result = $this->Table[$_index];
        return $result;
    }

}

?>