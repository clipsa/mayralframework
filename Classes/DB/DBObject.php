<?php

/* * **************************************************************************
  Description: объект базы данных
  Version: 1.1.1

  Changes info:
  = 03.06.2009	(Ivanov Kirill):	создан
  = 03.12.2011	(Zinchenko Sergey): GlobalActionAlias - происходит глобальное событие
  + 15.01.2013	(Zinchenko Sergey): Delete();
  + 25.02.2013	(Potapenko Vladimir): в Write() снятие кавычек у NULL значений в запросе
  + 07.11.2013	(Zinchenko Sergey): SetPropertyValue - добавление/изменение значений только через метод
  GetProperty()
  + 25.11.2013	(Zinchenko Sergey): Copy()
 * ************************************************************************** */

namespace Mayral\Classes\DB;

class DBObject extends \Mayral\Classes\Basic\BasicClass
{

    protected $ID=0;
    private $Property;
    public $SelectQueryAlias;
    public $UpdateQueryAlias;
    public $InsertQueryAlias;
    public $DeleteQueryAlias;
    protected $ConnectionName;
    protected $GlobalActionAlias;

    public function __construct($_name, $_parent, $_connection_name)
    {
        parent::__construct($_name, $_parent);
        $this->Property=array();
        $this->Property["ID"]=0;
        $this->ConnectionName=$_connection_name;
    }

    public function Clear()
    {
        $this->ID=0;
        unset($this->Property);
        $this->Property=array();
    }

    public function Read($ID)
    {
        $ParamArray=$ID;
        if(!is_array($ID))
        {
            $ParamArray=array("ID"=>$ID);
        }
        $ds=new DBDataSet("", $this, \Mayral\Classes\DB\SQLConnection::GetInstance());
        $SelectQueryText=\Project\Factory::GetInstance()->GetQueryCollection()->QueryText($this->SelectQueryAlias, $ParamArray);
        $ds->Open($SelectQueryText);
        $this->ReadFromDataSet($ds);
    }

    public function ReadFromDataSet($_ds)
    {
        foreach($_ds->ColumnInfo->Items as $col_info)
        {
            $FieldName=$col_info->Name;
            $FieldValue=$_ds->GetFieldValue($FieldName);

            $this->SetPropertyValue($FieldName, $FieldValue);

            if($FieldName=="ID")
            {
                $this->ID=$FieldValue;
            }
        }
    }

    public function Write()
    {
        $QueryText="";
        $action="";
        if($this->ID===0)
        {
            $QueryText=\Project\Factory::GetInstance()->GetQueryCollection()->QueryText($this->InsertQueryAlias, $this->Property);
            $action="insert";
        }
        else
        {
            $QueryText=\Project\Factory::GetInstance()->GetQueryCollection()->QueryText($this->UpdateQueryAlias, $this->Property);
            $action="update";
        }
        $QueryText=str_replace("'NULL'", "NULL", $QueryText);
        if($result=\Mayral\Classes\DB\SQLConnection::GetInstance()->sql_query($QueryText)&&$this->ID===0)
        {
            $this->ID=\Mayral\Classes\DB\SQLConnection::GetInstance()->sql_insert_id($result);
        }
        $this->SendGlobalEvent("new_db_action", array("action"=>$action, "ID"=>$this->ID, "Query"=>$QueryText));
    }

    public function Delete()
    {
        if($this->DeleteQueryAlias!='')
        {
            \Mayral\Classes\DB\SQLConnection::GetInstance()->sql_query(\Project\Factory::GetInstance()->GetQueryCollection()->QueryText($this->DeleteQueryAlias, array('ID'=>$this->Id())));
        }
    }

    public function RefreshData()
    {
        $this->Read($this->ID);
    }

    public function PropertyValue($_name)
    {
        $Result="";
        if(isset($this->Property[$_name]))
        {
            $Result=$this->Property[$_name];
        }
        return $Result;
    }

    public function SetPropertyValue($_name, $_value)
    {
        $current_value=$this->PropertyValue($_name);
        if($current_value!=$_value)
        {
            $this->IsChanged=true;
        }
        $this->Property[$_name]=$_value;
    }

    public function GetProperty()
    {
        return $this->Property;
    }

    public function Id()
    {
        return $this->ID;
    }

    /**
     * Сбрасывает ID в 0, что порождает копирование
     *
     * @return void ничего
     *
     */
    public function Copy()
    {
        $this->ID=0;
        $this->Property['ID']=0;
    }

}

?>