<?php

/* * **************************************************************************
  Description: базовый класс для хранения табличных данных
  Version: 1.0.0

  Changes info:
  = 10.07.2008	(Kecheor):			создан
  + 09.12.2008	(Ivanov Kirill):	добавлен механизм работы с зависимыми (связанными) компонентами
  + 04.03.2009	(Zinchenko Sergey): добавлен метод сортировки Sort
 * ************************************************************************** */

namespace Mayral\Classes\DB;

class DataTable extends \Mayral\Classes\Basic\BasicClass
{

    //Двумерный массив содержаший данные
    protected $Table;
    protected $DeletedRows=array();
    //Указатель на текущую строку
    protected $CurrentRow;
    //Количество, строк и колонок
    public $RowCount;
    public $ColCount;
    //	массив указывающий на компоненты связанные с данным DataTable
    public $LinkedComponents;

    //Конструктор
    public function __construct($_name, $_parent)
    {
        $this->LinkedComponents=new \Mayral\Classes\Lists\BasicList($this);
        $this->CurrentRow=0;
        $this->RowCount=0;
        $this->ColCount=0;
        parent::__construct($_name, $_parent);
    }

    //	добавляет зависимый компонент
    public function AddLinkedComponent($_component)
    {
        $this->LinkedComponents->Add($_component);
    }

    //	вызывает обновление зависимых компонентов
    public function RefreshLinkedComponents()
    {
        $LinkedComponents=$this->LinkedComponents;
        for($i=0; $i<$LinkedComponents->Count(); $i++)
        {
            $Component=$LinkedComponents->Item($i);
            \Mayral\Classes\Basic\ObjectsPool::Create()->AddToChangedComponents($Component);
        }
    }

    //Перевод указатель строки на первую
    public function FirstRow()
    {
        $this->SetRow(0);
    }

    //Перевод указатель строки на последнюю
    public function LastRow()
    {
        $this->SetRow($this->RowCount-1);
    }

    //Переводит указатель на след строку, и возвращает false если строк больше нет
    public function NextRow()
    {
        // :'( было так красива
        //return ++$this->CurrentRow < $this->RowCount;
        $result=false;
        do
        {
            $result=$this->SetRow($this->CurrentRow+1);
        } while(in_array($this->CurrentRow, $this->DeletedRows));
        return $result; //$this->CurrentRow < $this->RowCount;
    }

    public function SetRow($_row)
    {
        if($_row<$this->RowCount)
        {
            if($this->CurrentRow!=$_row)
            {
                $this->CurrentRow=$_row;
                $this->RefreshLinkedComponents();
            }
            return true;
        }
        else
        {
            return false;
        }
    }

    public function GetRow($_row=-1)
    {
        return $this->Table[$_row==-1?$this->CurrentRow:$_row];
    }

    public function RowIndex()
    {
        return $this->CurrentRow;
    }

    //Возвращает значение ячейки таблицы, либо текущего ряда, либо по двумерным координатам
    public function GetFieldValue($col, $row=-1)
    {
        if($row==-1)
        {
            $row=$this->CurrentRow;
        }
        return isset($this->Table[$row][$col])?$this->Table[$row][$col]:false;
    }

    //Записывает значение в ячейку таблицы, если ряд неуказан, то берется текущий
    public function SetFieldValue($value, $col, $row=-1)
    {
        if($row==-1)
        {
            $row=$this->CurrentRow;
        }
        if($row<$this->RowCount&&$col<$this->ColCount)
        {
            if($this->Table[$row][$col]!=$value)
            {
                $this->Table[$row][$col]=$value;
                $this->RefreshLinkedComponents();
            }
            return true;
        }
        else
        {
            return false;
        }
    }

    //Добавляет ряд в конец таблицы
    public function AddRow()
    {
        $this->RowCount++;
        $this->Table[]=array();
    }

    //Добаляет ряд к удаленным, если $row не указан, то берется текущий
    public function DeleteRow($row=-1)
    {
        if($row==-1)
        {
            $row=$this->CurrentRow;
        }
        else
        {
            if($row>$this->RowCount)
            {
                return false;
            }
        }
        $DeletedRows[]=$row;
        $this->RefreshLinkedComponents();
        return true;
    }

    /**
     * Метод для сортировки записей в таблице
     *
     * @param mixed $_field Идентификатор колонки
     * @param bool $_up По возрастанию/по убыванию
     *
     */
    public function Sort($_field, $_up=true)
    {
        $i=0;
        $j=0;
        $k=0;
        $tmp=0;

        if($this->RowCount==1)
            return;
        $n=$this->RowCount-1;
        $i=1;
        do
        {
            $j=0;
            do
            {
                if(($_up&&($this->Table[$i][$_field]<=$this->Table[$j][$_field]))||(!$_up&&($this->Table[$i][$_field]>=$this->Table[$j][$_field])))
                {
                    $k=$i;

                    $tmp=$this->Table[$i];
                    do
                    {
                        $this->Table[$k]=$this->Table[$k-1];
                        $k=$k-1;
                    } while($k>$j);

                    $this->Table[$j]=$tmp;
                    $j=$i;
                }
                else
                {
                    $j=$j+1;
                }
            } while($j<$i);
            $i=$i+1;
        } while($i<=$n);
    }

}

?>