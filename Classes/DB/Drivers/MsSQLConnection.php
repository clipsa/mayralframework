<?php

/* * **************************************************************************
  Description: класс используемый для работы в MsSQL
  Version: 1.0.0

  Changes info:
  = 10.12.2008:	(Ivanov Kirill)	создан
 * ************************************************************************** */

class MsSQLConnection extends \Mayral\Classes\DB\DBConnection
{

    //Открывает соединение с базой, возвращает указатель на соединение
    function sql_connect()
    {
        $result = false;
        if ($this->Host != '' && $this->Login != '' && $this->Pass != '' && $this->DBName != '')
        {
            $this->Connection = mssql_connect($this->Host, $this->Login, $this->Pass);
            if ($this->Connection !== false)
            {
                $result = $this->sql_select_db($this->DBName, $this->Connection);
            }
        }
        return $result;
    }

    function sql_attected_rows()
    {
        return mssql_affected_rows($this->Connection);
    }

    function sql_error()
    {
        return mssql_error($this->Connection);
    }

    function sql_escape_string($s)
    {
        return mssql_escape_string($s);
    }

    function sql_fetch_array($query_result, $t = MYSQL_BOTH)
    {
        return mssql_fetch_array($query_result, $t);
    }

    function sql_fetch_field($query_result, $i)
    {
        $result = mssql_fetch_field($query_result, $i);
        //	в теории это должно работать, на практике же это бага php =)
        //	$result->table = $result->column_source;
        $result->table = "";
        return $result;
    }

    function sql_field_name($query_result, $i)
    {
        return mssql_field_name($query_result, $i);
    }

    function sql_field_type($query_result, $i)
    {
        return mssql_field_type($query_result, $i);
    }

    function sql_free_result($query_result)
    {
        return mssql_free_result($query_result);
    }

    function sql_insert_id($query_result = null)
    {
        // не работает в mssql =(
        //return mssql_insert_id($this->Connection);
    }

    function sql_num_rows($query_result)
    {
        return mssql_num_rows($query_result);
    }

    function sql_num_fields($query_result)
    {
        return mssql_num_fields($query_result);
    }

    function sql_result($query_result, $r, $f)
    {
        $result = mssql_result($query_result, $r, $f);
        //$result = convert_cyr_string($result,"a","w"); 
        $result = iconv("cp1251", "UTF-8", $result);
        return $result;
    }

    function sql_query($q, $l = false)
    {
        echo $q;
        return mssql_query($q, $this->Connection);
    }

    function sql_select_db($db_name)
    {
        return mssql_select_db($db_name, $this->Connection);
    }

    function sql_close()
    {
        mssql_close($this->Connection);
    }

    function sql_data_seek($_result, $_row_num)
    {
        
    }

}

?>