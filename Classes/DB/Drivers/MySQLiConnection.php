<?php

/* * **************************************************************************
  Description: класс используемый для работы в MySQL через MySQLi

  Author: Zinchenko Sergey
  Created: 19.09.2012
  Version: 0.1.0

  Changes info:
  = 19.09.2012:	создан
 * ************************************************************************** */

namespace Mayral\Classes\DB\Drivers;

class MySQLiConnection extends \Mayral\Classes\DB\DBConnection
{

    //Открывает соединение с базой, возвращает указатель на соединение
    function sql_connect()
    {
        $this->Connected = false;
        if ($this->Host != '' && $this->Login != '' && $this->Pass != '' && $this->DBName != '')
        {
            $this->Connection = mysqli_connect($this->Host, $this->Login, $this->Pass);
            if ($this->Connection !== false)
            {
                $this->Connected = mysqli_select_db($this->Connection, $this->DBName);
                if ($this->Connected)
                {
                    $this->sql_query("SET NAMES UTF8");
                }
                return $this->Connected;
            }
        }

        $this->SendGlobalEvent("DB_connect_fail");

        return false;
    }

    function sql_attected_rows()
    {
        return $this->Connection->affected_rows;
    }

    function sql_error()
    {
        return $this->Connection->error;
    }

    function sql_escape_string($s)
    {
        return mysqli_real_escape_string($s);
    }

    function sql_fetch_array($query_result, $t = MYSQLI_BOTH)
    {
        return mysqli_fetch_array($query_result, $t);
    }

    function sql_fetch_field($query_result, $i)
    {
        $result = mysqli_fetch_field_direct($query_result, $i);
        $result->table = "";
        return $result;
    }

    function sql_field_name($query_result, $i)
    {
        return $this->sql_fetch_field($query_result, $i)->name;
    }

    function sql_field_type($query_result, $i)
    {
        return $this->sql_fetch_field($query_result, $i)->type;
    }

    function sql_free_result($query_result)
    {
        return mysqli_stmt_free_result($query_result);
    }

    function sql_insert_id($query_result = null)
    {
        return $this->Connection->insert_id;
    }

    function sql_num_rows($query_result)
    {
        $result = false;
        if ($query_result !== false)
        {
            $result = mysqli_num_rows($query_result);
        }
        return $result;
    }

    function sql_num_fields($query_result)
    {
        return mysqli_num_fields($query_result);
    }

    function sql_result($query_result, $r, $f)
    {
        //$Result = mysql_result($query_result, $r, $f);
        $query_result->data_seek($r);
        $ceva = $query_result->fetch_assoc();
        $keys = array_keys($ceva);
        $Result = $ceva[$keys[$f]];
        /* if ($Result === false)
          {
          Error("MySQL error in: ".$query_result);
          } */
        return $Result;
    }

    function sql_query($q, $l = false)
    {
        $this->Refresh();
        $Result = mysqli_query($this->Connection, $q);
        if ($Result === false)
        {
            //Error("MySQL error in: " . $q);
        }
        return $Result;
    }

    function sql_select_db($db_name)
    {
        return mysqli_select_db($this->Connection, $db_name);
    }

    function sql_close()
    {
        mysqli_close($this->Connection);
    }

    function sql_data_seek($_result, $_row_num)
    {
        mysqli_data_seek($_result, $_row_num);
    }

}

?>