<?php

/* * **************************************************************************
  Description: класс используемый для работы в SQLSRV
  Version: 1.0.0

  Changes info:
  = 01.08.2012	(Sokolov Aleksandr):	создан
  = 02.08.2012	(Sokolov Aleksandr):	поправлено пару методов
 * ************************************************************************** */

class SQLSRVConnection extends DBConnection
{

    function sql_connect()
    {
        $this->Connected = false;
        if ($this->Host != '' && $this->Login != '' && $this->Pass != '' && $this->DBName != '')
        {
            $this->Connection = sqlsrv_connect($this->Host, array(
                'UID' => $this->Login,
                'PWD' => $this->Pass,
                'Database' => $this->DBName,
                'CharacterSet' => 'UTF-8',
                'ReturnDatesAsStrings' => true)
            );
            if ($this->Connection != false)
                return true;
        }

        $this->SendGlobalEvent("DB_connect_fail");

        return false;
    }

    function sql_attected_rows()
    {
        return sqlsrv_rows_affected($this->Connection);
    }

    function sql_error()
    {
        return sqlsrv_errors();
    }

    function sql_escape_string($s)
    {
        return addslashes($s);
    }

    function sql_fetch_array($query_result, $t = SQLSRV_FETCH_NUMERIC, $_row = SQLSRV_SCROLL_ABSOLUTE, $_offset = 0)
    {
        return sqlsrv_fetch_array($query_result, $t, $_row, $_offset);
    }

    function sql_fetch_field($query_result, $i)
    {
        $result = new stdClass();
        $metadata = sqlsrv_field_metadata($query_result);
        $result->name = $metadata[$i]['Name'];
        $result->table = '';
        return $result;
    }

    function sql_field_name($query_result, $i)
    {
        $metadata = sqlsrv_field_metadata($query_result);
        return $metadata['Name'];
    }

    function sql_field_type($query_result, $i)
    {
        $metadata = sqlsrv_field_metadata($query_result);
        return $metadata['Type'];
    }

    function sql_free_result($query_result)
    {
        return sqlsrv_free_stmt($query_result);
    }

    function sql_insert_id($query_result = null)
    {
        sqlsrv_next_result($query_result);
        sqlsrv_fetch($query_result);
        return sqlsrv_get_field($query_result, 0);
    }

    function sql_num_rows($query_result)
    {
        return sqlsrv_num_rows($query_result);
    }

    function sql_num_fields($query_result)
    {
        return sqlsrv_num_fields($query_result);
    }

    function sql_result($query_result, $r, $f)
    {
        $Result = false;
        if (sqlsrv_fetch($query_result, SQLSRV_SCROLL_ABSOLUTE, $r))
            $Result = sqlsrv_get_field($query_result, $f);

        if (!$Result)
            Error("SQLSRV error in: " . $query_result);

        //$Result=iconv("cp1251","UTF-8",$Result);
        return $Result;
    }

    function sql_query($q, $l = false)
    {
        $this->Refresh();
        //$q=iconv("UTF-8","cp1251",$q);
        $Result = sqlsrv_query($this->Connection, $q, array(), array("Scrollable" => 'static'));
        if ($Result === false)
        {
            //Error("SQLSRV error in: " . $q);
        }

        return $Result;
    }

    function sql_select_db($db_name)
    {
        if ($this->sql_query("use Mayral\{$db_name}"))
            return true;
        return false;
    }

    function sql_get_field($result, $_index = 0)
    {
        sqlsrv_get_field($result, $_index);
    }

    function sqlsrv_fetch($_result, $_index = 0)
    {
        sqlsrv_fetch($_result, SQLSRV_SCROLL_ABSOLUTE, $_index);
    }

    function sql_close()
    {
        sqlsrv_close($this->Connection);
    }

    function sql_data_seek($_result, $_row_num)
    {
        
    }

}

?>