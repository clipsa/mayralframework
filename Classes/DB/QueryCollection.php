<?php

/* * **************************************************************************
  Description: коллекция запросов
  Author: Ivanov Kirill
  Version: 1.0.0

  Changes info:
  = 22.06.2009	(Ivanov Kirill):	создан
 * ************************************************************************** */

namespace Mayral\Classes\DB;

class QueryCollection
{

    public $Query;

    public function __construct()
    {
        $this->Query=new \Mayral\Classes\Lists\AssociationList($this);
    }

    protected function PrepareQueryText($_query_text, $_items)
    {
        if($_items!=='')
        {
            $s=new \Mayral\Classes\Basic\String($this, $_query_text);
            foreach($_items as $key=> $val)
            {
                if($key!='FILTER')
                {
                    //$_items[$key]=addslashes($_items[$key]);
                    $_items[$key]=$_items[$key];
                }
                else
                {
                    $_items[$key]=$_items[$key];
                }
            }
            $s->TemplateReplace($_items);
            $_query_text=$s->Text;
        }
        return $_query_text;
    }

    public function QueryText($_query_alias, $_items='')
    {
        $Result=$this->Query->Item($_query_alias);

        $PosibleMethodName=$_query_alias;
        if(method_exists($this, $PosibleMethodName))
        {
            $Result=$this->$PosibleMethodName($_query_alias, $_items);
        }
        if($Result!=='')
        {
            $Result=$this->PrepareQueryText($Result, $_items);
        }
        return $Result;
    }

    public function AddQuery($_alias, $_query_text)
    {
        $this->Query->Add($_query_text, $_alias);
    }

}

?>