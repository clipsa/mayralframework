<?php

/* * **************************************************************************
  Description: компонент для связи компонента с полем ODBObject
  Version: 1.0.1

  Changes info:
  = 27.11.2009	(Ivanov Kirill):	создан
  + 09.12.2009	(Ivanov Kirill):	преобразование в зависимости от типа компонента
 * ************************************************************************** */

namespace Mayral\Classes\DB;

class RVCAssigner extends \Mayral\Classes\Basic\BasicClass
{

    protected $Items;
    public $Record;

    public function __construct($_name, $_parent)
    {
        parent::__construct($_name, $_parent);
        $this->Items = array();
    }

    public function Add($_value_component, $_field_name, $_label_component = '', $_check_fill = false, $_type = 'text', $_record = '')
    {
        if ($_record === '')
        {
            $_record = $this->Record;
        }
        $Item = array('Conponent' => $_value_component, 'FieldName' => $_field_name,
            'Record' => $_record, 'LabelComponent' => $_label_component,
            'CheckFill' => $_check_fill, 'Type' => $_type);
        $this->Items[] = $Item;

        if ($_check_fill && $_label_component !== '')
        {
            $_label_component->Text = $_label_component->Text . ' *';
        }
    }

    public function Clear()
    {
        $this->Items = array();
    }

    /**
     * Передает значения полей компонентам
     *
     *
     */
    public function ToComponents()
    {
        foreach ($this->Items as $Item)
        {
            $Component = $Item['Conponent'];
            $Record = $Item['Record'];
            $FieldName = $Item['FieldName'];
            $LabelComponent = $Item['LabelComponent'];
            $CheckFill = $Item['CheckFill'];

            $Component->Value = $this->PrepareFieldValue($Record->PropertyValue($FieldName), $Component);
        }
    }

    /**
     * конвертирует значение поля в зависимост от класса компонента связанного с ним
     *
     * @param string $_value значение поле
     * @param OBasicValueComponent $_component связанный компонент
     * @return string результат работы функции
     *
     */
    protected function PrepareFieldValue($_value, $_component)
    {
        $Result = $_value;
        if (get_class($_component) === 'Mayral\Classes\VisualComponents\DatePicker')
        {
            if ($_value == '' || $_value == '1970-01-01')
            {
                //$Result = date('d.m.Y');
            }
            else
            {
                $Result = \Mayral\Classes\Basic\DateTimeFunctions::DatetimeMysqlToPhp($_value);
            }
        }
        if (get_class($_component) === 'Mayral\Classes\VisualComponents\CheckBox')
        {
            $Result = (bool) $_value;
        }
        if (get_class($_component) === 'Mayral\Classes\VisualComponents\Edit')
        {
            //$Result = htmlspecialchars($_value);
            //$Result = stripslashes($_value);
        }
        return $Result;
    }

    /**
     * конвертирует значение компонента в зависимост от класса
     *
     * @param OBasicValueComponent $_component связанный компонент
     * @return string результат работы функции
     *
     */
    protected function PrepareComponentValue($_component, $_type = 'text')
    {
        $Result = $_component->Value;
        if (get_class($_component) === 'Mayral\Classes\VisualComponents\DatePicker')
        {
            if ($Result != '')
            {
                $Result = \Mayral\Classes\Basic\DateTimeFunctions::DatetimePhpToMysql($Result);
            }
            else
            {
                $Result = 'NULL';
            }
        }
        if (get_class($_component) === 'Mayral\Classes\VisualComponents\CheckBox')
        {
            $Result = intval($Result);
        }

        if (get_class($_component) === 'Mayral\Classes\VisualComponents\Edit')
        {
            $Result = htmlspecialchars($Result);
            $Result = addslashes($Result);
        }

        if ($_type == 'float')
        {
            $Result = str_replace(',', '.', $Result);
            $Result = floatval($Result);
        }
        if ($_type == 'int')
        {
            $Result = intval($Result);
        }

        return $Result;
    }

    public function CheckFill()
    {
        $Result = true;
        foreach ($this->Items as $Item)
        {
            $Component = $Item['Conponent'];
            $Record = $Item['Record'];
            $FieldName = $Item['FieldName'];
            $LabelComponent = $Item['LabelComponent'];
            $CheckFill = $Item['CheckFill'];

            $this->InlightOff($Component);
            if ($LabelComponent !== '')
            {
                $this->InlightOff($LabelComponent);
            }

            if ($CheckFill && ($Component->Value === '' || ($Component instanceof OEditAutoCompleteAjax && $Component->GetDataProperty('id') == '')))
            {
                $Result = false;
                $this->InlightOn($Component);
                if ($LabelComponent !== '')
                {
                    $this->InlightOn($LabelComponent);
                }
            }
        }
        return $Result;
    }

    /**
     * Записывает значения компонентов в поля
     * проводит проверку полей обязательных к заполнению
     *
     * @return bool false в том случае если не все обязательные поля были заполнены
     *
     */
    public function ToRecord()
    {
        foreach ($this->Items as $Item)
        {
            $Component = $Item['Conponent'];
            $Record = $Item['Record'];
            $FieldName = $Item['FieldName'];
            $LabelComponent = $Item['LabelComponent'];
            $CheckFill = $Item['CheckFill'];

            $Record->SetPropertyValue($FieldName, $this->PrepareComponentValue($Component, $Item['Type']));
        }
        return $this->CheckFill();
    }

    protected function InlightOn($_label)
    {
        if ($_label->Style->Color !== 'red' && $_label->Style->FontWeight !== 'bold')
        {
            $_label->Style->Color = '#d00';
            $_label->Style->FontWeight = 'bold';
            \Mayral\Classes\Basic\ObjectsPool::Create()->AddToChangedComponents($_label);
        }
    }

    protected function InlightOff($_label)
    {
        if ($_label->Style->Color !== '' && $_label->Style->FontWeight !== '')
        {
            $_label->Style->Color = '';
            $_label->Style->FontWeight = '';
            \Mayral\Classes\Basic\ObjectsPool::Create()->AddToChangedComponents($_label);
        }
    }

    public function CompareRecords()
    {
        $result = array();

        foreach ($this->Items as $Item)
        {
            $Component = $Item['Conponent'];
            $Record = $Item['Record'];
            $FieldName = $Item['FieldName'];

            if ($Record->PropertyValue($FieldName) != $this->PrepareComponentValue($Component))
            {
                $result[] = $FieldName . ': ' . $Record->PropertyValue($FieldName) . '->' . $this->PrepareComponentValue($Component);
            }
        }

        return $result;
    }

}

?>