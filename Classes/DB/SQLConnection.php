<?php

/* * **************************************************************************
  Description: класс работающий либо с MSSQL либо с MySQL
  Author: Zinchenko Sergey
  Version: 1.1.0

  Changes info:
  = 29.07.2012:	(Zinchenko Sergey) создан
  = 02.08.2012:       (Zinchenko Sergey) немного переделано
 * ************************************************************************** */

namespace Mayral\Classes\DB;

class SQLConnection
{

    /**
     * Экземпляр объекта
     *
     * @var SQLConnection 
     *
     */
    private static $Instance;

    /**
     * Is MsSQL
     *
     * @var bool 
     *
     */
    //public static $IsMSSQLConnection=false;

    public static $Host = "";
    public static $Login = "";
    public static $Password = "";
    public static $DBName = "";
    public static $Drivers = array();
    public static $CurrentDriver;

    private function __construct()
    {
        
    }

    public static function &GetInstance($_parent = null)
    {
        if (is_null(self::$Instance))
        {
            $driver_name = self::$Drivers[self::$CurrentDriver];
            self::$Instance = new $driver_name("MainDBConnection", $_parent, self::$Host, self::$Login, self::$Password, self::$DBName, "");
        }
        return self::$Instance;
    }

    public static function Close()
    {
        if (!is_null(self::$Instance))
        {
            self::$Instance->sql_close();
        }
    }

}

SQLConnection::$Drivers["mssql"] = "\Mayral\Classes\DB\Drivers\MsSQLConnection";
SQLConnection::$Drivers["sqlsrv"] = "\Mayral\Classes\DB\Drivers\SQLSRVConnection";
SQLConnection::$Drivers["mysqli"] = "\Mayral\Classes\DB\Drivers\MySQLiConnection";
SQLConnection::$CurrentDriver="mysqli";


?>