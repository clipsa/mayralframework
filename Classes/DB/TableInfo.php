<?php

namespace Mayral\Classes\DB;

class TableInfo extends \Mayral\Classes\Basic\BasicClass
{

    public $PrimaryKey;
    public $Submitable;

    public function __construct($_name, $_parent, $PrimaryKeyField=null, $Submitable=false)
    {
        if(isset($this->PrimaryKey))
        {
            $this->PrimaryKey=$PrimaryKeyField;
        }
        $this->Submitable=$Submitable;
        parent::__construct($_name, $_parent);
    }

    public function SetPrimaryKey($PrimaryKeyField)
    {
        $this->PrimaryKey=$DBField;
    }

    public function MakeSubmitable($PrimaryKeyField)
    {
        $this->PrimaryKey=$PrimaryKeyField;
        $this->Submitable=true;
    }

    public function MakeUnsubmitable()
    {
        $this->Submitable=false;
    }

}

?>