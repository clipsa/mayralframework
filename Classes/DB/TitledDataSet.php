<?php

/* * **************************************************************************
  Description: добавилась поддержка алиасов колонок,, экспорт в CSV и XML
  Author: Kecheor
  Created: 10.07.2008
  Version: 1.0.0

  Changes author: -
  Changes info:
  = 10.07.2008:	создан
 * ************************************************************************** */

namespace Mayral\Classes\DB;

class TitledDataSet extends DataTable
{

    public $ColumnInfo;

    public function __construct($_name, $_parent)
    {
        $this->ColumnInfo=new \Mayral\Classes\Lists\NamedItemList('ColumnInfo', $this);
        parent::__construct($_name, $_parent);
    }

    //Устанавливает алиас для колонки
    public function SetColumnName($value, $col)
    {
        $col_info=new ColumnInfo($value, $this->ColumnInfo, $col);
        if($this->GetColumnByIndex($col)===false)
        {
            $this->ColumnInfo->Add($col_info);
        }
        else
        {
            $this->ColumnInfo->Item($col)->Name=$value;
        }
    }

    //Возвращает имя колонки по номеру
    public function GetColumnByIndex($col)
    {
        foreach($this->ColumnInfo->Items as $col_info)
        {
            if($col_info->Index==$col)
            {
                return $col_info;
            }
        }
        return false;
    }

    /**
     * Возвращает информацию о столбце по имени
     *
     * @param string $col Имя столбца
     * @return ColumnInfo Информация о столбце
     *
     */
    public function GetColumnByName($col)
    {
        return $this->ColumnInfo->ItemByName($col);
    }

    //Возвращает значение из таблицы, добавлена поддержка алиасов колонок
    public function GetFieldValue($col, $row=-1)
    {
        if($this->ColumnInfo->ItemByName("$col")!==false)
        {
            $col=$this->ColumnInfo->ItemByName($col)->Index;
        }
        return parent::GetFieldValue($col, $row);
    }

    //Записывает значение в ячейку таблицы, добавлена поддержка алиасов колонок
    public function SetFieldValue($value, $col, $row=-1)
    {
        if($this->ColumnInfo->ItemByName("$col")!==false)
        {
            $col=$this->ColumnInfo->ItemByName($col)->Index;
        }
        return parent::SetFieldValue($value, $col, $row);
    }

    //Возвращает содержание таблице в формате CSV файла, сам файл не создает!!!
    public function ForCSV()
    {
        $result='';
        for($r=0; $r<$this->RowCount; $r++)
        {
            $result='';
            for($c=0; $c<$this->ColCount; $c++)
            {
                if($result!='')
                {
                    $result .= ';';
                }
                $result .= $this->GetFieldValue($c, $r);
            }
            $result .= '\n';
        }
        return $result;
    }

}

?>