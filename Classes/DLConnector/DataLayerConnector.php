<?php

namespace Mayral\Classes\DLConnector;

class DataLayerConnector
{

    private $Requests;
    private $DLHost;
    private static $Instance;

    private function __construct()
    {
        $this->Requests = new \Mayral\Classes\Lists\BasicList('');
    }

    public function SetServerName($_server_name)
    {
        $this->DLHost = $_server_name;
    }

    public function GetServerName()
    {
        return $this->DLHost;
    }

    public static function Create()
    {
        if (is_null(self::$Instance))
        {
            self::$Instance = new self();
        }
        return self::$Instance;
    }

    public function AddRequest(\Mayral\Classes\DLConnector\DataLayerRequest $_request)
    {
        $this->Requests->Add($_request);
    }

    public function Connect()
    {
        $requests = clone $this->Requests;
        $this->Requests->Clear();
        for ($i = 0; $i < $requests->Count(); $i++)
        {
            $this->DataLayerConnect($requests->Item($i));
        }
    }

    protected function DataLayerConnect(\Mayral\Classes\DLConnector\DataLayerRequest $_request)
    {
        $connector = new \Mayral\Classes\Net\SimpleConnector();
        $connector->SetServerName($this->GetServerName());
        $connector->SetParams($_request->ToAssociationList());
        $connector->AddObserver($_request);
        $connector->SendRequest();
    }

}

?>