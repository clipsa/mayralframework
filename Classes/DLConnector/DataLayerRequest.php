<?php

namespace Mayral\Classes\DLConnector;

class DataLayerRequest extends \Mayral\Classes\Listeners\Observable implements \Mayral\Classes\Interfaces\IObserver
{
    const SIMPLE_JSON="SimpleJson";
    
    public $Data;
    
    private $RequestType;
    
    private $ResponseType;
    
    private $Function;
    
    public function __construct()
    {
        parent::__construct();
        $this->Data=new \Mayral\Classes\Lists\AssociationList('');
        $this->Data->Add(array(), 'Data');
        
        $this->SetRequestType(self::SIMPLE_JSON);
        $this->SetResponseType(self::SIMPLE_JSON);
    }
    
    public function SetRequestType($_request_type)
    {
        $this->RequestType=$_request_type;
        $this->Data->Add($_request_type, 'RequestType');
    }
    
    public function GetRequestType()
    {
        return $this->RequestType;
    }
    
    public function SetResponseType($_response_type)
    {
        $this->ResponseType=$_response_type;
        $this->Data->Add($_response_type, 'ResponseType');
    }
    
    public function GetResponseType()
    {
        return $this->ResponseType;
    }
    
    public function SetFunction($_function)
    {
        $this->Function=$_function;
        $this->Data->Add($_function, 'Function');
    }
    
    public function GetFunction()
    {
        return $this->Function;
    }
    
    public function ToString()
    {
        $result='';
        
        $result='RequestType='.$this->GetRequestType().'&callback=&Data='.json_encode($this->Data);
        
        return $result;
    }
    
    public function ToAssociationList()
    {
        $result=new \Mayral\Classes\Lists\AssociationList('');
        
        $result->Add($this->GetRequestType(), 'RequestType');
        $result->Add('', 'callback');
        $result->Add(json_encode($this->Data->Items), 'Data');
        
        return $result;
    }
    
    protected function PrepareResponse($_response_str)
    {
        $result='';
        
        $result=\Mayral\Classes\Basic\StringFunctions::json_to_object($_response_str, new DataLayerResponse());
        
        return $result;
    }

    public function Update($_data, $_observable)
    {
        $this->Notify($this->PrepareResponse($_data));
    }

}

?>