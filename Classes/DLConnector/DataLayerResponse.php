<?php

namespace Mayral\Classes\DLConnector;

class DataLayerResponse implements \Mayral\Classes\DLConnector\Interfaces\IResponse
{

    private $Request;
    private $Status;
    private $Time;
    private $ErrorCode;
    private $Version;
    private $Data;

    public function GetData()
    {
        return $this->Data;
    }

    public function SetData($_data)
    {
        if (isset($_data->Data))
        {
            $this->Data = $_data->Data;
        }
        else
        {
            $this->Data = $_data;
        }
    }

    public function SetRequest(\Mayral\Classes\DLConnector\Interfaces\IRequest $_request)
    {
        $this->Request = $_request;
    }

    public function GetRequest()
    {
        return $this->Request;
    }

    public function SetStatus($_status)
    {
        $this->Status = $_status;
    }

    public function GetStatus()
    {
        return $this->Status;
    }

    public function SetTime($_time)
    {
        $this->Time = $_time;
    }

    public function GetTime()
    {
        return $this->Time;
    }

    public function SetErrorCode($_error_code)
    {
        $this->ErrorCode = $_error_code;
    }

    public function GetErrorCode()
    {
        return $this->ErrorCode;
    }

    public function SetVersion($_version)
    {
        $this->Version = $_version;
    }

    public function GetVersion()
    {
        return $this->Version;
    }

}

?>