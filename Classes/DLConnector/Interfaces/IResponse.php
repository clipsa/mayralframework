<?php

namespace Mayral\Classes\DLConnector\Interfaces;

interface IResponse
{
    public function SetData($_data);
    public function GetData();
}

?>