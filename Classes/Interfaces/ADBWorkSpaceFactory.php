<?php

namespace Mayral\Classes\Interfaces;

abstract class ADBWorkSpaceFactory extends AWorkSpaceFactory
{
    
    
    abstract function CreateQueryCollection();
    
    public function GetQueryCollection($_name='QueryCollection')
    {
        if(!isset($_SESSION[$_name]))
        {
            $_SESSION[$_name]=$this->CreateQueryCollection();
        }
        return $_SESSION[$_name];
    }
}

?>