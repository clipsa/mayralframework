<?php

namespace Mayral\Classes\Interfaces;

abstract class AWorkSpaceFactory
{
    public $ProjectPath;
    
    abstract public function CreateWorkSpace();
    
    public function GetWorkSpace($_name='WorkSpace')
    {
        if(!isset($_SESSION[$_name]))
        {
            $_SESSION[$_name]=$this->CreateWorkSpace();
        }
        return $_SESSION[$_name];
    }
}

?>
