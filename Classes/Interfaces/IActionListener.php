<?php

/****************************************************************************
	Description: интерфейс обработчика событий
 	Author: Zinchenko Sergey
 	Version: 1.0.0

	Changes info: 
		+ 15.06.2012	(Zinchenko Sergey):	создан
****************************************************************************/
namespace Mayral\Classes\Interfaces;

interface IActionListener
{
	/**
	 * Метод обработки события
	 *
	 * @param string $_action_name имя события
	 * @param array $_action_arguments параметры события
	 * @return void ничего не возвращает
	 *
	 */	
	public function ActionPerform($_event_name, $_event_argument);
}

?>