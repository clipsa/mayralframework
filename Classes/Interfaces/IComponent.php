<?php

/****************************************************************************
	Description: интерфейс базового компонента системы
 	Author: Zinchenko Sergey
 	Version: 1.0.0

	Changes info: 
		+ 15.06.2012	(Zinchenko Sergey):	создан
****************************************************************************/
namespace Mayral\Classes\Interfaces;

interface IComponent
{
	public function Generate();
}

?>