<?php

namespace Mayral\Classes\Interfaces;

interface IObserver
{
    public function Update($_data, $_observable);
}

?>