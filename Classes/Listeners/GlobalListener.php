<?php

/* * **************************************************************************
  Description: глобальный слушатель событий (singletone)
  Author: Zinchenko Sergey
  Version: 1.0.0

  Changes info:
  + 15.06.2012    (Zinchenko Sergey):	создан
  + 18.08.2012    (Zinchenko Sergey):	RemoveListener
 * ************************************************************************** */

namespace Mayral\Classes\Listeners;

class GlobalListener extends \Mayral\Classes\Basic\MayralObject
{

    /**
     * список слушателей
     *
     * @var OBasicList 
     *
     */
    protected $Listeners;

    public function __construct()
    {
        $this->Listeners = new \Mayral\Classes\Lists\BasicList('GlobalListener_list', $this);
    }

    public function AddListener(\Mayral\Classes\Interfaces\IActionListener $_listener)
    {
        $this->Listeners->Add($_listener);
    }

    public function RemoveListener(\Mayral\Classes\Interfaces\IActionListener $_listener)
    {
        $_listener->MarkToKill();
    }

    public function DeleteMarkedToKill()
    {
        $cnt = $this->Listeners->Count();
        for ($i = 0; $i < $cnt; $i++)
        {
            if ($this->Listeners->Item($i) != '' && $this->Listeners->Item($i)->KillMark)
            {
                $this->Listeners->Del($i);
            }
        }
    }

    public function ProcessGlobalEvent($_event_name, $_event_argument)
    {
        $result = array();

        for ($i = 0; $i < $this->Listeners->Count(); $i++)
        {
            $tmp_result = $this->Listeners->Item($i)->ActionPerform($_event_name, $_event_argument);
            if (!is_null($tmp_result) && $tmp_result != '')
            {
                $result[] = $tmp_result;
            }
        }

        return $result;
    }

}

?>