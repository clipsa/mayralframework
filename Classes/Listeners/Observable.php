<?php

/* * **************************************************************************
  Description: Observer
  Author: Zinchenko Sergey
  Created: 2014
  Version: 1.0.0

  Changes info:
  = 2014	(Zinchenko Sergey):	создан
 * ************************************************************************** */
namespace Mayral\Classes\Listeners;

class Observable
{
    private $Listeners;
    
    public function __construct()
    {
        $this->Listeners=new \Mayral\Classes\Lists\BasicList('');
    }
    
    public function AddObserver(\Mayral\Classes\Interfaces\IObserver $_listeners)
    {
        $this->Listeners->Add($_listeners);
    }
    
    public function RemoveObserver(\Mayral\Classes\Interfaces\IObserver $_listeners)
    {
        $this->Listeners->Del($this->Listeners->ItemIndex($_listeners));
    }
    
    public function Notify($_data)
    {
        for($i=0; $i<$this->Listeners->Count(); $i++)
        {
            $this->Listeners->Item($i)->Update($_data, $this);
        }
    }
}

?>