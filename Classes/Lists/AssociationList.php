<?php

/* * **************************************************************************
  Description:
  Author: Ivanov Kirill
  Created: 08.07.2008
  Version: 3.0.0

  Changes info:
  + 08.07.2008	(Ivanov Kirill):	создан
  + 11.12.2008	(Ivanov Kirill):	IsExist
  + 04.03.2009	(Zinchenko Sergey): GetKeys
 * ************************************************************************** */

namespace Mayral\Classes\Lists;

class AssociationList extends BasicList
{
    /*
      добавление элемента
      $_item - элемент
      $_item_index - ассоциативный индекс
     */

    public function Add($_item, $_item_index='')
    {
        $this->Items[$_item_index]=$_item;
    }

    //	проверяет наличие элемента по $_index
    public function IsExist($_index)
    {
        return isset($this->Items[$_index]);
    }

    //	получения элемента по $_index
    public function Item($_index)
    {
        $result="";
        if($this->IsExist($_index))
        {
            $result=$this->Items[$_index];
        }
        return $result;
    }

    //	удаление элемента по $_index
    public function Del($_index)
    {
        unset($this->Items[$_index]);
    }

    //	получение индекса элемента
    public function ItemIndex($item)
    {
        foreach($this->Items as $key=> $value)
        {
            if($this->Item($key)==$item)
            {
                $result=$key;
            }
        }
        return $result;
    }

    // получение значений ключей
    public function GetKeys()
    {
        return array_keys($this->Items);
    }

    public function WriteToString()
    {
        $Result="";
        foreach($this->Items as $key=> $value)
        {
            if($Result!=="")
            {
                $Result .= "[ITEM_END]";
            }
            $Result .= $key."[VALUE]".$value;
        }
        return $Result;
    }

    public function ReadFromString($_string)
    {
        $Params=explode("[ITEM_END]", $_string);
        for($i=0; $i<count($Params); $i++)
        {
            $ParamStr=$Params[$i];
            $Param=explode("[VALUE]", $ParamStr);
            if(isset($Param[1])&&$Param[0])
            {
                $this->Add($Param[1], $Param[0]);
            }
        }
    }

}

?>