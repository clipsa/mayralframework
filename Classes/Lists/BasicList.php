<?php

/* * **************************************************************************
  Description: базовый класс для списков
  Author: Ivanov Kirill
  Created: 08.11.2007
  Version: 1.0.0

  Changes author: -
  Changes info:
  = 08.11.2007	(Ivanov Kirill):	создан
  + 15.07.2007	(Ivanov Kirill):	Clear()
 * ************************************************************************** */

namespace Mayral\Classes\Lists;

class BasicList extends \Mayral\Classes\Basic\BasicClass
{

    public $Items;

    public function __construct($_parent)
    {
        parent::__construct('', $_parent);
        $this->Items = array();
    }

    public function Count()
    {
        return count($this->Items);
    }

    public function Add($item)
    {
        $new_index = $this->Count();
        $this->Items[$new_index] = $item;
    }

    public function Item($index)
    {
        $result = '';
        if (isset($this->Items[$index]))
        {
            $result = $this->Items[$index];
        }
        return $result;
    }

    public function Del($index)
    {
        for ($i = $index; $i < $this->Count() - 1; $i++)
        {
            $this->Items[$i] = $this->Items[$i + 1];
        }
        unset($this->Items[$this->Count() - 1]);
    }

    public function ItemIndex($item)
    {
        $result = -1;
        for ($i = 0; $i < $this->Count(); $i++)
        {
            if ($this->Item($i) == $item)
            {
                $result = $i;
            }
        }
        return $result;
    }

    /*
      функция очищает список
     */

    public function Clear()
    {
        unset($this->Items);
        $this->Items = array();
    }

}

?>