<?

/* * **************************************************************************
  Description:
  Author: Ivanov Kirill
  Created: 10.07.2008
  Version: 1.0.0

  Changes info:
  + 10.07.2008	(Ivanov Kirill):	создан
 * ************************************************************************** */

namespace Mayral\Classes\Lists;

class NamedItemList extends AssociationList
{
    /*
      добавление элемента
     */

    public function Add($_item, $_item_index='')
    {
        parent::Add($_item, $_item_index==''?$_item->Name:$_item_index);
    }

    public function ItemByName($name)
    {
        $result=false;
        foreach($this->Items as $key=> $value)
        {
            if($this->Item($key)->Name==$name)
            {
                $result=$this->Item($key);
            }
        }
        return $result;
    }

}

?>