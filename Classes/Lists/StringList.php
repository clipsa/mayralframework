<?

/* * **************************************************************************
  Description:
  Author: Ivanov Kirill
  Created: 24.10.2007
  Version: 3.0.1

  Changes author: Ivanov Kirill
  Changes info:
  + 26.10.2007:	функция Explode аналогичная php
  + 26.10.2007:	функция ItemIndex поиск индекса элемента по его содержанию
  + 26.10.2007:	функция ToStr
  = 08.11.2007:	переход на Mayral 3
 * ************************************************************************** */

namespace Mayral\Classes\Lists;

class StringList extends BasicList
{

    public function Add($item)
    {
        $new_index=$this->Count();
        $this->Items[$new_index]=$item;
    }

    public function Item($index)
    {
        $result="";
        $result=$this->Items[$index];
        return $result;
    }

    public function Del($index)
    {
        for($i=$index; $i<$this->Count()-1; $i++)
        {
            $this->Items[$i]=$this->Items[$i+1];
        }
        unset($this->Items[$this->Count()-1]);
    }

    public function ExplodeStr($dec, $str)
    {
        $items_array=explode($dec, $str);
        for($i=0; $i<count($items_array); $i++)
        {
            $this->Add($items_array[$i]);
        }
    }

    public function ToStr($dec="")
    {
        $result="";
        for($i=0; $i<$this->ItemsCount(); $i++)
        {
            $item=$this->Item($i);
            if($result!="")
            {
                $result=$result.$dec;
            }
            $result=$result.$item;
        }
        return $result;
    }

    public function ItemIndex($item)
    {
        $result=-1;
        for($i=0; $i<$this->ItemsCount(); $i++)
        {
            if($this->Item($i)==$item)
            {
                $result=$i;
            }
        }
        return $result;
    }

}

?>