<?php

namespace Mayral\Classes\Net;

class FTPConnector
{

    private $Connection;
    private $Host;
    private $Port;
    private $Timeout;
    private $Login;
    private $Password;
    private $CurrentDir;

    public function __construct($_host = null, $_login = null, $_pass = null)
    {
        $this->SetHost($_host);
        $this->SetPort(21);
        $this->SetTimeout(90);
    }

    public function __destruct()
    {
        ftp_close($this->GetConnection());
    }

    public function Connect()
    {
        $result = false;

        if (($this->Connection = ftp_connect($this->GetHost(), $this->GetPort(), $this->GetTimeout())) !== false && ftp_login($this->Connection, $this->GetLogin(), $this->GetPassword()))
        {
            $result = true;
            $this->SetCurrentDir(ftp_pwd($this->Connection));
        }

        return $result;
    }

    public function UploadFile($_ftp_save_path, $_local_file_path)
    {
        $result = false;
        if ($this->IsConnected())
        {
            $result = ftp_put($this->GetConnection(), $_ftp_save_path, $_local_file_path, FTP_BINARY);
        }
        return $result;
    }

    public function DownloadFile($_local_save_path, $_ftp_file_path)
    {
        $result = false;

        if ($this->IsConnected())
        {
            $result = ftp_get($this->GetConnection(), $_local_save_path, $_ftp_file_path, FTP_BINARY);
        }

        return $result;
    }

    public function IsConnected()
    {
        return ($this->Connection !== null && $this->Connection !== false);
    }

    public function GetFileList()
    {
        return ftp_nlist($this->GetConnection(), $this->GetCurrentDir());
    }

    public function GetConnection()
    {
        return $this->Connection;
    }

    public function SetHost($_host)
    {
        $this->Host = $_host;
    }

    public function GetHost()
    {
        return $this->Host;
    }

    public function SetPort($_port)
    {
        $this->Port = $_port;
    }

    public function GetPort()
    {
        return $this->Port;
    }

    public function SetTimeout($_timeout)
    {
        $this->Timeout = $_timeout;
    }

    public function GetTimeout()
    {
        return $this->Timeout;
    }

    public function SetCurrentDir($_current_dir)
    {
        $this->CurrentDir = $_current_dir;
    }

    public function GetCurrentDir()
    {
        return $this->CurrentDir;
    }

    public function SetLogin($_login)
    {
        $this->Login = $_login;
    }

    public function GetLogin()
    {
        return $this->Login;
    }

    public function SetPassword($_password)
    {
        $this->Password = $_password;
    }

    public function GetPassword()
    {
        return $this->Password;
    }

}

?>