<?php

namespace Mayral\Classes\Net;

class SimpleConnector extends \Mayral\Classes\Listeners\Observable
{

    private $Params;
    private $ServerName;

    public function __construct()
    {
        parent::__construct();
        $this->Params = new \Mayral\Classes\Lists\AssociationList('');
        $this->Listeners=new \Mayral\Classes\Lists\BasicList('');
    }

    public function SetServerName($_server_name)
    {
        $this->ServerName = $_server_name;
    }

    public function GetServerName()
    {
        return $this->ServerName;
    }

    public function AddParam($_key, $_value)
    {
        $this->Params->Add($_value, $_key);
    }
    
    public function SetParams(\Mayral\Classes\Lists\AssociationList $_params)
    {
        $this->Params=$_params;
    }

    public function RemoveParam($_key)
    {
        $this->Params->Del($_key);
    }

    protected function PrepareRequest()
    {
        $result = '';
        $keys = $this->Params->GetKeys();
        for ($i = 0; $i < count($keys); $i++)
        {
            if ($result != '')
            {
                $result.='&';
            }
            $result.=$keys[$i] . '=' . $this->Params->Item($keys[$i]);
        }

        return $result;
    }

    public function SendRequest()
    {
        $result=$this->SendPost();
        
        return $result;
    }

    public function SendGet()
    {
        $result=file_get_contents($this->GetServerName() . '?' . $this->PrepareRequest());
        $this->Notify($result);
        return $result;
    }

    public function SendPost()
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $this->PrepareRequest());
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $this->GetServerName());

        $response = curl_exec($ch);
        curl_close($ch);
        
        $this->Notify($response);
        
        return $response;
    }

}

?>