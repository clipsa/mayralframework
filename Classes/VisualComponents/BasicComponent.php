<?php

/* * **************************************************************************
  Description: базовый класс для визуальных компонентов
  Author: Ivanov Kirill
  Created: 11.11.2007
  Version: 1.2.5

  Changes info:
  = 11.11.2007	(Ivanov Kirill):	создан
  + 10.07.2007	(Ivanov Kirill):	свойство Visible
  + 14.07.2007	(Ivanov Kirill):	существенно изменен механизм генерации компонентов
  + 01.06.2009	(Ivanov Kirill):
  + 04.02.2010	(Ivanov Kirill):	DeleteLastTemplate
  + 26.06.2011	(Ivanov Kirill):	GetParentForm - теперь находит именно форму родителя, +GetFirstForm - находит первую форму, т.е если вызвать функцию у формы она вернет ее же
  = 30.11.2011	(Efimov Evgenij):	PreparePropertyValue не фильтровала символы ' и \
  = 16.06.2012  (Zinchenko Sergey):     реализует интерфейс IComponent
 * ************************************************************************** */
namespace Mayral\Classes\VisualComponents;

use Mayral\Classes\Basic;

class BasicComponent extends \Mayral\Classes\Basic\BasicClass implements \Mayral\Classes\Interfaces\IComponent
{

    protected $SendProp;
    protected $AllTempaltesFileName;
    protected $_Visible = true;
    protected $PreGenerateVar;
    protected $Hint;
    public $IsForm = false;

    public function __construct($_name, $_parent)
    {
        parent::__construct($_name, $_parent);

        $this->SendProp = array();
        $this->SendProp['Visible'] = true;
        $this->SendProp['Enable'] = true;

        $this->AllTempaltesFileName = new \Mayral\Classes\Lists\StringList($this);
        $this->SetTemplateFileName('Templates/Components/' . 'BasicComponent.html');

        if ($_parent !== '')
        {
            $_parent->AddComponent($this);
            if ($_name !== '')
            {
                $_parent->$_name = $this;
                $this->GetParentForm()->$_name = $this;
            }
        }

        $this->AddToObjectsPool();
    }

    public function MarkToKill()
    {
        parent::MarkToKill();
        $this->Visible = false;
    }

    public function __destruct()
    {
        
    }

    /*
      функция возвращает родительскую форму
     */

    /**
     * удаляет последний темплейт из списка темплейтов
     *
     */
    public function DeleteLastTemplate()
    {
        $this->AllTempaltesFileName->Del($this->AllTempaltesFileName->Count() - 1);
    }

    public function GetParentForm()
    {
        $result = $this;
        if ($this->Parent !== '')
        {
            $result = $this->Parent->GetFirstForm();
        }
        return $result;
    }

    public function GetFirstForm()
    {
        $result = $this;
        if (!$this->IsForm && $this->Parent !== '')
        {
            $result = $this->Parent->GetFirstForm();
        }
        return $result;
    }

    /*
      функция присваивает шаблон компоненту и записывает его в список шаблонов
     */

    public function SetTemplateFileName($_file_name)
    {
        $this->AllTempaltesFileName->Add($_file_name);
    }

    //	действия перед генерацией компонента
    protected function BeforeGenerate()
    {
        //	заполнение массива переменный для подмены в шаблоне
        $this->PreGenerateVar['Name'] = $this->FullName();
        $this->PreGenerateVar['ParentName'] = $this->GetParentForm()->FullName();
        $this->PreGenerateVar['ComponentContainerStyle'] = '';
        $this->PreGenerateVar['ComponentContainerOuterClass'] = '';
        $this->PreGenerateVar['Child'] = '';
        $this->PreGenerateVar['Hint'] = $this->Hint;
    }

    //	загружаем темплэйт компонента
    protected function LoadTemplate($_type)
    {
        $result = new Basic\String($this);
        $result->Text = '{Child}';
        $s = new Basic\String($this);

        //	генерируем темплейт (проходим по всем родительский темплейтам)
        for ($i = 0; $i < $this->AllTempaltesFileName->Count(); $i++)
        {
            if ($this->Visible)
            {
                $path = $this->AllTempaltesFileName->Items[$i];
                $s->LoadFromFile($path);
                $result->Replace('{Child}', $s->Text);
                //	если это не последний темплейт то удаляем место для компонентов {_Components}
                if ($i < $this->AllTempaltesFileName->Count() - 1)
                {
                    $result->Replace('{_Components}', '');
                }
            }
        }
        return $result->Text;
    }

    //	генерируем компонент
    public function Generate($_type = '')
    {
        $result = $this->LoadTemplate($_type);

        $result = $this->GenerateByTemplate($result);
        return $result;
    }

    /*
      генерируем компонент из переданного темплейта
     */

    public function GenerateByTemplate($_template_body)
    {
        $this->BeforeGenerate();
        return $this->ReplacePreGenerateVar($_template_body);
    }

    //	заменяем в строке соответствия из PreGenerateVar
    protected function ReplacePreGenerateVar($_text)
    {
        foreach ($this->PreGenerateVar as $name => $value)
        {
            $scripted_name = '{' . $name . '}';
            $_text = str_replace($scripted_name, strval($value), $_text);
        }
        return $_text;
    }

    //	функция проверяет доступен ли компонент клиенту
    public function ClientAccessible()
    {
        //	проверяем видим ли компонент
        $this_accessible = $this->Visible;
        //	проверяем доступен ли родители компонента
        $parents_accessible = ($this->Parent === '' || $this->Parent->ClientAccessible());
        //	компонент считается доступным если он доступен сам и доступны все его родители
        return $this_accessible && $parents_accessible;
    }

    /*
      функция перехватывает чтение значений
     */

    function __get($_prop_name)
    {
        $result = '';
        if ($_prop_name == 'Visible')
        {
            $result = $this->_Visible;
        } else
        {
            if (isset($this->$_prop_name))
            {
                $result = $this->$_prop_name;
            }
        }
        return $result;
    }

    /*
      функция перехватывает присвоение значений членам класса
     */

    function __set($_prop_name, $_prop_value)
    {
        if (isset($this->SendProp[$_prop_name]) && $this->SendProp[$_prop_name] && $this->$_prop_name != $_prop_value)
        {
            \Mayral\Classes\Basic\ObjectsPool::Create()->AddToChangedComponents($this);
        }
        if ($_prop_name == 'Visible')
        {
            $this->_Visible = $_prop_value;
        }
        if ($_prop_name != 'Visible')
        {
            $this->$_prop_name = $_prop_value;
        }
        return true;
    }

    public function CustomXML($_xml)
    {
        
    }

    protected function PreparePropertyValue($_value)
    {
        return strval($_value);
    }

}

?>