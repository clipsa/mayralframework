<?php

/* * **************************************************************************
  Description: базовый компонент контейнер
  Author: Ivanov Kirill
  Created: 09.07.2008
  Version: 1.0.1

  Changes info:
  = 09.07.2008	(Ivanov Kirill):	создан
  = 21.06.2012    (Zinchenko Sergey):     GenerateComponents
 * ************************************************************************** */

namespace Mayral\Classes\VisualComponents;

class BasicContainer extends BasicStyleComponent
{

    public $Components;
    public $InnerTemplateFile = '';

    public function __construct($_name, $_parent)
    {
        parent::__construct($_name, $_parent);
        $this->Components = new \Mayral\Classes\Lists\NamedItemList($this);
    }

    protected function BeforeGenerate()
    {
        parent::BeforeGenerate();
        //	формируем массив прегенерации
        $this->PreGenerateVar['_Components'] = '';
        if ($this->Visible)
        {
            $this->GenerateComponents();
        }
    }

    protected function GenerateComponents()
    {
        foreach ($this->Components->Items as $key => $component)
        {
            $component_gen = $component->Generate();
            if ($component->Style->Position == 'absolute' || $component->Style->Position == 'relative')
            {
                $this->PreGenerateVar['_Components'] .= $component_gen;
            }
            else
            {
                $this->PreGenerateVar[$key] = $component_gen;
            }
        }
    }

    //	добавление компонента
    public function AddComponent($_component)
    {
        $this->Components->Add($_component, $_component->Name);
    }

    //	удаление компонента 
    public function RemoveChild($_component_name)
    {
        parent::RemoveChild($_component_name);
        $this->Components->Del($_component_name);
    }

    public function Clear()
    {
        if (!is_null($this->Components))
        {
            foreach ($this->Components->Items as $key => $component)
            {
                $component->MarkToKill();
            }
        }
    }

    public function SetVisibility($_flag)
    {
        parent::SetVisibility($_flag);
        if (!is_null($this->Components))
        {
            foreach ($this->Components->Items as $key => $component)
            {
                if ($component instanceof BasicContainer)
                {
                    $component->SetVisibility($_flag);
                }
            }
        }
    }

    public function SetEnable($_flag)
    {
        if (!is_null($this->Components))
        {
            foreach ($this->Components->Items as $key => $component)
            {
                if ($component instanceof BasicContainer)
                {
                    $component->SetEnable($_flag);
                }
                else
                {
                    $component->Enable = $_flag;
                }
            }
        }
    }

    /**
     * Создание формы из xml файла
     *
     * @param string $_file Путь к xml файлу
     * @return void 
     *
     */
    protected function CreateFromXML($_file)
    {
        $xml = simplexml_load_file($_file);
        foreach ($xml as $key => $value)
        {
            //$this->CreateObjectFromXML($xmlnode->component, $this);
            switch ($key)
            {
                case 'children':
                    foreach ($value as $xmlnode)
                    {
                        $this->CreateObjectFromXML($xmlnode, $this);
                    }
                    break;
                default:
                    if (!is_object($this->$key))
                    {
                        $this->$key = strval(current($value));
                    }
                    else
                    {
                        $this->CreatePropsFromXML($value, $this->$key);
                    }
                    break;
            }
        }
    }

    protected function CreateObjectFromXML($_xml, $_parent)
    {
        $object = null;
        if (strpos(reset($_xml['class']), '\\') === false)
        {
            $objecttype = "\\Mayral\\Classes\\VisualComponents\\" . reset($_xml['class']);
        }
        else
        {
            $objecttype = reset($_xml['class']);
        }
        $objectname = reset($_xml['name']);
        if (!isset($_parent->$objectname))
        {
            $object = new $objecttype($objectname, $_parent);
        }
        else
        {
            $object = $_parent->$objectname;
        }
        foreach (get_object_vars($_xml) as $key => $value)
        {
            switch ($key)
            {
                case '@attributes':
                    break;
                case 'children':
                    foreach ($value as $xmlnode)
                    {
                        $this->CreateObjectFromXML($xmlnode, $object);
                    }
                    break;
                case 'list':
                    $this->CreateListFromXML($value, $object);
                    break;
                case 'custom':
                    $object->CustomXML($value);
                    break;
                default:
                    if (!is_object($object->$key))
                    {
                        $value = $this->PreparePropertyValue($value);
                        $object->$key = $value;
                    }
                    else
                    {
                        $this->CreatePropsFromXML($value, $object->$key);
                    }
                    break;
            }
        }
        $i = 0;
    }

    protected function CreateListFromXML($_xml, $_parent)
    {
        $listclass = reset($_xml['class']);
        $listname = reset($_xml['name']);

        $list = new $listclass($listname, $_parent);

        foreach ($_xml->xpath('item') as $itemnode)
        {
            $itemvalue = $this->PreparePropertyValue(reset($itemnode['value']));
            $itemname = $this->PreparePropertyValue(reset($itemnode['name']));
            $list->Add($itemname, $itemvalue);
        }

        $_parent->$listname = $list;
    }

    protected function CreatePropsFromXML($_xml, $_object)
    {
        $props = get_object_vars($_xml);
        if ($props !== false)
        {
            foreach ($props as $key => $value)
            {
                if (count(get_object_vars($_xml->$key)) == 0)
                {
                    $value = $this->PreparePropertyValue($value);
                    $_object->$key = $value;
                }
                if (count(get_object_vars($_xml->$key)) >= 1)
                {
                    $this->CreatePropsFromXML($value, $_object->$key);
                }
            }
        }
    }

}

?>