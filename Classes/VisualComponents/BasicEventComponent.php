<?php

/* * **************************************************************************
  Description: базовый компонент с событиями
  Created: 15.07.2008
  Version: 1.0.0

  Changes info:
  = 15.07.2008	(Ivanov Kirill):	создан
  + 01.08.2008	(Ivanov Kirill):	OnMouseOut
  + 01.09.2008	(Kecheor):			CustomEvent
  + 02.12.2008	(Ivanov Kirill):	возможность вызова в событии функции любого компонента ( $component->FullName()."DemoEvent" )
  + 20.04.2011	(Ivanov Kirill):	OnKeyUp
  + 02.09.2011	(Ivanov Kirill):	изменен принцип вызовы обработчика события, изменения от 02.12.2008 удалены
 * ************************************************************************** */

namespace Mayral\Classes\VisualComponents;

class BasicEventComponent extends BasicComponent
{

    public $AutoPost = false;
    /*
      событие:		компонент теряет фокус
      используют:		select, text, textarea
     */
    public $javascript_OnBlur;
    public $OnBlur;
    public $BlurListeners;

    /*
      событие:		компонент теряет фокус и изменяется его значение
      используют:		select, text, textarea
     */
    public $use_ajax_OnChange = false;
    public $javascript_OnChange;
    public $OnChange;
    public $ChangeListeners;

    /*
      событие:		пользователь кликает на компонент
      используют:		button, checkbox, radio, link, reset, submit
     */
    public $javascript_OnClick;
    public $OnClick;
    public $ClickListeners;

    /*
      событие:		компонент получает фокус
      используют:		select, text, textarea
     */
    public $javascript_OnFocus;
    public $OnFocus;
    public $FocusListeners;

    /*
      событие:		курсор мыши наводится на компонент
      используют:		link (на самом деле это пашет на многих)
     */
    public $javascript_OnMouseOver;
    public $OnMouseOver;
    public $MouseOverListeners;

    /*
      событие:		курсор мыши наводится на компонент
      используют:		link (на самом деле это пашет на многих)
     */
    public $javascript_OnMouseOut;
    public $OnMouseOut;
    public $MouseOutListeners;

    /*
      событие:		нажата кнопка
      используют:		input
     */
    public $javascript_OnKeyPress;
    public $OnKeyPress;
    public $KeyPressListeners;

    /*
      событие:		отпущена кнопка
      используют:		input
     */
    public $javascript_OnKeyUp;
    public $OnKeyUp;
    public $KeyUpListeners;


    /*
     * событие блокирует клиент
     */
    public $BlockOnEvent = false;

    public function __construct($_name, $_parent)
    {
        parent::__construct($_name, $_parent);

        $this->BlurListeners = new \Mayral\Classes\Listeners\Observable();
        $this->ChangeListeners = new \Mayral\Classes\Listeners\Observable();
        $this->ClickListeners = new \Mayral\Classes\Listeners\Observable();
        $this->FocusListeners = new \Mayral\Classes\Listeners\Observable();
        $this->KeyPressListeners = new \Mayral\Classes\Listeners\Observable();
        $this->KeyUpListeners = new \Mayral\Classes\Listeners\Observable();
        $this->MouseOutListeners = new \Mayral\Classes\Listeners\Observable();
        $this->MouseOverListeners = new \Mayral\Classes\Listeners\Observable();
    }

    /*
      генерируем строку для одного события
     */

    protected function GenerateEventString($_javascript_event, $_javascript_event_text, $_server_event_aliase, $_server_event, $_use_ajax_on_server_event = true, $_argument = "''", $_event_parent = "this", $_block_on_event = false)
    {
        $result = $_javascript_event_text;
        if ($_server_event != "" && $_use_ajax_on_server_event)
        {
            $EventText = "ServerEvent(this, '" . $_server_event_aliase . "', " . ($_block_on_event ? 'true' : 'false') . ");";
            if ($_argument !== "")
            {
                $EventText = "ServerArgEvent(" . $_event_parent . ", '" . $_server_event_aliase . "', " . $_argument . ", " . ($_block_on_event ? 'true' : 'false') . ");";
            }

            if ($this->AutoPost)
            {
                $ParentForm = $this->GetParentForm();
                $EventText = "ServerEventWithFormData(" . $_event_parent . ", 
                                    '" . $_server_event_aliase . "', '" . $ParentForm->FullName() . "', 
                                        " . $_argument . ", " . ($_block_on_event ? 'true' : 'false') . ");";
            }

            $result = $result . $EventText;
        }
        if ($result != "")
        {
            $result = $_javascript_event . "=\"javascript:" . $result . "\" ";
        }
        return $result;
    }

    /*
      генерируем строку для событий
     */

    protected function GenerateAllEventString()
    {
        $result = "";
        $result = $result . $this->GenerateEventString("onblur", $this->javascript_OnBlur, "onexit", $this->OnBlur);
        $result = $result . $this->GenerateEventString("onchange", $this->javascript_OnChange, "onchange", $this->OnChange, $this->use_ajax_OnChange);
        $result = $result . $this->GenerateEventString("onclick", $this->javascript_OnClick, "onclick", $this->OnClick, true, "''", "this", $this->BlockOnEvent);
        $result = $result . $this->GenerateEventString("onfocus", $this->javascript_OnFocus, "onfocus", $this->OnFocus);
        $result = $result . $this->GenerateEventString("onmouseover", $this->javascript_OnMouseOver, "onmouseover", $this->OnMouseOver);
        $result = $result . $this->GenerateEventString("onmouseout", $this->javascript_OnMouseOut, "onmouseout", $this->OnMouseOut);
        $result = $result . $this->GenerateEventString("onkeypress", $this->javascript_OnKeyPress, "onkeypress", $this->OnKeyPress, true, "event.keyCode");
        $result = $result . $this->GenerateEventString("onkeyup", $this->javascript_OnKeyUp, "onkeyup", $this->OnKeyUp, true);
        return $result;
    }

    //	действия перед генерацией компонента
    protected function BeforeGenerate()
    {
        parent::BeforeGenerate();
        //	заполнение массива переменный для подмены в шаблоне
        $this->PreGenerateVar["Events"] = $this->GenerateAllEventString();
    }

    function Event($_event_name, $_event_args)
    {
        $action_function_name = "";
        if ($_event_name == "onexit")
        {
            $action_function_name = $this->OnExit;
        }
        if ($_event_name == "onchange")
        {
            $this->ChangeListeners->Notify($_event_args);
        }
        if ($_event_name == "onclick")
        {
            $action_function_name = $this->OnClick;
            $this->ClickListeners->Notify($_event_args);
        }
        if ($_event_name == "onfocus")
        {
            $action_function_name = $this->OnFocus;
            $this->FocusListeners->Notify($_event_args);
        }
        if ($_event_name == "onmouseover")
        {
            $action_function_name = $this->OnMouseOver;
            $this->MouseOverListeners->Notify($_event_args);
        }
        if ($_event_name == "onmouseout")
        {
            $action_function_name = $this->OnMouseOut;
            $this->MouseOutListeners->Notify($_event_args);
        }
        if ($_event_name == "onkeypress")
        {
            $action_function_name = $this->OnKeyPress;
            $this->KeyPressListeners->Notify($_event_args);
        }
        if ($_event_name == "onkeyup")
        {
            $action_function_name = $this->OnKeyUp;
            $this->KeyUpListeners->Notify($_event_args);
        }
        if ($action_function_name == "")
        {
            $action_function_name = $this->CustomEvent($_event_name);
        }
        else
        {
            //$action_function_name = 'Parent->'.$action_function_name;
        }
        if ($action_function_name != "")
        {
            if (method_exists($this->GetParentForm(), $action_function_name))
            {
                $this->GetParentForm()->$action_function_name($this, $_event_name, $_event_args);
            }
            else
            {
                if (method_exists($this, $action_function_name))
                {
                    $this->$action_function_name($this, $_event_name, $_event_args);
                }
                else
                {
                    $this->SendGlobalEvent("Error", "Undefined function '" . $action_function_name . "' called by '" . $this->Name . "'");
                }
            }
        }
    }

    /**
     * Функция для обработки собственных событий в наследуемых компонентах
     *
     * @param string $_event_name Произошедшее событие
     * @return string Имя функции для исполнения
     *
     */
    protected function CustomEvent($_event_name)
    {
        return '';
    }

}

?>