<?php

/* * **************************************************************************
  Description: простая панелька
  Author: Ivanov Kirill
  Version: 1.0.0

  Changes info:
  = 24.07.2008	(Ivanov Kirill):	создан
 * ************************************************************************** */

namespace Mayral\Classes\VisualComponents;

class BasicPanel extends BasicContainer
{

    var $Title;

    public function __construct($_name, $_parent)
    {
        parent::__construct($_name, $_parent);
        //$this->Style->Overflow = 'hidden';
        $this->SetTemplateFileName('Templates/Components/'.'BasicPanel.html');
    }

    protected function BeforeGenerate()
    {
        parent::BeforeGenerate();
        //	заполнение массива переменный для подмены в шаблоне
        $this->PreGenerateVar['Title']=$this->Title;
    }

}

?>