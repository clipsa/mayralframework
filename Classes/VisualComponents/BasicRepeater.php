<?php

/* * **************************************************************************
  Description: базовый компонет
  Author: Ivanov Kirill
  Version: 1.0.0

  Changes info:
  = 22.07.2008	(Ivanov Kirill):	создан
  = 27.05.2009	(Ivanov Kirill):	переделан с нуля
  + 27.11.2009	(Ivanov Kirill):	OnItemClick
  + 02.07.2011	(Ivanov Kirill):	OnPrepareValue - позволяет передать внешнюю функцию для подготовки значения
 * ************************************************************************** */

namespace Mayral\Classes\VisualComponents;

class BasicRepeater extends BasicValueComponent
{

    public $ItemTemplate;
    public $EmptyTemplate;
    public $Items;
    public $javascript_OnItemClick="";
    public $OnItemClick="";
    public $OnPrepareValue="";

    public function __construct($_name, $_parent)
    {
        parent::__construct($_name, $_parent);

        $this->Items=new \Mayral\Classes\Lists\BasicList($this);
        $this->SendProp["Value"]=false;
    }

    /*
      генерируем элементы
     */

    protected function GenerateItems()
    {
        $result="";
        for($i=0; $i<$this->Items->Count(); $i++)
        {
            $result=$result.$this->GenerateItem($i);
        }
        return $result;
    }

    protected function GetItemTemplate($_index)
    {
        return $this->ItemTemplate;
    }

    protected function GenerateItem($_index)
    {
        //$this->Items->Item($_index)->GetProperty();
        $Item=$this->Items->Item($_index);
        if(method_exists($Item, 'GetProperty'))
        {
            $Item=$Item->GetProperty();
        }
        else
        {
            $Item=isset($Item->Property)?$Item->Property:$Item;
        }
        $ItemTemplate=$this->GetItemTemplate($_index);
        $ItemTemplate=str_replace("{INDEX}", $_index, $ItemTemplate);
        if(is_array($Item)||is_object($Item))
        {
            foreach($Item as $name=> $value)
            {
                $scripted_name="{".$name."}";
                $ItemTemplate=str_replace($scripted_name, $this->PrepareItemValue($_index, $name, $value), $ItemTemplate);
            }
        }
        else
        {
            $ItemTemplate=str_replace('{Item}', $this->PrepareItemValue($_index, 'Item', $Item), $ItemTemplate);
        }
        $OnItemClick_Text="";
        if($this->OnItemClick!=="")
        {
            $OnItemClick_Text=$this->GenerateEventString("onclick", $this->javascript_OnItemClick, "onitemclick", $this->OnItemClick, true, "'".$_index."'", "'".$this->FullName()."'");
        }
        $ItemTemplate=str_replace("{ItemEvents}", $OnItemClick_Text, $ItemTemplate);
        return $ItemTemplate;
    }

    protected function PrepareItemValue($_index, $_name, $_value)
    {
        //$_value = htmlspecialchars($_value);
        $_value=nl2br($_value);
        if($this->OnPrepareValue!=="")
        {
            $OnPrepareValue=$this->OnPrepareValue;
            $_value=$this->GetParentForm()->$OnPrepareValue($_index, $_name, $_value);
        }
        return $_value;
    }

    protected function BeforeGenerate()
    {
        parent::BeforeGenerate();
        //	заполнение массива переменный для подмены в шаблоне
        $Items=$this->GenerateItems();
        $Items=$this->ReplacePreGenerateVar($Items);
        if($Items==="")
        {
            $Items=$this->EmptyTemplate;
        }
        $this->PreGenerateVar["Items"]=$Items;
    }

    //	загружаем темплэйт компонента
    protected function LoadTemplate($_type)
    {
        $result=new \Mayral\Classes\Basic\String($this);
        $result->Text=parent::LoadTemplate($_type);

        $this->ItemTemplate=$result->CutTextBetween("<!--item_start-->", "<!--item_end-->");
        $this->EmptyTemplate=$result->CutTextBetween("<!--empty_start-->", "<!--empty_end-->");

        return $result->Text;
    }

    /**
     * Функция для обработки собственных событий в наследуемых компонентах
     *
     * @param string $_event_name Произошедшее событие
     * @return string Имя функции для исполнения
     *
     */
    protected function CustomEvent($_event_name)
    {
        if($_event_name==="onitemclick")
        {
            return $this->OnItemClick;
        }
        return '';
    }

}

?>