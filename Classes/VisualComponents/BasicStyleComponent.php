<?php

/* * **************************************************************************
  Description: базовый компонент со стилями
  Author: Ivanov Kirill
  Created: 09.07.2008
  Version: 1.0.0

  Changes info:
  = 09.07.2008	(Ivanov Kirill):	создан
 * ************************************************************************** */

namespace Mayral\Classes\VisualComponents;

class BasicStyleComponent extends BasicEventComponent
{

    public $Style;
    public $Class='';
    public $Enable=true;
    public $UseParentFont=false;

    public function __construct($_name, $_parent)
    {
        $this->Style=new Style($this);
        //$this->Style->Overflow='auto';

        parent::__construct($_name, $_parent);
    }

    protected function BeforeGenerate()
    {
        parent::BeforeGenerate();
        //	заполнение массива переменный для подмены в шаблоне
        $this->PreGenerateVar['ComponentContainerStyle']=$this->Style->GenerateComponentContainerStyle();
        $this->PreGenerateVar['Style']=$this->Style->GenerateComponentBodyStyle();
        $this->PreGenerateVar['Font_Style']=$this->GetFont()->Generate();
        $this->PreGenerateVar['Class']=$this->Class;
        $this->PreGenerateVar['Disabled']='';
        if($this->Enable!=true)
        {
            $this->PreGenerateVar['Disabled']='disabled';
        }
    }

    //	функция проверяет доступен ли компонент клиенту
    public function ClientAccessible()
    {
        //	проверяем видим ли компонент
        $this_accessible=$this->Enable&&parent::ClientAccessible();
        //	компонент считается доступным если он доступен сам и доступны все его родители
        return $this_accessible;
    }

    //	функция получает стиль шрифта родителя
    public function GetFont()
    {
        $result=$this->Style->Font;
        if($this->UseParentFont&&$this->Parent!=''&&isset($this->Parent->Style))
        {
            $result=$this->Parent->GetFont();
        }
        return $result;
    }

    public function SetVisibility($_flag)
    {
        $this->_Visible=$_flag;
    }

    /*
      функция перехватывает присвоение значений членам класса
     */

    function __set($prop_name, $prop_value)
    {
        parent::__set($prop_name, $prop_value);
        if($prop_name=='Visible')
        {
            $DisplayValue=null;
            if(!$prop_value)
            {
                $DisplayValue='none';
            }
            $this->SetVisibility($prop_value);
            $this->Style->Display=$DisplayValue;
            $this->IsChanged=true;
        }
        if($prop_name!='Visible')
        {
            $this->$prop_name=$prop_value;
        }
        return true;
    }

}

?>