<?php

/* * **************************************************************************
  Description: базовый компонент с полем Value
  Author: Ivanov Kirill
  Created: 09.07.2008
  Version: 1.0.0

  Changes info:
  = 09.07.2008	(Ivanov Kirill):	создан
  + 10.07.2008	(Ivanov Kirill):	Событие OnChange
  + 10.07.2008	(Ivanov Kirill):	AutoPost
 * ************************************************************************** */

namespace Mayral\Classes\VisualComponents;

class BasicValueComponent extends BasicStyleComponent
{

    protected $_Value="";

    public function __construct($_name, $_parent)
    {
        parent::__construct($_name, $_parent);
        $this->OnChange="EventOnChange";

        $this->SendProp["Value"]=true;
    }

    protected function BeforeGenerate()
    {
        parent::BeforeGenerate();
        //	заполнение массива переменный для подмены в шаблоне
        $this->PreGenerateVar["Value"]=$this->Value;
    }

    /*
      функция перехватывает присвоение значений членам класса
     */

    function __set($prop_name, $prop_value)
    {
        if($prop_name==="Value"||$prop_name==="ServerValue")
        {
            if($this->_Value!==$prop_value)
            {
                $this->OnSetValue($prop_value);
                $this->_Value=$prop_value;
                if($this->OnChange!=""&&$this->Parent!=="")
                {
                    $OnChange=$this->OnChange;
                    if(method_exists($this->GetParentForm(), $OnChange))
                    {
                        $this->GetParentForm()->$OnChange($this, 'onchange');
                    }
                    else
                    {
                        if(method_exists($this, $OnChange))
                        {
                            $this->$OnChange($this, 'onchange');
                        }
                        else
                        {
                            $this->SendGlobalEvent("Error", "Undefined function '".$OnChange."' called by '".$this->Name."'");
                        }
                    }
                }
                if(isset($this->SendProp[$prop_name])&&$this->SendProp[$prop_name]&&$prop_name==="Value")
                {
                    \Mayral\Classes\Basic\ObjectsPool::Create()->AddToChangedComponents($this);
                }
            }
        }
        else
        {
            parent::__set($prop_name, $prop_value);
        }
        return true;
    }

    public function OnSetValue($_value)
    {
        
    }

    /*
      функция перехватывает чтение значений членов класса
     */

    function __get($prop_name)
    {
        $result=parent::__get($prop_name);
        if($prop_name=="Value")
        {
            $result=$this->_Value;
        }
        return $result;
    }

    public function EventOnChange($_sender, $_event, $_event_args=null)
    {
        
    }

}

?>