<?php

/* * **************************************************************************
  Description: checkbox в стиле Windoes 8
  Author: Sergey Zinchenko
  Created: 10.08.2013
  Version: 1.0.0

  Changes info:
  = 10.08.2043	(Sergey Zinchenko):	создан
 * ************************************************************************** */

namespace Mayral\Classes\VisualComponents;

class BlockCheckBox extends \Mayral\Classes\VisualComponents\CheckBox
{

    public $Icon;

    public function __construct($_name, $_parent)
    {
        parent::__construct($_name, $_parent);

        $this->DeleteLastTemplate();
        $this->DeleteLastTemplate();
        $this->SetTemplateFileName('Templates/Components/'.'BlockCheckBox.html');
    }

    protected function BeforeGenerate()
    {
        parent::BeforeGenerate();

        $this->PreGenerateVar['Icon']=$this->Icon;
    }

}

?>