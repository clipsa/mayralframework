<?php

/* * **************************************************************************
  Description: список чекбоксов
  Author: Zinchenko Sergey
  Created: 11.08.2013
  Version: 1.0.0

  Changes info:
  = 11.08.2013	(Zinchenko Sergey):	создан
 * ************************************************************************** */

namespace Mayral\Classes\VisualComponents;

class BlockCheckListBox extends \Mayral\Classes\VisualComponents\CheckListBox
{

    public function AddItem($_key, $_value, $_icon='')
    {
        $this->Add($_value, false, $_key, $_icon);
    }

    public function Add($_item_text, $_checked=false, $_object='', $_icon='')
    {
        $index=$this->Items->Count();
        $CheckBox=new BlockCheckBox($this->Name.'BlockCheckBox'.$index, $this);
        $CheckBox->Text=$_item_text;
        $CheckBox->Icon=$_icon;
        $CheckBox->OnClick=$this->OnItemClick;
        $this->Items->Add($CheckBox);
        $this->Objects->Add($_object);
        \Mayral\Classes\Basic\ObjectsPool::Create()->AddToChangedComponents($CheckBox);
    }

}

?>