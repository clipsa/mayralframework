<?php

/* * **************************************************************************
  Description: список букетов с изменяемым порядком вывода
  Author: Zinchenko Sergey
  Version: 1.0.0

  Changes info:
  = 25.06.2012	(Zinchenko Sergey):	создан
 * ************************************************************************** */

namespace Mayral\Project\CMS\Components;

class BlockRepeater extends \Mayral\Classes\VisualComponents\GRepeater
{

    public function __construct($_name, $_parent)
    {
        parent::__construct($_name, $_parent);

        $this->DeleteLastTemplate();
        $this->SetTemplateFileName('Project/Components/BlockRepeater.html');
    }

    public function CustomEvent($_event_name)
    {
        $result=parent::CustomEvent($_event_name);

        if($_event_name=='onsaveorder')
        {
            $result='Event_SaveOrder_OnClick';
        }

        return $result;
    }

}

?>