<?php

/* * **************************************************************************
  Description: класс описания стиля границы
  Author: Ivanov Kirill
  Created: 11.07.2008
  Version: 1.0.0

  Changes info:
  = 11.07.2008	(Ivanov Kirill):	создан
 * ************************************************************************** */

namespace Mayral\Classes\VisualComponents;

class BorderStyle extends \Mayral\Classes\Basic\BasicClass
{

    public $Size;
    public $Type; //	Values:		none, hidden, dotted, dashed, solid, double, groove, ridge, inset, outset
    public $Color;

    public function __construct($_parent)
    {
        parent::__construct('', $_parent);
    }

    public function Generate()
    {
        $attr=new \Mayral\Classes\Basic\String($this);
        if($this->Size!='')
        {
            $attr->AddStringWithSeparator($this->Size.'px', ' ');
        }
        if($this->Type!='')
        {
            $attr->AddStringWithSeparator($this->Type, ' ');
        }
        if($this->Color!='')
        {
            $attr->AddStringWithSeparator($this->Color, ' ');
        }
        if($attr->Text!='')
        {
            $attr->Text='border:'.$attr->Text.';';
        }
        return $attr->Text;
    }

}

?>