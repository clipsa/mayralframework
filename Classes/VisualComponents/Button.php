<?php

/* * **************************************************************************
  Description: кнопка
  Author: Ivanov Kirill
  Created: 09.07.2008
  Version: 1.0.0

  Changes info:
  = 09.07.2008	(Ivanov Kirill):	создан
 * ************************************************************************** */

namespace Mayral\Classes\VisualComponents;

class Button extends BasicValueComponent
{

    var $Text;
    var $Image="";

    public function __construct($_name, $_parent)
    {
        parent::__construct($_name, $_parent);

        $this->Style->Overflow='hidden';
        $this->Style->TextAlign="center";
        $this->Style->Height="32px";
        $this->SetTemplateFileName("Templates/Components/"."Button.html");
    }

    protected function BeforeGenerate()
    {
        parent::BeforeGenerate();
        //	заполнение массива переменный для подмены в шаблоне
        $this->PreGenerateVar["Text"]=$this->Text;
        $ImageTag="";
        if($this->Image!=="")
        {
            $ImageTag="<img src='".$this->Image."' border=0 style='vertical-align: middle; margin-top: -5px;'>";
        }
        $this->PreGenerateVar["Image"]=$ImageTag;
    }

}

?>