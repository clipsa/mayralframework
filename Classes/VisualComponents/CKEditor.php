<?php

/* * **************************************************************************
  Description: editor CKEditor (http://docs.cksource.com/)
  Author: Zinchenko Sergey
  Version: 1.0.0

  Changes info:
  = 03.06.2012	(Zinchenko Sergey):	создан
 * ************************************************************************** */
namespace Mayral\Classes\VisualComponents;

class CKEditor extends BasicValueComponent
{

    public function __construct($_name, $_parent)
    {
        parent::__construct($_name, $_parent);
        $this->SetTemplateFileName("Templates/Components/"."CKEditor.html");
    }

}

?>