<?php

/* * **************************************************************************
  Description: checkbox
  Author: Ivanov Kirill
  Created: 10.07.2008
  Version: 1.0.0

  Changes info:
  = 10.07.2008	(Ivanov Kirill):	создан
 * ************************************************************************** */

namespace Mayral\Classes\VisualComponents;

class CheckBox extends BasicValueComponent
{

    public $Text;

    public function __construct($_name, $_parent)
    {
        parent::__construct($_name, $_parent);

        $this->SetTemplateFileName("Templates/Components/"."CheckBox.html");
    }

    protected function BeforeGenerate()
    {
        parent::BeforeGenerate();
        //	заполнение массива переменный для подмены в шаблоне
        $this->PreGenerateVar["Text"]=$this->Text;
        $this->PreGenerateVar["Checked"]="";
        if($this->Value==1)
        {
            $this->PreGenerateVar["Checked"]="checked";
        }
    }

    /*
      функция перехватывает присвоение значений членам класса
     */

    function __set($prop_name, $prop_value)
    {
        if($prop_name==="Value"||$prop_name==="ServerValue")
        {
            $prop_value=($prop_value===true)||($prop_value===1)||($prop_value==="1")||($prop_value==="true")||($prop_value==="checked")||($prop_value==="on");
        }
        return parent::__set($prop_name, $prop_value);
    }

}

?>