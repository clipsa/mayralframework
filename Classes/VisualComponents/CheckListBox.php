<?php

/* * **************************************************************************
  Description: checklistbox
  Author: Ivanov Kirill
  Created: 23.06.2009
  Version: 1.0.0

  Changes info:
  = 23.06.2009	(Ivanov Kirill):	создан
  + 05.12.2011	(Zinchenko Sergey): GetCheckedObjects
 * ************************************************************************** */

namespace Mayral\Classes\VisualComponents;

class CheckListBox extends BasicPanel
{

    protected $Items;
    protected $Objects;
    public $OnItemClick=""; //Событие: клик по чекбоксу

    public function __construct($_name, $_parent)
    {
        parent::__construct($_name, $_parent);
        $this->Items=new \Mayral\Classes\Lists\BasicList($this);
        $this->Objects=new \Mayral\Classes\Lists\BasicList($this);
        //$this->Style->BackGroundColor = "#fff";
        $this->Style->Border->Size="1";
        $this->Style->Border->Type="solid";
        $this->Style->Border->Color="#ccc";
        $this->Style->Overflow='auto';
    }

    public function Add($_item_text, $_checked=false, $_object="")
    {
        $index=$this->Items->Count();
        $CheckBox=new CheckBox($this->Name."CheckBox".$index, $this);
        $CheckBox->Value=$_checked;
        $CheckBox->Text=$_item_text;
        $CheckBox->Style->Left=5;
        $CheckBox->Style->Right=5;
        $CheckBox->Style->Top=5+15*$index;
        $CheckBox->Style->Position='relative';
        $CheckBox->OnClick=$this->OnItemClick;
        $this->Items->Add($CheckBox);
        $this->Objects->Add($_object);
        \Mayral\Classes\Basic\ObjectsPool::Create()->AddToChangedComponents($CheckBox);
    }

    public function AddItem($_key, $_value)
    {
        $this->Add($_value, false, $_key);
    }

    public function Clear()
    {
        $this->Items->Clear();
        $this->Objects->Clear();
        $this->Components->Clear();
    }

    public function Object($_index)
    {
        $Item=$this->Objects->Item($_index);
        if($Item!=="")
        {
            return $Item;
        }
        return "";
    }

    public function ItemText($_index, $_item_text="")
    {
        $Item=$this->Items->Item($_index);
        if($Item!=="")
        {
            if($_item_text!=="")
            {
                $Item->Text=$_item_text;
            }
            return $Item->Text;
        }
        return "";
    }

    public function Checked($_index, $_checked="")
    {
        $Item=$this->Items->Item($_index);
        if($Item!=="")
        {
            if($_checked!=="")
            {
                $Item->Value=$_checked;
            }
            return $Item->Value;
        }
        return "";
    }

    public function Count()
    {
        return $this->Items->Count();
    }

    public function GetIndexByName($_name)
    {
        $result=-1;

        for($i=0; $i<$this->Count(); $i++)
        {
            $checkbox=$this->Items->Item($i);
            if($checkbox->Name==$_name)
            {
                $result=$i;
                break;
            }
        }

        return $result;
    }

    public function GetCheckedObjects()
    {
        $result=array();

        for($i=0; $i<$this->Count(); $i++)
        {
            if($this->Checked($i))
            {
                $result[]=$this->Object($i);
            }
        }

        return $result;
    }

}

?>