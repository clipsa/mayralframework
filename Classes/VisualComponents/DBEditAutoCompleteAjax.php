<?php

/* * **************************************************************************
  Description: edit с аяксовым автозаполнением и привязкой к таблице БД
  Version: 1.0.0

  Changes info:
  = 07.09.2012    (Zinchenko Sergey):	создан
  - 06.09.2012    (Zinchenko Sergey): $Hash
 * ************************************************************************** */

namespace Mayral\Classes\VisualComponents;

class DBEditAutoCompleteAjax extends EditAutoCompleteAjax
{

    public $MinLength;
    public $TextFieldName;
    public $ValueFieldName;
    public $QueryAlias;
    public $OnGetData;

    public function __construct($_name, $_parent)
    {
        parent::__construct($_name, $_parent);
        $this->MinLength=3;
        $this->OnGetData='Event_OnGetData';
    }

    protected function BeforeGenerate()
    {
        parent::BeforeGenerate();
        $this->PreGenerateVar['MinLength']=$this->MinLength;
    }

    protected function CustomEvent($_event_name)
    {
        $result=parent::CustomEvent($_event_name);

        if($_event_name==='data_request')
        {
            $result=$this->OnGetData;
        }
        return $result;
    }

    protected function Event_OnGetData($_sender, $_event_name, $_args='')
    {
        $result=array();

        $ds=new \Mayral\Classes\DB\DBDataSet('', $this, \Mayral\Classes\DB\SQLConnection::GetInstance());
        $QueryText=\Project\Factory::GetInstance()->GetQueryCollection()->QueryText($this->QueryAlias, array('Filter'=>$this->TextFieldName.' LIKE '%'.$_args.'%''));
        $ds->Open($QueryText);
        if($ds->RowCount>0)
        {
            do
            {
                $result[]=array('label'=>$ds->GetFieldValue($this->TextFieldName), 'value'=>$ds->GetFieldValue($this->ValueFieldName), 'id'=>$ds->GetFieldValue('ID'));
            } while($ds->NextRow());
        }
        $this->SendGlobalEvent('ClientData', json_encode($result));
    }

}

?>
