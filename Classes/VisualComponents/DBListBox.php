<?php

/* * **************************************************************************
  Description: ListBox, берущий значения из датасета
  Author: Zinchenko Sergey
  Created: 13.03.2009
  Version: 2.0.0

  Changes info:
  = 13.03.2009	(Zinchenko Sergey):	создан
  = 15.03.2010	(Kecheor):			переделан
  + 14.09.2010	(Zinchenko Sergey): NullOption - первое значение в списке
 * ************************************************************************** */

namespace Mayral\Classes\VisualComponents;

class DBListBox extends ListBox
{

    /**
     * Первое значение в списке. Например 'не выбрано'
     *
     * @var string 
     *
     */
    public $NullOption;

    /**
     * Алиас поля, используемого как видимый текст
     *
     * @var string 
     *
     */
    public $TextFieldName;

    /**
     * Алиас поля, используемого как значение
     *
     * @var string 
     *
     */
    public $ValueFieldName;

    /**
     * Алиас запроса выбора из базы данных
     *
     * @var mixed 
     *
     */
    public $SelectQueryAlias;

    /**
     * Запрос выбора из базы данных
     *
     * @var mixed 
     *
     */
    public $SelectQuery;

    protected function BeforeGenerate()
    {
        if(strlen($this->ValueFieldName))
        {
            $this->GetItems();
        }
        parent::BeforeGenerate();
    }

    protected function GetItems()
    {
        $this->Clear();
        if(strlen($this->KeyFieldName)==0)
        {
            $this->KeyFieldName=$this->ValueFieldName;
        }
        $ds=new \Mayral\Classes\DB\DBDataSet('', $this, \Mayral\Classes\DB\SQLConnection::GetInstance());
        $QueryText=strlen($this->SelectQuery)?$this->SelectQuery:\Project\Factory::GetInstance()->GetQueryCollection()->QueryText($this->SelectQueryAlias);
        if($QueryText!='')
        {
            $ds->Open($QueryText);
            if($ds->RowCount>0)
            {
                if($this->NullOption!==null)
                    $this->AddItem('NULL', $this->NullOption);
                do
                {
                    $this->AddItem($ds->GetFieldValue($this->ValueFieldName), $ds->GetFieldValue($this->TextFieldName));
                } while($ds->NextRow());
            }
        }

        $j=0;
    }

}

?>