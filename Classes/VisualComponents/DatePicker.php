<?php

/* * **************************************************************************
  Description: edit, с заданием значения через ява календарик
  Author: Kecheor
  Created: 19.08.2008
  Version: 1.1.0

  Changes info:
  = 19.08.2008	(Kecheor):	создан
  + 13.12.2010	(Zinchenko Sergey): WTime - выбор времени
 * ************************************************************************** */

namespace Mayral\Classes\VisualComponents;

class DatePicker extends BasicValueComponent
{

    public $WTime=false;

    public function __construct($_name, $_parent)
    {
        parent::__construct($_name, $_parent);

        $this->SetTemplateFileName('Templates/Components/'.'DatePicker.html');
    }

    protected function BeforeGenerate()
    {
        parent::BeforeGenerate();
        $this->PreGenerateVar['WTime']=$this->WTime?'true':'false';
    }

}

?>