<?php

/* * **************************************************************************
  Description: дерево
  Author: Ivanov Kirill
  Version: 1.0.0

  Changes info:
  = 27.11.2009	(Ivanov Kirill):	создан
  + 03.12.2009	(Kecheor):			Refresh
  = 03.12.2009	(Ivanov Kirill):	Refresh = RefreshTree
 * ************************************************************************** */

namespace Mayral\Classes\VisualComponents;

class DirectoryTree extends Tree
{

    /**
     * путь до головной директории
     *
     * @var string 
     *
     */
    protected $Path;

    /**
     * текущая выбранная директория
     *
     * @var string 
     *
     */
    public $SelectedDirectory;

    public function __construct($_name, $_parent)
    {
        parent::__construct($_name, $_parent);

        $this->OnNodeClick='OnNodeClick';
    }

    /**
     * заполняет дерево списком директорий
     *
     * @return mixed This is the return value description
     *
     */
    protected function Fill()
    {
        clearstatcache();
        $this->Clear();
        $RootNode=new Node('Root', $this, $this->Path, 'Images/icons/folders_stack.png');
        $this->AddNode($RootNode, '');
        $this->FillSubDir($RootNode, $this->Path);
        $this->Value=0;
    }

    protected function FillSubDir($_parent_node, $_path)
    {
        $DirListing=scandir($_path);
        if($DirListing)
        {
            for($i=0; $i<count($DirListing); $i++)
            {
                $File=$DirListing[$i];
                if($File!=='.'&&$File!=='..')
                {
                    $FileFullPath=$_path.$File;
                    if(is_dir($FileFullPath))
                    {
                        $Node=new Node($File, $this, $FileFullPath, 'Images/icons/folder.png');
                        $this->AddNode($Node, $_parent_node);
                        $this->FillSubDir($Node, $FileFullPath.'/');
                    }
                }
            }
        }
    }

    /**
     * устанавливает путь до головной директории, вызывает заполнение дерева
     *
     * @param string $_path путь до головной директории
     *
     */
    public function SetPath($_path)
    {
        $this->Path=$_path;
        $this->SelectedDirectory=$this->Path;
        $this->Fill();
    }

    public function nNodeClick($_sender, $_event, $_event_args=null)
    {
        //
    }

    public function nSetValue($_value)
    {
        $Index=$_value;
        if(isset($this->AllNodes[$Index]))
        {
            $Node=$this->AllNodes[$Index];
            $this->SelectedDirectory=$Node->Object;
        }
    }

    public function RefreshTree()
    {
        $this->Fill();
    }

}

?>