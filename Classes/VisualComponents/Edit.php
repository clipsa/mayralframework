<?php

/* * **************************************************************************
  Description: простой edit
  Author: Ivanov Kirill
  Created: 09.07.2008
  Version: 1.0.1

  Changes info:
  = 09.07.2008	(Ivanov Kirill):	создан
  + 26.01.2013    (Zinchenko Sergey): $Helper
  - 13.11.2013	(Zinchenko Sergey): $Helper
 * ************************************************************************** */

namespace Mayral\Classes\VisualComponents;

class Edit extends BasicValueComponent
{

    public $Type;
    public $Mask='';
    public $KeyFilter='';
    public $AutoFocus=0;

    public function __construct($_name, $_parent)
    {
        parent::__construct($_name, $_parent);
        $this->Type='text';
        $this->SetTemplateFileName('Templates/Components/'.'Edit.html');
    }

    protected function BeforeGenerate()
    {
        parent::BeforeGenerate();
        //	заполнение массива переменный для подмены в шаблоне
        $this->PreGenerateVar['Type']=$this->Type;
        $this->PreGenerateVar['Mask']=$this->Mask;
        $this->PreGenerateVar['Helper']=($this->Type=='password'?'btn-reveal':'btn-clear');
        $this->PreGenerateVar["KeyFilter"]=$this->KeyFilter;
        $this->PreGenerateVar["AutoFocus"]=$this->AutoFocus;
    }

}

?>