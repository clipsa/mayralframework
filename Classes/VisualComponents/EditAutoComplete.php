<?php

/* * **************************************************************************
  Description: edit с автозаполнением
  Version: 1.0.0

  Changes info:
  = 07.09.2011	(Ivanov Kirill):	создан
 * ************************************************************************** */
namespace Mayral\Classes\VisualComponents;

class EditAutoComplete extends Edit
{

    public $AutoCompleteValue;

    public function __construct($_name, $_parent)
    {
        parent::__construct($_name, $_parent);
        $this->DeleteLastTemplate();
        $this->SetTemplateFileName('Templates/Components/'.'EditAutoComplete.html');
    }

    protected function BeforeGenerate()
    {
        parent::BeforeGenerate();
        //	заполнение массива переменный для подмены в шаблоне
        $this->AutoCompleteValue[]='кирилл';
        $this->PreGenerateVar['AutoCompleteArray']='[\''.implode('','', $this->AutoCompleteValue).'\']';
    }

}

?>