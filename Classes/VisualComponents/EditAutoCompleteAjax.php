<?php

/* * **************************************************************************
  Description: edit с аяксовым автозаполнением
  Version: 1.0.0

  Changes info:
  = 07.09.2011	(Ivanov Kirill || Alexander Sokolov):	создан
 * ************************************************************************** */

namespace Mayral\Classes\VisualComponents;

class EditAutoCompleteAjax extends EditAutoComplete
{

    public $ListClass='';
    public $javascript_OnDrawItem;
    public $SuggestLimit;
    public $MinLength;
    public $OnSelect;
    protected $Data;
    protected $ReadOnly=false;

    public function __construct($_name, $_parent)
    {
        parent::__construct($_name, $_parent);
        $this->DeleteLastTemplate();
        $this->SetTemplateFileName('Templates/Components/'.'EditAutoCompleteAjax.html');
        $this->javascript_OnDrawItem='0';
        $this->SuggestLimit=10;
        $this->MinLength=3;
    }

    protected function BeforeGenerate()
    {
        parent::BeforeGenerate();
        $this->PreGenerateVar['ListClass']=$this->ListClass;
        $this->PreGenerateVar['OnSelect']='ServerArgEvent(\'{$this->FullName()}\', \'OnSelect\', $(\'#{$this->FullName()}\').data(\'onchange_data\'));';
        $this->PreGenerateVar['javascript_OnDrawItem']=$this->javascript_OnDrawItem;
        $this->PreGenerateVar['SuggestLimit']=$this->SuggestLimit;
        $this->PreGenerateVar['MinLength']=$this->MinLength;
        if($this->ReadOnly)
        {
            $this->PreGenerateVar['Disabled']='disabled=\'disabled\'';
        }
    }

    public function SetReadOnly($_value=true)
    {
        $this->ReadOnly=$_value;
    }

    public function GetDataProperty($_name)
    {
        return isset($this->Data[$_name])?$this->Data[$_name]:null;
    }

    public function SetDataProperty($_name, $_value)
    {
        $this->Data[$_name]=$_value;
    }

    protected function CustomEvent($_event_name)
    {
        $result=parent::CustomEvent($_event_name);
        if($_event_name==='OnSelect')
        {
            return 'OnSelect';
        }
        return $result;
    }

    public function nSelect($_sender, $_event, $_args=null)
    {
        $this->Data=$_args?$_args:array();
        $this->Value=isset($_args['label'])?$_args['label']:'';
        //элемент выбран - вызываем внешний OnSelect, по сути AfterSelect
        if($this->OnSelect!=''&&method_exists($this->GetParentForm(), $this->OnSelect))
        {
            $this->GetParentForm()->{$this->OnSelect}($_sender, $_event, $_args);
        }
    }

}

?>