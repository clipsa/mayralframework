<?php

/* * **************************************************************************
  Description: ElFinder (beta)
  Author: Zinchenko Sergey
  Version: 0.1.0 beta

  Changes info:
  = 12.06.2012	(Zinchenko Sergey):	создан
  + 31.07.2012	(Zinchenko Sergey): событие
 * ************************************************************************** */

namespace Mayral\Classes\VisualComponents;

class ElFinder extends BasicValueComponent
{

    public $Path='Content/';
    public $global_event_name='files_selected_elfinder';

    public function __construct($_name, $_parent)
    {
        parent::__construct($_name, $_parent);

        $this->SetTemplateFileName('Templates/Components/'.'ElFinder.html');
    }

    protected function BeforeGenerate()
    {
        parent::BeforeGenerate();

        $this->PreGenerateVar['Path']=$this->Path;
    }

    protected function CustomEvent($_event_name)
    {
        $result=parent::CustomEvent($_event_name);

        if($_event_name=='files_selected')
        {
            $result='Event_FilesSelected';
        }
        return $result;
    }

    protected function Event_FilesSelected($_sender, $_event_name, $_args='')
    {
        $this->SendGlobalEvent($this->global_event_name, $_args);
    }

}

?>