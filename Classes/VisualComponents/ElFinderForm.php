<?php

/* * **************************************************************************
  Description: форма с элфайндером
  Author: Zinchenko Sergey
  Version: 1.0.0

  Changes info:
  = 31.07.2012	(Zinchenko Sergey):	создан
 * ************************************************************************** */
namespace Mayral\Classes\VisualComponents;

class ElFinderForm extends Form implements \Mayral\Classes\Interfaces\IActionListener
{

    public $global_event_name;

    public function __construct($_name, $_parent, $_global_event_name)
    {
        parent::__construct($_name, $_parent);

        $this->global_event_name=$_global_event_name;

        $this->Style->Width=800;
        $this->Style->Height=800;

        $elfinder=new ElFinder('finder', $this);
        $elfinder->Style->Top=10;
        $elfinder->Style->Left=10;
        $elfinder->Style->Right=10;
        $elfinder->Style->Bottom=50;
        $elfinder->global_event_name=$_global_event_name;

        $button_ok=new Button('button_ok', $this);
        $button_ok->Style->Bottom=10;
        $button_ok->Style->Right=120;
        $button_ok->Style->Width=100;
        $button_ok->Text='Ok';
        $button_ok->javascript_OnClick='SendFiles();';

        $button_ok=new Button('button_cancel', $this);
        $button_ok->Style->Bottom=10;
        $button_ok->Style->Right=10;
        $button_ok->Style->Width=100;
        $button_ok->Text='Отмена';
        $button_ok->OnClick='Close';
    }

    public function Work()
    {
        $this->SetCenterPosition();
        $this->ShowModal();
    }

    public function ActionPerform($_event_name, $_event_argument)
    {
        if($_event_name===$this->global_event_name)
        {
            $this->Close();
        }
    }

}

?>