<?php

/* * **************************************************************************
  Description: список файлов
  Author: Kecheor
  Version: 1.0.0

  Changes info:
  = 27.11.2009	(Ivanov Kirill):	создан
 * ************************************************************************** */

namespace Mayral\Classes\VisualComponents;

class FileBox extends SPRepeater
{

    protected $_Value='none';
    protected $Path;
    protected $ExtensionIcon;
    protected $Extension;
    public $SelectedFile;
    public $SelectedFileUrl;

    public function __construct($_name, $_parent)
    {
        parent::__construct($_name, $_parent);
        $this->SetTemplateFileName('Templates/Components/'.'FileBox_list.html');
        $this->ItemsPerPage=0;

        $this->Extension=array();
        $this->ExtensionIcon=array();

        $this->AddExtensionIcon('gif, bmp, jpg, jpeg, jpe, ico , png', 'Images/icons/image.png');
        $this->AddExtensionIcon('doc, docx', 'Images/icons/document_word.png');
        $this->AddExtensionIcon('xls, xlsx', 'Images/icons/document_excel.png');
        $this->AddExtensionIcon('txt', 'Images/icons/document_text.png');
        $this->AddExtensionIcon('html, htm', 'Images/icons/document_text_image.png');
        $this->AddExtensionIcon('php', 'Images/icons/script_code.png');
    }

    public function AddExtensionIcon($_ext, $_icon)
    {
        $Ext=explode(',', $_ext);
        foreach($Ext as $ext)
        {
            $ext=trim($ext);
            $this->ExtensionIcon[$ext]=$_icon;
        }
    }

    public function AddExtension($_ext)
    {
        $Ext=explode(',', $_ext);
        foreach($Ext as $ext)
        {
            $ext=trim($ext);
            $this->Extension[$ext]=true;
        }
    }

    /**
     * устанавливает путь, заполняет список файлов
     *
     */
    public function SetPath($_path)
    {
        $this->Path=$_path;
        $this->Fill();
        \Mayral\Classes\Basic\ObjectsPool::Create()->AddToChangedComponents($this);
    }

    protected function Fill()
    {
        $this->SelectedFile='';
        $this->SelectedFileUrl='';
        $this->_Value='none';
        $this->Items->Clear();
        $Path=$this->Path;
        $DirListing=scandir($Path);
        if($DirListing)
        {
            for($i=0; $i<count($DirListing); $i++)
            {
                $File=$DirListing[$i];
                if($File!=='.'&&$File!=='..')
                {
                    $FileFullPath=$Path.(substr($Path, strlen($Path)-1, 1)=='/'?$File:'/'.$File);
                    if(is_file($FileFullPath))
                    {
                        $path_info=pathinfo($FileFullPath);
                        $Ext='';
                        if(isset($path_info['extension']))
                        {
                            $Ext=$path_info['extension'];
                        }
                        if(count($this->Extension)==0||isset($this->Extension[$Ext]))
                        {
                            $Item='';
                            $Item->Property=array('FileName'=>$File, 'Icon'=>$this->GetFileIcon($FileFullPath), 'FileFullName'=>$FileFullPath, 'FileSize'=>(ceil(filesize($FileFullPath)/1024)));
                            $this->Items->Add($Item);
                        }
                    }
                }
            }
        }
    }

    protected function GetFileIcon($_file_name)
    {
        $Result='Images/icons/document.png';
        $path_info=pathinfo($_file_name);
        if(isset($path_info['extension']))
        {
            $Ext=$path_info['extension'];
            if(isset($this->ExtensionIcon[$Ext]))
            {
                $Result=$this->ExtensionIcon[$Ext];
            }
        }
        return $Result;
    }

    //	действия перед генерацией компонента
    protected function BeforeGenerate()
    {
        parent::BeforeGenerate();
        //	заполнение массива переменный для подмены в шаблоне
        $this->PreGenerateVar['Value']=$this->_Value;
    }

    /*
      функция перехватывает присвоение значений членам класса
     */

    function __set($prop_name, $prop_value)
    {
        if($prop_name==='Value'||$prop_name==='ServerValue')
        {
            if($this->_Value!==$prop_value)
            {
                $this->_Value=$prop_value;

                $Item=$this->Items->Item($prop_value);
                if($Item!=='')
                {
                    $this->SelectedFile=$Item->PropertyValue('FileFullName');

                    $FileFullName=$Item->PropertyValue('FileFullName');
                    $FileFullName=str_replace($_SERVER['DOCUMENT_ROOT'], '', $FileFullName);
                    $this->SelectedFileUrl=$FileFullName;
                }
            }
        }
        else
        {
            parent::__set($prop_name, $prop_value);
        }
        return true;
    }

}

?>