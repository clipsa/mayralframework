<?php

/* * **************************************************************************
  Description: компонент файл-менеджер
  Author: Ivanov Kirill
  Version: 1.0.0

  Changes info:
  = 27.11.2009	(Ivanov Kirill):	создан
 * ************************************************************************** */
namespace Mayral\Classes\VisualComponents;

class FileExplorer extends Panel
{

    public function __construct($_name, $_parent)
    {
        parent::__construct($_name, $_parent);

        $DirectoryTree=new DirectoryTree('DirectoryTree', $this);
        $DirectoryTree->Style->Top='5';
        $DirectoryTree->Style->Left='5';
        $DirectoryTree->Style->Bottom='5';
        $DirectoryTree->Style->Width='250';
        $DirectoryTree->OnNodeClick='OnNodeClick';

        $FileBox=new FileBox('FileBox', $this);
        $FileBox->Style->Top='5';
        $FileBox->Style->Left='260';
        $FileBox->Style->Bottom='5';
        $FileBox->Style->Right='5';

        $this->IsForm=true;
    }

    public function SetPath($_path)
    {
        $this->DirectoryTree->SetPath($_path);
        $this->FileBox->SetPath($_path);
    }

    public function nNodeClick($_sender, $_event, $_event_args=null)
    {
        $this->DirectoryTree->OnNodeClick($_sender, $_event, $_event_args);
        $this->RefreshFileBox();
    }

    public function RefreshFileBox()
    {
        $this->FileBox->SetPath($this->DirectoryTree->SelectedDirectory);
    }

}

?>