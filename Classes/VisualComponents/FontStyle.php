<?php

/* * **************************************************************************
  Description: класс описания стиля шрифта
  Author: Ivanov Kirill
  Created: 11.07.2008
  Version: 1.0.0

  Changes info:
  = 11.07.2008	(Ivanov Kirill):	создан
 * ************************************************************************** */

namespace Mayral\Classes\VisualComponents;

use Mayral\Classes\Basic;

class FontStyle extends Basic\BasicClass
{

    public $Style;
    public $Varint;
    public $Weight;
    public $Size;
    public $Height;
    public $Family;

    public function __construct($_parent)
    {
        parent::__construct('', $_parent);
    }

    public function Generate()
    {
        $attr=new Basic\String($this);
        if($this->Style!='')
        {
            $attr->AddStringWithSeparator($this->Style, ' ');
        }
        if($this->Varint!='')
        {
            $attr->AddStringWithSeparator($this->Varint, ' ');
        }
        if($this->Weight!='')
        {
            $attr->AddStringWithSeparator($this->Weight, ' ');
        }
        if($this->Size!='')
        {
            $attr->AddStringWithSeparator($this->Size.'px', ' ');
        }
        if($this->Height!='')
        {
            $attr->AddStringWithSeparator($this->Height, ' ');
        }
        if($this->Family!='')
        {
            $attr->AddStringWithSeparator($this->Family, ' ');
        }


        if($attr->Text!='')
        {
            $attr->Text='font:'.$attr->Text.';';
        }

        return $attr->Text;
    }

}

?>