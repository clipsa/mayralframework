<?php

/* * **************************************************************************
  Description: базовая форма
  Version: 1.0.0

  Changes info:
  = 22.07.2008	(Ivanov Kirill):	создан
  + 2008        (Kecheor):              добавлена генерация из xml
  + 11.12.2008	(Ivanov Kirill):	Show(), Hide()
  + 22.06.2008	(Ivanov Kirill):	IconSrc
  + 22.06.2008	(Ivanov Kirill):	Close() прячет и удаляет форму
 * ************************************************************************** */

namespace Mayral\Classes\VisualComponents;

class Form extends BasicPanel
{

    public $Draggable = false;
    public $TitleHeight = '';
    public $IconSrc = 'Images/icons/application_form.png';
    protected $ComponentContainerOuterClass;
    public $CloseListeners;

    public function __construct($_name, $_parent)
    {
        parent::__construct($_name, $_parent);
        $this->IsForm = true;
        $this->Style->Padding = '0';
        $this->Style->Margin = '0';

        $this->Class = 'Form';
        $this->DeleteLastTemplate();
        $this->SetTemplateFileName('Templates/Components/' . 'Form.html');
        $this->Visible = false;

        $CloseButton = new Image('CloseButton', $this);
        $CloseButton->Src = 'Images/components/Form/iconClose.gif';
        $CloseButton->Style->Position = 'static';
        $CloseButton->Style->Cursor = 'pointer';
        $CloseButton->OnClick = 'CloseButton_OnClick';

        $this->CloseListeners = new \Mayral\Classes\Listeners\Observable();
    }

    protected function BeforeGenerate()
    {
        parent::BeforeGenerate();
        //	заполнение массива переменный для подмены в шаблоне
        $this->PreGenerateVar['BodyTop'] = '';
        if ($this->TitleHeight !== '')
        {
            $this->PreGenerateVar['BodyTop'] = 'top:' . $this->TitleHeight . ';';
        }
        $this->PreGenerateVar['ComponentContainerOuterClass'] = $this->ComponentContainerOuterClass;
        $this->PreGenerateVar['IconSrc'] = $this->IconSrc;
        $this->PreGenerateVar['Draggable'] = $this->Draggable ? 'true' : 'false';
    }

    public function Show()
    {
        $this->ComponentContainerOuterClass = '';
        $this->Visible = true;
    }

    public function ShowModal()
    {
        $this->ComponentContainerOuterClass = 'Form_ModalOuter';
        $this->Class = 'window flat shadow';
        $this->Visible = true;
    }

    public function Hide()
    {
        $this->ComponentContainerOuterClass = '';
        $this->Visible = false;
    }

    public function SetCenterPosition()
    {
        $Width = $this->Style->Width;
        $Height = $this->Style->Height;

        $this->Style->Top = '50%';
        $this->Style->Left = '50%';

        $this->Style->Margin = '-' . ($Height / 2) . 'px -' . ($Width / 2) . 'px';
    }

    public function CloseButton_OnClick($_sender, $_event, $_event_args = null)
    {
        $this->Close();
    }

    public function Close()
    {
        $this->MarkToKill();
        $this->CloseListeners->Notify($this);
    }

    public function MarkToKill()
    {
        parent::MarkToKill();
        $this->Hide();
    }

}

?>
