<?php

/* * **************************************************************************
  Description: контроллер форм
  Author: Ivanov Kirill
  Created: 19.06.2009
  Version: 1.0.1

  Changes info:
  = 19.06.2009	(Ivanov Kirill):	создан
  + 27.01.2013  (Zinchenko Sergey):     CloseAll
 * ************************************************************************** */

namespace Mayral\Classes\VisualComponents;

class FormControllerFormInfo
{

    public $Property = array();
    public $Form;

}

class FormController extends BasicRepeater implements \Mayral\Classes\Interfaces\IObserver
{

    public $Panel;
    public $AllowDublicate = false;
    private $PrevTabIndex;

    public function __construct($_name, $_parent)
    {
        parent::__construct($_name, $_parent);
        $this->SetTemplateFileName('Templates/Components/' . 'FormController.html');
        $this->Style->Overflow = 'auto';
    }

    public function SetActiveForm($_form_name)
    {
        $FormIndex = $this->FormIndex($_form_name);
        if ($FormIndex != -1)
        {
            $this->HideAll();
            $Form = $this->Items->Item($FormIndex)->Form;
            $Form->Visible = true;
            \Mayral\Classes\Basic\ObjectsPool::Create()->AddToChangedComponents($this);
        }
        return ($FormIndex != -1);
    }

    public function Add(Form $_form)
    {
        if ($this->AllowDublicate || !$this->IsFormExist($_form->Name))
        {
            $_form->CloseButton->Visible = false;
            $_form->TitleHeight = '0';

            for ($i = 0; $i < $this->Items->Count(); $i++)
            {
                $Form = $this->Items->Item($i)->Form;
                if ($Form->Visible)
                {
                    $this->PrevTabIndex = $i;
                }
            }

            $_form->CloseListeners->AddObserver($this);

            $this->HideAll();
            $Item = new FormControllerFormInfo();
            $Item->Form = $_form;
            $Item->Property['Title'] = $_form->Title;
            $Item->Property['IconSrc'] = $_form->IconSrc;
            $Item->Property['BGIMAGE'] = 'Images/button_bg.jpg';
            $Item->Property['FONTCOLOR'] = '#fff';

            $this->Items->Add($Item);

            \Mayral\Classes\Basic\ObjectsPool::Create()->AddToChangedComponents($this);
        }
    }

    public function FormIndex($_form_name)
    {
        for ($i = 0; $i < $this->Items->Count(); $i++)
        {
            $Form = $this->Items->Item($i)->Form;
            if ($Form->Name === $_form_name)
            {
                return $i;
            }
        }
        return -1;
    }

    public function IsFormExist($_form_name)
    {
        return ($this->FormIndex($_form_name) != -1);
    }

    public function CloseTab($_index)
    {
        $Item = $this->Items->Item($_index);
        if ($Item !== '')
        {
            $this->HideAll();
            $this->Items->Del($_index);
            if (!$Item->Form->KillMark)
            {
                $Item->Form->Close();
            }
            \Mayral\Classes\Basic\ObjectsPool::Create()->AddToChangedComponents($this);

            $this->ShowTab(is_null($this->PrevTabIndex) ? $_index : $this->PrevTabIndex);
        }
    }

    protected function ShowTab($_index)
    {
        $NextItem = $this->Items->Item($_index);
        if ($NextItem !== '')
        {
            $NextItem->Form->Show();
        }
        else
        {
            $PrevItem = $this->Items->Item($_index - 1);
            if ($PrevItem !== '')
            {
                $PrevItem->Form->Show();
            }
        }
    }

    public function HideAll()
    {
        for ($i = 0; $i < $this->Items->Count(); $i++)
        {
            $Form = $this->Items->Item($i)->Form;
            //if ($Form->Visible)
            //{
            $Form->Visible = false;
            //}
        }
    }

    public function CloseAll()
    {
        for ($i = 0; $i < $this->Items->Count(); $i++)
        {
            $Form = $this->Items->Item($i)->Form;

            $Form->Close();
        }
    }

    protected function PrepareItemValue($_index, $_name, $_value)
    {
        if ($_name === 'Title' && $this->Items->Item($_index)->Form->Visible)
        {
            $_value = '<b>' . $_value . '</b>';
        }
        if ($_name === 'BGCOLOR' && $this->Items->Item($_index)->Form->Visible)
        {
            $_value = '#92a1b7';
        }
        if ($_name === 'BGIMAGE' && $this->Items->Item($_index)->Form->Visible)
        {
            $_value = 'Images/big_title_4.jpg';
        }
        if ($_name === 'FONTCOLOR' && $this->Items->Item($_index)->Form->Visible)
        {
            $_value = '#fff';
        }

        return $_value;
    }

    public function Update($_data, $_observable)
    {
        $this->CloseTab($this->FormIndex($_data->Name));
    }

}

?>