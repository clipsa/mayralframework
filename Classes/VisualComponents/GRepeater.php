<?php

/* * **************************************************************************
  Description: Репитер c колонками и кнопками из XML
  Author: Kecheor
  Version: 1.0.1

  Changes info:
  = 04.12.2009    (Kecheor):	создан
  = 16.02.2011    (Zinchenko Sergey): в GenValueColumns в шаблон подставляется INDEX_J(название колонки).
  + 16.09.2012    (Zinchenko Sergey): Clear()
 * ************************************************************************** */

namespace Mayral\Classes\VisualComponents;

class GRepeater extends SPRepeater
{

    public $Columns;
    public $RowActionButtons;
    protected $ColumnHeaderTemplate;
    protected $ColumnHeaderSortNoneTemplate;
    protected $ColumnHeaderSortUpTemplate;
    protected $ColumnHeaderSortDownTemplate;
    protected $ColumnValueTemplate;
    protected $ActionButtonTemplate;
    public $javascript_OnEditItemClick='';
    public $OnEditItemClick='';
    public $javascript_OnDeleteItemClick='';
    public $OnDeleteItemClick='';

    public function __construct($_name, $_parent)
    {
        parent::__construct($_name, $_parent);

        $this->ColumnHeaderTemplate=new \Mayral\Classes\Basic\String($this);

        $this->Columns=array();
        $this->RowActionButtons=array();
    }

    /**
     * удаление кнопки
     *
     * @param string $_event_name название события
     * @return void void
     *
     */
    public function RemoveRowActionButton($_event_name)
    {
        foreach($this->RowActionButtons as $key=> $object)
        {
            if($object->Event==$_event_name)
            {
                unset($this->RowActionButtons[$key]);
                break;
            }
        }
    }

    public function CustomXML($_xml)
    {
        $this->GetColumns($_xml->xpath('Columns/Column'));
        $this->GetActionButtons($_xml->xpath('Buttons/ActionButton'));
    }

    protected function GetColumns($_colnodes)
    {
        foreach($_colnodes as $column)
        {
            $RCol=new RepeaterColumn($this->PreparePropertyValue($column['ViewName']), $this->PreparePropertyValue($column['ValueName']), $this->PreparePropertyValue($column['Sortable'])=='true'?true:false, $this->PreparePropertyValue($column['Sort']));

            $this->AddColumn($RCol);
        }
    }

    public function AddColumn($_col)
    {
        $this->Columns[]=$_col;
    }

    public function AddColumnToHead($_col)
    {
        $min=min(array_keys($this->Columns));
        $this->Columns[$min]=$_col;
    }

    public function RemoveColumn($_view_name)
    {
        for($i=0; $i<count($this->Columns); $i++)
        {
            if($this->Columns[$i]->ViewName==$_view_name)
            {
                unset($this->Columns[$i]);
            }
        }
    }

    protected function GetActionButtons($_btnnodes)
    {
        foreach($_btnnodes as $btn)
        {
            $RBtn=new RepeaterActionButton($this->PreparePropertyValue($btn['Event']), $this->PreparePropertyValue($btn['Icon']), $this->PreparePropertyValue($btn['Title']), $this->PreparePropertyValue($btn['javascript_OnClick']));

            $this->RowActionButtons[]=$RBtn;
        }
    }

    protected function BeforeGenerate()
    {
        parent::BeforeGenerate();

        $this->PreGenerateVar['CustomColumns']=$this->GenColumnHeaders();
    }

    /**
     * Парсинг шаблонов страниц
     *
     * @return OString Основной шаблон таблицы
     *
     */
    protected function LoadTemplate($_type)
    {

        $result=new \Mayral\Classes\Basic\String($this);
        $result->Text=parent::LoadTemplate($_type);

        $this->ColumnHeaderTemplate->Text=$result->GetChangeTextBetween('<!--ColHeader-->', '<!--/ColHeader-->', '{CustomColumns}');
        $this->ColumnHeaderSortNoneTemplate=$this->ColumnHeaderTemplate->CutTextBetween('<!--SortedNone-->', '<!--/SortedNone-->');
        $this->ColumnHeaderSortUpTemplate=$this->ColumnHeaderTemplate->CutTextBetween('<!--SortedUp-->', '<!--/SortedUp-->');
        $this->ColumnHeaderSortDownTemplate=$this->ColumnHeaderTemplate->CutTextBetween('<!--SortedDown-->', '<!--/SortedDown-->');

        $ItemTemplate=new \Mayral\Classes\Basic\String($this);
        $ItemTemplate->Text=$this->ItemTemplate;
        $this->ActionButtonTemplate=$ItemTemplate->GetChangeTextBetween('<!--ActionButton-->', '<!--/ActionButton-->', '{CustomActions}');
        $this->ColumnValueTemplate=$ItemTemplate->GetChangeTextBetween('<!--ValColumn-->', '<!--/ValColumn-->', '{CustomColumns}');


        $ItemTemplate->Text=str_replace('{CustomActions}', $this->GenActionButtons(), $ItemTemplate->Text);
        $ItemTemplate->Text=str_replace('{CustomColumns}', $this->GenValueColumns(), $ItemTemplate->Text);
        $this->ItemTemplate=$ItemTemplate->Text;
        return $result->Text;
    }

    protected function GenerateItem($_index)
    {
        $result=parent::GenerateItem($_index);
        $result=str_replace('{HEADER}', @$this->Columns[$_index]->ViewName, $result);
        return $result;
    }

    protected function GenColumnHeaders()
    {
        $CustomColumns='';
        foreach($this->Columns as $colid=> $colitem)
        {
            $col=$this->ColumnHeaderTemplate->Text;
            $col=str_replace('{HEADER}', $colitem->ViewName, $col);

            $sortselect='';
            if($colitem->Sortable)
            {

                switch($colitem->Sort)
                {
                    case 'up':
                        $sortselect=$this->ColumnHeaderSortUpTemplate;
                        break;
                    case 'down':
                        $sortselect=$this->ColumnHeaderSortDownTemplate;
                        break;
                    default:
                        $sortselect=$this->ColumnHeaderSortNoneTemplate;
                        break;
                }
                $sortselect=str_replace('{SORTBY}', $colid, $sortselect);
            }

            $col=str_replace('{SORTING}', $sortselect, $col);
            $col=str_replace('{Name}', $this->ClientName, $col);
            $CustomColumns .= $col;
        }
        return $CustomColumns;
    }

    protected function GenActionButtons()
    {
        $RowActions='';
        foreach($this->RowActionButtons as $item)
        {
            $btn=$this->ActionButtonTemplate;
            $btn=str_replace('{EVENT}', $item->Event, $btn);
            $btn=str_replace('{ICON}', $item->Icon, $btn);
            $btn=str_replace('{TITLE}', $item->Title, $btn);
            $btn=str_replace('{javascript_OnClick}', $item->javascript_OnClick, $btn);
            $RowActions .= $btn;
        }
        return $RowActions;
    }

    protected function GenValueColumns()
    {
        $ValueColumns='';
        foreach($this->Columns as $colid=> $colitem)
        {
            $col=$this->ColumnValueTemplate;
            $col=str_replace('{VALUE}', '{'.$colitem->ValueName.'}', $col);
            $col=str_replace('{INDEX_J}', $colitem->ValueName, $col);
            $ValueColumns .= $col;
        }
        return $ValueColumns;
    }

    /**
     * Метод для сортировки записей в таблице
     *
     * @param mixed $_field Идентификатор колонки
     * @param bool $_up По возрастанию/по убыванию
     *
     */
    public function Sort($_field, $_up=true)
    {
        $sortfield=$this->GetColumnById($_field);
        parent::Sort($sortfield, $_up);
    }

    public function GetColumnById($_id)
    {
        $result='';
        if(strpos($_id, 'c:')===0)
        {
            $sortcol=substr($_id, 2);
            if(array_key_exists($sortcol, $this->Columns))
            {
                $result=$this->Columns[$sortcol]->ValueName;
            }
        }
        return $result;
    }

    public function CustomEvent($_event_name)
    {
        $result=parent::CustomEvent($_event_name);
        if($_event_name==='onitemclick')
        {
            $result=$this->OnItemClick;
        }
        if($_event_name==='onedititemclick')
        {
            $result=$this->OnEditItemClick;
        }
        if($_event_name==='ondeleteitemclick')
        {
            $result=$this->OnDeleteItemClick;
        }
        if($result=='')
        {
            $parent_method=$this->Parent->CustomEvent($_event_name);
            if($parent_method!='')
                $result=$parent_method;
        }

        return $result;
    }

    public function Clear()
    {
        $this->Items->Clear();
    }

}

?>