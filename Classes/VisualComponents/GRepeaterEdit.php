<?php

/* * **************************************************************************
  Description: Репитер c редактируемыми полями
  Author: Zinchenko Sergey
  Version: 1.1.0

  Changes info:
  = 30.05.2012	(Zinchenko Sergey): создан
  = 12.08.2012        (Zinchenko Sergey): переработан
 * ************************************************************************** */

namespace Mayral\Classes\VisualComponents;

class GRepeaterEdit extends GRepeater
{

    public $ShowAways = true;
    public $Appendable = true;
    public $FullyEditable = true;

    public function __construct($_name, $_parent)
    {
        parent::__construct($_name, $_parent);
//        $this->DeleteLastTemplate();
        $this->SetTemplateFileName("Templates/Components/" . "GRepeaterEditable.html");
    }

    protected function BeforeGenerate()
    {
        parent::BeforeGenerate();

        $this->Appendable = ($this->Appendable === 'true' || $this->Appendable === true || $this->Appendable == 1) ? true : false;

        $this->PreGenerateVar["ShowAlways"] = $this->ShowAlways;
        $this->PreGenerateVar["FullyEditable"] = $this->FullyEditable;
    }

    protected function GetColumns($_colnodes)
    {
        foreach ($_colnodes as $column)
        {
            switch (strval($column['EditType']))
            {
                case 'autocomplete':
                    $RCol = new GRepeaterAutocompleteColumn($this->PreparePropertyValue($column['ViewName']), $this->PreparePropertyValue($column['ValueName']), $this->PreparePropertyValue($column['Sortable']) == 'true', $this->PreparePropertyValue($column['Sort']));
                    $RCol->LabelName = $this->PreparePropertyValue($column['LabelName']);
                    $RCol->QueryAlias = $this->PreparePropertyValue($column['QueryAlias']);
                    $RCol->DependentColumns = $this->GetDependentColumns($column);
                    break;
                case 'listbox':
                case 'select':
                    $RCol = new GRepeaterListboxColumn($this->PreparePropertyValue($column['ViewName']), $this->PreparePropertyValue($column['ValueName']), $this->PreparePropertyValue($column['Sortable']) == 'true', $this->PreparePropertyValue($column['Sort']));
                    $RCol->DependentColumns = $this->GetDependentColumns($column);
                    $RCol->Items = $this->GetItems($column);
                    break;
                case 'edit':
                    $RCol = new GRepeaterEditColumn($this->PreparePropertyValue($column['ViewName']), $this->PreparePropertyValue($column['ValueName']), $this->PreparePropertyValue($column['Sortable']) == 'true', $this->PreparePropertyValue($column['Sort']));
                    break;
                default:
                    $RCol = new GRepeaterColumn($this->PreparePropertyValue($column['ViewName']), $this->PreparePropertyValue($column['ValueName']), $this->PreparePropertyValue($column['Sortable']) == 'true', $this->PreparePropertyValue($column['Sort']));
            }


            $this->AddColumn($RCol);
        }
    }

    protected function GetDependentColumns($_xml_data)
    {
        $Result = array();
        $Columns = $_xml_data->xpath('DependentColumns/Column');
        if (count($Columns) > 0)
            foreach ($Columns as $_column)
            {
                $Result[] = strval($_column->attributes()->ValueName);
            }
        return $Result;
    }

    protected function GetItems($_xml_data)
    {
        $Result = array();
        $Items = $_xml_data->xpath('Items/Item');
        if (count($Items) > 0)
            foreach ($Items as $_item)
            {
                $Item = new \stdClass();
                $Item->Name = strval($_item->attributes()->Name);
                $Item->Value = strval($_item->attributes()->Value);
                $Result[] = $Item;
            }
        return $Result;
    }

    protected function GetTableInfo()
    {
        $TableProperty = new \stdClass();
        $TableProperty->Appendable = $this->Appendable;
        $TableProperty->Columns = array();

        foreach ($this->Columns as $colitem)
        {
            $TableProperty->Columns[] = $colitem->GetInfo();
        }

        $TableProperty->ActionButtons = array();
        foreach ($this->RowActionButtons as $colitem)
        {
            $TableProperty->ActionButtons[] = $colitem->GetInfo();
        }

        return $TableProperty;
    }

    protected function jsaddslashes($s)
    {
        $o = "";
        $l = strlen($s);
        for ($i = 0; $i < $l; $i++)
        {
            $c = $s[$i];
            switch ($c)
            {
                //case '<': $o.='\\x3C'; break; 
                //case '>': $o.='\\x3E'; break; 
                case '\'': $o.='\\\'';
                    break;
                //case '\\': $o.='\\\\'; break; 
                case '"': $o.='\\"';
                    break;
                case "\n": $o.='\\n';
                    break;
                case "\r": $o.='\\r';
                    break;
                default:
                    $o.=$c;
            }
        }
        return $o;
    }

    protected function GetTableData()
    {
        $TableData = array();
        foreach ($this->Items->Items as $_index => $_row)
        {
            $TableData[$_index] = $_row->GetProperty();
        }
        return $TableData;
    }

    protected function SetTableData(array $_data)
    {
        if (count($_data) == 0)
        {
            return false;
        }

        foreach ($_data as $_row)
        {
            $_row['is_deleted'] = strval($_row['is_deleted']) === 'true' ? true : false;
            if (isset($this->Items->Items[$_row["index"]]))
            {
                if ($_row['is_deleted'])
                {
                    $this->Items->Items[$_row["index"]]->IsDeleted = true;
                    continue;
                }
                foreach ($_row['cell'] as $_cell)
                {
                    $this->SetCellValue($this->Items->Items[$_row["index"]], $_cell);
                }
            }
            else
            {
                if ($_row['is_deleted'])
                {
                    continue;
                }
                $Row = new \Mayral\Classes\DB\DBObject('', $this, 'MainDBConnection');

                foreach ($_row['cell'] as $_cell)
                {
                    $this->SetCellValue($Row, $_cell);
                }

                $this->Items->Add($Row);
            }
        }
        return true;
    }

    protected function SetCellValue(&$_row, $_cell)
    {
        foreach ($this->Columns as $_col)
        {
            if ($_col->ValueName === $_cell['property'])
            {
                return $_col->UpdateValue($_row, $_cell['value']);
            }
        }
        return null;
    }

    protected function Event_OnSaveTableData($_sender, $_event_name, $_args)
    {
        $this->SendGlobalEvent('ClientData', json_encode(array('save_table_data_result' => $this->SetTableData(isset($_args['data']) ? $_args['data'] : array()))));
    }

    public function OnSetValue($_value)
    {
        $this->SetTableData(isset($_value) ? $_value : array());
    }

    protected function Event_OnSaveFormData($_sender, $_event_name, $_args)
    {
        $this->GetParentForm()->ButtonOk_OnClick();
    }

    protected function Event_OnDataRequest($_sender, $_event_name, $_args)
    {
        foreach ($this->Columns as $_column)
        {
            if ($_column->ValueName === $_args['ValueName'])
            {
                $this->SendGlobalEvent('ClientData', json_encode($_column->OnDataRequest($_args)));
                break;
            }
        }
    }

    public function CustomEvent($_event_name)
    {
        $result = parent::CustomEvent($_event_name);

        if ($_event_name == "on_cell_change")
        {
            $result = "Event_on_cell_change";
        }
        if ($_event_name === "data_request")
        {
            $result = 'Event_OnDataRequest';
        }
        if ($_event_name === "save_table_data")
        {
            $result = 'Event_OnSaveTableData';
        }
        if ($_event_name === "save_form_data")
        {
            $result = 'Event_OnSaveFormData';
        }
        if ($_event_name === "get_table_data")
        {
            $result = 'Event_GetTableData';
        }

        return $result;
    }

    protected function Event_GetTableData($_sender, $_event_name, $_args)
    {
        $this->SendGlobalEvent('ClientData', json_encode(array('TableInfo' => $this->GetTableInfo(), 'TableData' => $this->GetTableData())));
    }

}

class GRepeaterColumn extends RepeaterColumn
{

    public function GetInfo()
    {
        return array('Sort' => $this->Sort, 'Sortable' => $this->Sortable, 'ValueName' => $this->ValueName, 'ViewName' => $this->ViewName);
    }

    public function UpdateValue(&$_row, $_value)
    {
        $_row->SetPropertyValue($this->ValueName, $_value);
    }

}

class GRepeaterEditColumn extends GRepeaterColumn
{

    public function GetInfo()
    {
        return array_merge(parent::GetInfo(), array('EditType' => 'edit'));
    }

}

class GRepeaterAutocompleteColumn extends GRepeaterColumn
{

    public $LabelName;
    public $QueryAlias;
    public $DependentColumns;

    public function OnDataRequest($_arg)
    {
        $Result = array();
        $daData = new DBDataArray(\Project\Factory::GetInstance()->GetQueryCollection()->QueryText($this->QueryAlias, array($this->LabelName => $_arg['hash'])));
        if ($daData->Fill())
        {
            foreach ($daData as $_row)
            {
                $item = array('label' => $_row[$this->LabelName], 'value' => $_row[$this->LabelName], 'id' => $_row[$this->ValueName]);
                foreach ($this->DependentColumns as $_dcName)
                {
                    $item[$_dcName] = $_row[$_dcName];
                }
                $Result[] = $item;
            }
        }
        return $Result;
    }

    public function UpdateValue(&$_row, $_value)
    {
        $_row->SetPropertyValue($this->ValueName, isset($_value['id']) ? $_value['id'] : null);
        $_row->SetPropertyValue($this->LabelName, isset($_value['value']) ? $_value['value'] : null);
    }

    public function GetInfo()
    {
        return array_merge(parent::GetInfo(), array('EditType' => 'autocomplete', 'LabelName' => $this->LabelName, 'DependentColumns' => $this->DependentColumns));
    }

}

class GRepeaterListboxColumn extends GRepeaterColumn
{

    public $QueryAlias;
    public $DependentColumns;
    public $Items;

    public function GetInfo()
    {
        return array_merge(parent::GetInfo(), array('EditType' => 'listbox', 'DependentColumns' => $this->DependentColumns, 'Items' => $this->Items));
    }

}

?>