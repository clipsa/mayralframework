<?php

/* * **************************************************************************
  Description: панель
  Author: Sokolov Alexander
  Version: 1.0.0

  Changes info:
  = 11.07.2012	(Sokolov Alexander):	создан
 * ************************************************************************** */

namespace Mayral\Classes\VisualComponents;

class GroupBox extends BasicPanel
{

    var $Title;

    public function __construct($_name, $_parent)
    {
        parent::__construct($_name, $_parent);

        $this->DeleteLastTemplate();
        $this->SetTemplateFileName('Templates/Components/'.'GroupBox.html');
        //$this->Style->BackGroundColor = '#e8e2d9';
    }

    protected function BeforeGenerate()
    {
        parent::BeforeGenerate();
        //	заполнение массива переменный для подмены в шаблоне
    }

}

?>