<?php

/* * **************************************************************************
  Description: Класс-картинка
  Author: Zinchenko Sergey
  Created: 18.03.2009
  Version: 1.0.0

  Changes info:
  = 18.03.2009	(Zinchenko Sergey):	создан
  + 19.04.2011	(Kirill Ivanov):	Src, Alt вызывают автообновление
 * ************************************************************************** */

namespace Mayral\Classes\VisualComponents;

class Image extends BasicStyleComponent
{

    /**
     * Путь до картинки
     *
     * @var string 
     *
     */
    protected $Src;

    /**
     * Альтернативный текст
     *
     * @var string 
     *
     */
    protected $Alt;

    public function __construct($_name, $_parent='')
    {
        parent::__construct($_name, $_parent);

        $this->SendProp['Alt']=true;
        $this->SendProp['Src']=true;

        $this->SetTemplateFileName('Templates/Components/'.'Image.html');
    }

    protected function BeforeGenerate()
    {
        parent::BeforeGenerate();
        //	заполнение массива переменный для подмены в шаблоне
        $this->PreGenerateVar['ALT']=$this->Alt;
        $this->PreGenerateVar['SRC']=$this->Src;
    }

}

?>