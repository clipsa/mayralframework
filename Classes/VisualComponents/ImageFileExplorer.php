<?php

/* * **************************************************************************
  Description: файл-менеджер для изображений
  Author: Ivanov Kirill
  Version: 1.0.0

  Changes info:
  = 27.11.2009	(Ivanov Kirill):	создан
 * ************************************************************************** */

namespace Mayral\Classes\VisualComponents;

class ImageFileExplorer extends FileExplorer
{

    public function __construct($_name, $_parent)
    {
        parent::__construct($_name, $_parent);

        $this->IsForm=true;

        $this->FileBox->Style->Bottom='200';

        $this->FileBox->OnItemClick='OnItemClick';

        $Panel=new Panel('Panel', $this);
        $Panel->Style->Height='190';
        $Panel->Style->Left='260';
        $Panel->Style->Bottom='5';
        $Panel->Style->Right='5';

        $Image=new Image('Image', $Panel);
        $Image->Style->Left='5';
        $Image->Style->Right='5';
        $Image->Style->FullBorder='1px solid #aaa';
        $Image->Src='Images/icons/document.png';

        $this->FileBox->AddExtension('gif, bmp, jpg, jpeg, jpe, ico, png');
    }

    public function nItemClick($_sender, $_event, $_event_args=null)
    {
        $this->FileBox->OnItemClick($_sender, $_event, $_event_args);
        $this->Image->Src=$this->FileBox->SelectedFileUrl;
        \Mayral\Classes\Basic\ObjectsPool::Create()->AddToChangedComponents($this->Image);
    }

}

?>