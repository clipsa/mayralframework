<?php

/* * **************************************************************************
  Description: Label
  Author: Ivanov Kirill
  Created: 25.07.2008
  Version: 1.0.0

  Changes info:
  = 25.07.2008	(Ivanov Kirill):	создан
 * ************************************************************************** */

namespace Mayral\Classes\VisualComponents;

class Label extends BasicStyleComponent
{

    protected $_Text;

    public function __construct($_name, $_parent)
    {
        parent::__construct($_name, $_parent);
        $this->SetTemplateFileName('Templates/Components/'.'Label.html');
        $this->SendProp['Text']=true;
    }

    protected function BeforeGenerate()
    {
        parent::BeforeGenerate();
        //	заполнение массива переменный для подмены в шаблоне
        $this->PreGenerateVar['Text']=$this->Text;
    }

    /*
      функция перехватывает чтение значений
     */

    function __get($_prop_name)
    {
        $result='';
        if($_prop_name=='Text')
        {
            $result=$this->_Text;
        }
        else
        {
            $result=parent::__get($_prop_name);
        }
        return $result;
    }

    /*
      функция перехватывает присвоение значений членам класса
     */

    function __set($_prop_name, $_prop_value)
    {
        if($_prop_name=='Text')
        {
            $this->_Text=$_prop_value;
        }
        else
        {
            parent::__set($_prop_name, $_prop_value);
        }
        if(isset($this->SendProp[$_prop_name])&&$this->SendProp[$_prop_name])
        {
            \Mayral\Classes\Basic\ObjectsPool::Create()->AddToChangedComponents($this);
        }
        return true;
    }

}

?>