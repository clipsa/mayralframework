<?php

/* * **************************************************************************
  Description: Ссылка
  Author: Zinchenko Sergey
  Created: -
  Version: 1.0.0

  Changes info:
  = 	(Zinchenko Sergey):	создан
 * ************************************************************************** */

namespace Mayral\Classes\VisualComponents;

class LinkLabel extends Label
{

    public $Href;
    public $Target='_self';

    public function __construct($_name, $_parent)
    {
        parent::__construct($_name, $_parent);
        $this->AllTempaltesFileName->Del($this->AllTempaltesFileName->Count()-1);
        $this->SetTemplateFileName('Templates/Components/'.'LinkLabel.html');
    }

    protected function BeforeGenerate()
    {
        parent::BeforeGenerate();

        $this->PreGenerateVar['Href']=$this->Href;
        $this->PreGenerateVar['Target']=$this->Target;
    }

}

?>