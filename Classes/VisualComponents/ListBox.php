<?php

/* * **************************************************************************
  Description: простой ListBox
  Author: Zinchenko Sergey
  Created: 13.03.2009
  Version: 1.0.0

  Changes info:
  = 13.03.2009	(Zinchenko Sergey):	создан
  = 23.06.2009	(Ivanov Kirill):	Value
  + 24.11.2010	(Zinchenko Sergey): GetCurentText
  + 20.06.2011	(Ivanov Kirill): Автообновление добавил при манипуляциях с айтемами, Value переделал, добавил обработчик на OnChange
 * ************************************************************************** */

namespace Mayral\Classes\VisualComponents;

use Mayral\Classes\Basic;

class ListBox extends BasicRepeater
{

    /**
     * Количество видимых строк (не менее 2-х)
     * исправлено: не менее 1-й (скрол боксы то надо)
     * @var int 
     *
     */
    protected $size=1;

    public function __construct($_Name, $_Parent)
    {
        parent::__construct($_Name, $_Parent);

        $this->Items=new \Mayral\Classes\Lists\AssociationList($this);
        $this->SetTemplateFileName('Templates/Components/'.'ListBox.html');
    }

    public function Clear()
    {
        $this->Items->Clear();
        Basic\ObjectsPool::Create()->AddToChangedComponents($this);
    }

    /**
     * Добавление элемента списка
     *
     * @param mixed $_key ключ
     * @param mixed $_value значение
     *
     */
    public function AddItem($_key, $_value)
    {
        $this->Items->Add($_value, $_key);
        Basic\ObjectsPool::Create()->AddToChangedComponents($this);
    }

    /**
     * Добавление элементов из массива
     *
     * @param mixed $_items массив ключ=>значение
     *
     */
    public function AddItems($_items=array())
    {
        foreach($_items as $key=> $value)
        {
            $this->AddItem($key, $value);
        }
        Basic\ObjectsPool::Create()->AddToChangedComponents($this);
    }

    protected function GenerateItems()
    {
        $result='';
        $this->PreGenerateVar['ValueText']='';

        if($this->Items->Count()>0)
        {
            $keys=$this->Items->GetKeys();
            $keys_cnt=count($keys);
            for($i=0; $i<$keys_cnt; $i++)
            {
                $item=new Basic\String($this);
                $item->Text=$this->ItemTemplate;
                $SELECTED='';
                if($keys[$i]==$this->Value)
                {
                    $SELECTED='SELECTED';
                    $this->PreGenerateVar['ValueText']=$this->Items->Item($keys[$i]);
                }
                $item->Replace('{Key}', $keys[$i]);
                $item->Replace('{SELECTED}', $SELECTED);
                $item->Replace('{Value}', $this->Items->Item($keys[$i]));
                $result.=$item->Text;
            }
        }

        return $result;
    }

    protected function BeforeGenerate()
    {
        parent::BeforeGenerate();

        $this->PreGenerateVar['Size']=$this->size;
        $this->PreGenerateVar['Value']=$this->Value;
    }

    function __set($_prop_name, $_prop_value)
    {
        if($_prop_name==='Size')
        {
            if($_prop_value>=1)
            {
                $this->size=$_prop_value;
            }
        }
        else
        {
            parent::__set($_prop_name, $_prop_value);
        }
        return true;
    }

    /**
     * получение видимого значения выбранного пункта
     *
     * @return string текст пункта
     *
     */
    public function GetCurentText()
    {
        $result='';
        if($this->Value!='')
        {
            $result=$this->Items->Item($this->Value);
        }
        return $result;
    }

}

?>