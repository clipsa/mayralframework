<?php

/* * **************************************************************************
  Description: форма диалог
  Author: Ivanov Kirill
  Version: 1.0.0

  Changes info:
  = 18.06.2009	(Ivanov Kirill):	создан
  = 22.06.2011	(Ivanov Kirill):	+FunctionComponent позволяет передать компонент в котором содержиться функция обработки
 * ************************************************************************** */

namespace Mayral\Classes\VisualComponents\MessageBox;

class MessageDialog extends \Mayral\Classes\VisualComponents\Form
{

    protected $Argument;
    protected $Function;
    protected $FunctionComponent;

    public function __construct($_name, $_parent, $_function_component='')
    {
        $this->IconSrc='Images/icons/exclamation.png';
        parent::__construct($_name, $_parent);
        $this->CreateFromXML('Classes/VisualComponents/MessageBox/Layout/LayoutMessageDialog.xml');
        $this->FunctionComponent=$_function_component;
    }

    public function ShowModal()
    {
        $this->ComponentContainerOuterClass='Form_ModalOuter';
        $this->Class='message-dialog bg-red fg-white';
        $this->Visible=true;
    }

    public function ButtonYes_OnClick($_sender, $_event, $_event_args=null)
    {
        $FunctionName=$this->Function;
        $this->Close();
        if($FunctionName!=='')
        {
            if($this->FunctionComponent!=='')
            {
                $this->FunctionComponent->$FunctionName('yes', $this->Argument);
            }
            else
            {
                $this->Parent->$FunctionName('yes', $this->Argument);
            }
        }
    }

    public function ButtonNo_OnClick($_sender, $_event, $_event_args=null)
    {
        $FunctionName=$this->Function;
        $this->Close();
        if($FunctionName!=='')
        {
            if($this->FunctionComponent!=='')
            {
                $this->FunctionComponent->$FunctionName('no', $this->Argument);
            }
            else
            {
                $this->Parent->$FunctionName('no', $this->Argument);
            }
        }
    }

    public function Work($_text, $_function_name='', $_arg='', $_title='')
    {
        if($_title!=='')
        {
            $this->Title=$_title;
        }
        $this->Argument=$_arg;
        $this->Function=$_function_name;
        $this->LabelMessage->Text=$_text;
        $this->SetCenterPosition();
        $this->ShowModal();
    }

}

?>