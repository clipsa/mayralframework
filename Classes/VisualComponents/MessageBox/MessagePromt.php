<?php

/* * **************************************************************************
  Description: форма диалог с ответом
  Author: Zinchenko Sergey
  Version: 1.0.0

  Changes info:
  = 12.12.2012	(Zinchenko Sergey):	создан
 * ************************************************************************** */
namespace Mayral\Classes\VisualComponents\MessageBox;

class MessagePromt extends MessageDialog
{

    public function __construct($_name, $_parent, $_function_component='')
    {
        parent::__construct($_name, $_parent);
        $this->CreateFromXML('Classes/VisualComponents/MessageBox/Layout/LayoutMessagePromt.xml');
    }

    public function ButtonYes_OnClick($_sender, $_event, $_event_args=null)
    {
        $FunctionName=$this->Function;
        $this->Close();
        if($FunctionName!=='')
        {
            if($this->FunctionComponent!=='')
            {
                $this->FunctionComponent->$FunctionName('yes', array($this->Edit_Answer->Value, $this->Argument));
            }
            else
            {
                $this->Parent->$FunctionName('yes', array($this->Edit_Answer->Value, $this->Argument));
            }
        }
    }

    public function ButtonNo_OnClick($_sender, $_event, $_event_args=null)
    {
        $this->Close();
    }

}

?>