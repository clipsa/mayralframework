<?php

/* * **************************************************************************
  Description: сообщение
  Author: Ivanov Kirill
  Version: 1.0.0

  Changes info:
  = 07.07.2009	(Ivanov Kirill):	создан
 * ************************************************************************** */

namespace Mayral\Classes\VisualComponents\MessageBox;

class MessageWarning extends \Mayral\Classes\VisualComponents\Form
{

    public function __construct($_name, $_parent)
    {
        $this->IconSrc='Images/icons/exclamation.png';
        parent::__construct($_name, $_parent);
        $this->CreateFromXML('Classes/VisualComponents/MessageBox/Layout/LayoutMessageWarning.xml');
        $this->ButtonOk->Visible=true;
    }

    public function ShowModal()
    {
        $this->ComponentContainerOuterClass='Form_ModalOuter';
        $this->Class='message-dialog bg-green fg-white';
        $this->Visible=true;
    }

    public function ButtonOk_OnClick($_sender, $_event, $_event_args=null)
    {
        $this->Close();
    }

    public function Work($_text, $_function_name='', $_arg='', $_title='')
    {
        if($_title!=='')
        {
            $this->Title=$_title;
        }
        $this->LabelMessage->Text=$_text;
        $this->SetCenterPosition();
        $this->ShowModal();
    }

}

?>