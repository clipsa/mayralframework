<?php

namespace Mayral\Classes\VisualComponents;

class Node extends \Mayral\Classes\Basic\BasicClass
{

    public $Object;
    public $Text;
    public $Nodes;
    public $Index;
    public $Icon='';
    public $Expand=true;
    public $Level;
    public $ParentNode;

    public function __construct($_Text, $_parent, $_object='', $_icon='')
    {
        parent::__construct('', $_parent);
        $this->Nodes=new \Mayral\Classes\Lists\BasicList($this);
        $this->Object=$_object;
        $this->Text=$_Text;
        $this->Icon=$_icon;
        $this->ParentNode=null;
    }

}

?>