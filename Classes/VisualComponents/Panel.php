<?php

/* * **************************************************************************
  Description: панель
  Author: Ivanov Kirill
  Version: 1.0.0

  Changes info:
  = 11.07.2008	(Ivanov Kirill):	создан
 * ************************************************************************** */
namespace Mayral\Classes\VisualComponents;

class Panel extends BasicPanel
{

    var $Title;

    public function __construct($_name, $_parent)
    {
        parent::__construct($_name, $_parent);

        $this->DeleteLastTemplate();
        $this->SetTemplateFileName('Templates/Components/'.'Panel.html');
    }

    protected function BeforeGenerate()
    {
        parent::BeforeGenerate();
        //	заполнение массива переменный для подмены в шаблоне
        $this->PreGenerateVar['Title']=$this->Title;
    }

}

?>