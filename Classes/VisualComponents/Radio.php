<?php

/* * **************************************************************************
  Description: radio
  Author: Zinchenko Sergey
  Created: 19.08.2010
  Version: 1.0.0

  Changes info:
  = 19.08.2010	(Zinchenko Sergey):	создан
 * ************************************************************************** */

namespace Mayral\Classes\VisualComponents;

class Radio extends CheckBox
{

    public $GroupName;

    public function __construct($_name, $_parent)
    {
        parent::__construct($_name, $_parent);
        $this->DeleteLastTemplate();
        $this->SetTemplateFileName('Templates/Components/'.'Radio.html');
    }

    protected function BeforeGenerate()
    {
        parent::BeforeGenerate();
        $this->PreGenerateVar['GroupName']=($this->GroupName==null)?$this->Name:$this->GroupName;
    }

}

?>