<?php

/* * **************************************************************************
  Description: группа радиокнопок
  Author: Zinchenko Sergey
  Created: 05.03.2011
  Version: 1.0.0

  Changes info:
  = 05.03.2011	(Zinchenko Sergey):	создан
 * ************************************************************************** */

namespace Mayral\Classes\VisualComponents;

class RadioGroup extends BasicContainer
{

    public function __construct($_name, $_parent)
    {
        parent::__construct($_name, $_parent);
        $this->SetTemplateFileName('Templates/Components/'.'BasicPanel.html');
    }

    public function AddComponent($_component, $_value=null)
    {
        if(get_class($_component)=='Mayral\Classes\VisualComponents\Radio')
        {
            $_component->GroupName=$this->Name;
            if($_value===null)
            {
                $_value=$this->Components->Count()+1;
            }
            $_component->ReturnValue=$_value;
            parent::AddComponent($_component);
        }
    }

    function __set($_prop_name, $_prop_value)
    {
        if($_prop_name==='Value')
        {
            if($this->_Value!==$_prop_value)
            {
                $component_keys=$this->Components->GetKeys();
                $component_cnt=count($component_keys);
                for($i=0; $i<$component_cnt; $i++)
                {
                    $component=$this->Components->Item($component_keys[$i]);
                    if(isset($component->ReturnValue)&&$component->ReturnValue==$_prop_value)
                    {
                        $component->Value=true;
                    }
                    else
                    {
                        $component->Value=false;
                    }
                }
                $this->_Value=$_prop_value;
                if($this->OnChange!=''&&$this->Parent!='')
                {
                    $OnChange=$this->OnChange;
                    $this->$OnChange($this, 'onchange');
                }
            }
        }
        else
        {
            parent::__set($_prop_name, $_prop_value);
        }
        return true;
    }

    function __get($prop_name)
    {
        $result=parent::__get($prop_name);
        if($prop_name=='Value')
        {
            $component_keys=$this->Components->GetKeys();
            $component_cnt=count($component_keys);
            $result='';
            for($i=0; $i<$component_cnt; $i++)
            {
                $component=$this->Components->Item($component_keys[$i]);
                if($component->Value===true)
                {
                    $result=$component->ReturnValue;
                    break;
                }
            }
        }
        return $result;
    }

}

?>