<?php

namespace Mayral\Classes\VisualComponents;

class RepeaterActionButton
{

    /**
     * Событие вызываемое на onclick
     *
     * @var mixed 
     *
     */
    public $Event;

    /**
     * Картинка на кнопке
     *
     * @var mixed 
     *
     */
    public $Icon;

    /**
     * Подсказка на картинке или текст
     *
     * @var string 
     *
     */
    public $Title;

    /**
     * Яваскрипт выполняемый по нажатию
     *
     * @var string 
     *
     */
    public $javascript_OnClick;

    public function __construct($_event, $_icon, $_title = '', $javascript_OnClick = '')
    {
        $this->Event = $_event;
        $this->Icon = $_icon;
        $this->Title = $_title;
        $this->javascript_OnClick = $javascript_OnClick;
    }

}

?>