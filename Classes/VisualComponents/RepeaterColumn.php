<?php

namespace Mayral\Classes\VisualComponents;

class RepeaterColumn
{

    /**
     * Текст заголовка
     *
     * @var string 
     *
     */
    public $ViewName;

    /**
     * Название поля в датасете 
     *
     * @var string 
     *
     */
    public $ValueName;

    /**
     * Флаг сортируема ли колонка
     *
     * @var bool 
     *
     */
    public $Sortable;

    /**
     * Состояние сортировки: none, up, down
     *
     * @var mixed 
     *
     */
    public $Sort;

    public function __construct($_viewname, $_valuename, $_sortable=false, $_sort='none')
    {
        $this->ViewName=$_viewname;
        $this->ValueName=$_valuename;
        $this->Sortable=$_sortable;
        $this->Sort=$_sort;
    }

}

?>