<?php

/* * **************************************************************************
  Description: Репитер со страницами и сортировкой
  Author: Kecheor
  Version: 1.0.1

  Changes info:
  = 05.11.2009	(Kecheor):	создан
  = 31.07.2012	(Zinchenko Sergey):	$TotalRowsCount
 * ************************************************************************** */

namespace Mayral\Classes\VisualComponents;

class SPRepeater extends BasicRepeater
{

    /**
     * Имя свойства индекса (Primary Key)
     *
     * @var mixed 
     *
     */
    public $IndexProperty='ID';

    /**
     * Количество элементов на страницу
     *
     * @var integer 
     *
     */
    public $ItemsPerPage=15;

    /**
     * Текущая страница
     *
     * @var int 
     *
     */
    public $CurrentPage=1;

    /**
     * общее кол-во записей
     *
     * @var int 
     *
     */
    public $TotalRowsCount=0;

    /**
     * Шаблон селектора страниц
     *
     * @var mixed 
     *
     */
    protected $PagesTemplate;

    /**
     * Шаблон кнопки страницы
     *
     * @var mixed 
     *
     */
    protected $PageItemTemplate;

    /**
     * Шаблон текущей страницы
     *
     * @var mixed 
     *
     */
    protected $ActivePageItemTemplate;

    /**
     * Имя функции используемой для обработки изменения текущей страницы
     *
     * @var string 
     *
     */
    public $OnPageChange='ChangePage';

    /**
     * Имя функции используемой для сортировки по убыванию
     *
     * @var string 
     *
     */
    public $OnSortChangeDown='ChangeSortDown';

    /**
     * Имя функции используемой для сортировки по возрастанию
     *
     * @var string 
     *
     */
    public $OnSortChangeUp='ChangeSortUp';

    public function __construct($_name, $_parent)
    {
        parent::__construct($_name, $_parent);

        $this->PagesTemplate=new \Mayral\Classes\Basic\String($this);
        $this->PageItemTemplate=new \Mayral\Classes\Basic\String($this);
        $this->ActivePageItemTemplate=new \Mayral\Classes\Basic\String($this);
    }

    protected function BeforeGenerate()
    {
        parent::BeforeGenerate();

        $this->PreGenerateVar['Pages']=$this->GenPages();
    }

    protected function GenerateItems()
    {
        $result='';

        for($i=0; $i<$this->Items->Count(); $i++)
        {
            $result=$result.$this->GenerateItem($i);
        }
        return $result;
    }

    /**
     * Парсинг шаблонов страниц
     *
     * @return OString Основной шаблон таблицы
     *
     */
    protected function LoadTemplate($_type)
    {

        $result=new \Mayral\Classes\Basic\String($this);
        $result->Text=parent::LoadTemplate($_type);

        $this->PagesTemplate->Text=$result->GetChangeTextBetween('<!--Pages-->', '<!--/Pages-->', '{Pages}');
        $this->PageItemTemplate->Text=$this->PagesTemplate->GetChangeTextBetween('<!--PageItemTemplate-->', '<!--/PageItemTemplate-->', '{PAGEITEMS}');
        $this->ActivePageItemTemplate->Text=$this->PagesTemplate->GetChangeTextBetween('<!--ActivePageItemTemplate-->', '<!--/ActivePageItemTemplate-->', '');

        return $result->Text;
    }

    /**
     * Функия генерации номеров страниц
     *
     * @return string html код
     *
     */
    protected function GenPages()
    {
        $pages_count=0;

        if($this->ItemsPerPage>0)
        {
            $pages_count=ceil($this->TotalRowsCount/$this->ItemsPerPage);
        }

        if($pages_count>1)
        {
            $pages='';

            for($i=1; $i<=$pages_count; $i++)
            {
                if($i==$this->CurrentPage)
                {
                    $page=new \Mayral\Classes\Basic\String($this);
                    $page->Text=$this->ActivePageItemTemplate->Text;
                    $page->Replace('{PAGEN}', $i);
                    $pages .= $page->Text;
                }
                else
                {
                    $page=new \Mayral\Classes\Basic\String($this);
                    $page->Text=$this->PageItemTemplate->Text;
                    $page->Replace('{PAGEN}', $i);
                    $pages .= $page->Text;
                }
            }

            $result=new \Mayral\Classes\Basic\String($this);
            $result->Text=$this->PagesTemplate->Text;
            $result->Replace('{PAGEITEMS}', $pages);

            $result->Replace('{TOTAL_PAGES}', $pages_count);
            $result->Replace('{CURRENT_PAGE}', $this->CurrentPage);
            $result->Replace('{Name}', $this->FullName());

            return $result->Text;
        }

        return '';
    }

    /**
     * Обработка собственных событий компонента
     *
     * @param string $_event_name Имя произошедшего события
     * @return string Имя метода используемого для обработки
     *
     */
    function CustomEvent($_event_name)
    {
        $action_function_name=parent::CustomEvent($_event_name);

        if($_event_name=='onpagechange')
        {
            $action_function_name=$this->OnPageChange;
        }
        if($_event_name=='onsortdown')
        {
            $action_function_name=$this->OnSortChangeDown;
        }
        if($_event_name=='onsortup')
        {
            $action_function_name=$this->OnSortChangeUp;
        }
        if($_event_name=='onrowclick')
        {
            $action_function_name=$this->OnRowClick;
        }
        return $action_function_name;
    }

    protected function ChangePage($_sender, $_event, $_event_args=null)
    {
        if($_event_args!==null)
        {
            $this->CurrentPage=$_event_args;
        }
        \Mayral\Classes\Basic\ObjectsPool::Create()->AddToChangedComponents($this);
    }

    public function ChangeSortUp($_sender, $_event, $_event_args=null)
    {
        $this->Sort($_event_args, true);
        \Mayral\Classes\Basic\ObjectsPool::Create()->AddToChangedComponents($this);
    }

    public function ChangeSortDown($_sender, $_event, $_event_args=null)
    {
        $this->Sort($_event_args, false);
        \Mayral\Classes\Basic\ObjectsPool::Create()->AddToChangedComponents($this);
    }

    /**
     * Метод для сортировки записей в таблице
     *
     * @param mixed $_field Идентификатор колонки
     * @param bool $_up По возрастанию/по убыванию
     *
     */
    public function Sort($_field, $_up=true)
    {
        $i=0;
        $j=0;
        $k=0;
        $tmp=0;

        if($this->Items->Count()<=1)
            return;


        $n=$this->Items->Count()-1;
        $i=1;

        do
        {
            $j=0;
            do
            {
                if(($_up&&($this->Items->Items[$i]->Property[$_field]<$this->Items->Items[$j]->Property[$_field]))||(!$_up&&($this->Items->Items[$i]->Property[$_field]>$this->Items->Items[$j]->Property[$_field])))
                {
                    $k=$i;

                    $tmp=$this->Items->Items[$i];
                    do
                    {
                        $this->Items->Items[$k]=$this->Items->Items[$k-1];
                        $k=$k-1;
                    } while($k>$j);

                    $this->Items->Items[$j]=$tmp;
                    $j=$i;
                }
                else
                {
                    $j=$j+1;
                }
            } while($j<$i);
            $i=$i+1;
        } while($i<=$n);
    }

}

?>