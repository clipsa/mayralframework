<?php

namespace Mayral\Classes\VisualComponents;

class SortableList extends \Mayral\Classes\VisualComponents\BasicRepeater
{

    public function __construct($_name, $_parent)
    {
        parent::__construct($_name, $_parent);

        $this->SetTemplateFileName('Templates/Components/SortableList.html');
    }

}

?>