<?php

/* * **************************************************************************
  Description: класс описания стилей
  Author: Ivanov Kirill
  Created: 09.07.2008
  Version: 1.0.0

  Changes info:
  = 09.07.2008	(Ivanov Kirill):	создан
 * ************************************************************************** */

namespace Mayral\Classes\VisualComponents;

global $conformity;

function AddConformity($_name, $_css_name, $_for_container, $_for_body, $_add_text)
{
    global $conformity;

    $conformity[$_name]['css_name']=$_css_name;
    $conformity[$_name]['container']=$_for_container;
    $conformity[$_name]['body']=$_for_body;
    $conformity[$_name]['add_text']=$_add_text;
}

AddConformity('Padding', 'padding', true, false, '');
AddConformity('Margin', 'margin', true, false, '');
AddConformity('Width', 'width', true, false, 'px');
AddConformity('Height', 'height', true, false, 'px');
AddConformity('Top', 'top', true, false, 'px');
AddConformity('Left', 'left', true, false, 'px');
AddConformity('Bottom', 'bottom', true, false, 'px');
AddConformity('Right', 'right', true, false, 'px');
AddConformity('Position', 'position', true, false, '');
AddConformity('Overflow', 'overflow', true, true, '');
AddConformity('WordWrap', 'word-wrap', false, true, '');
AddConformity('TextWrap', 'text-wrap', false, true, '');
AddConformity('TextAlign', 'text-align', false, true, '');
AddConformity('Color', 'color', false, true, '');
AddConformity('Display', 'display', true, false, '');
AddConformity('Visibility', 'visibility', true, false, '');
AddConformity('BackGroundColor', 'background-color', false, true, '');
AddConformity('Cursor', 'cursor', false, true, '');
AddConformity('zIndex', 'z-index', true, false, '');
AddConformity('FontSize', 'font-size', false, true, '');
AddConformity('FontWeight', 'font-weight', false, true, '');
AddConformity('FullBorder', 'border', false, true, '');

class Style extends \Mayral\Classes\Basic\BasicClass
{

    public $Width;
    public $Padding;
    public $Margin;
    public $Height;
    public $Top;
    public $Left;
    public $Bottom;
    public $Right;
    public $Position;
    public $Border;
    public $Font;
    public $Color;
    public $TextAlign; //	Value:	start | end | left | right | center | justify
    public $Overflow; //	Value:	auto | hidden | scroll | visible
    public $WordWrap; //	Value:	normal | break-word 
    public $TextWrap; //	Value:	normal | unrestricted | none | suppress 
    public $Display; //	Value:	block | inline | inline-table | list-item | none | run-in | table | table-caption | table-cell | table-column-group | table-footer-group | table-header-group | table-row | table-row-group
    public $Visibility; //	Value:	visible | hidden | collapse
    public $Cursor;  //	Value:	auto | crosshair | default | e-resize | help | move | n-resize | ne-resize | nw-resize | pointer | progress | s-resize | se-resize | sw-resize | text | w-resize | wait
    public $BackGroundColor;
    public $zIndex;
    public $FontSize;
    public $FontWeight;
    public $FullBorder;
    public $FreeZone;

    public function __construct($_parent)
    {
        parent::__construct('', $_parent);
        $this->Border=new BorderStyle($this);
        $this->Font=new FontStyle($this);

        $this->Position='absolute';
    }

    /*
      функция генерирует по массиву соответствий
     */

    public function GenerateStyleByConformity($_type)
    {
        global $conformity;

        $result='';
        foreach($conformity as $name=> $value)
        {
            if($_type==''||$value[$_type])
            {
                $property_value=$this->$name;
                if($property_value!==null)
                {
                    if($value['add_text']!='')
                    {
                        $property_value=$this->AddTextIfNumber($property_value, $value['add_text']);
                    }
                    $result=$result.$value['css_name'].':'.$property_value.';';
                }
            }
        }
        return $result;
    }

    /*
      функция генерирует стиль
     */

    public function Generate()
    {
        $result=$this->GenerateStyleByConformity($conformity);
        //	добавляем 'свободную зону'
        $result=$result.$this->FreeZone;
        //	добавляем стиль границы
        $result=$result.$this->Border->Generate();
        //	добавляем стиль шрифта
        $result=$result.$this->GenerateFontStyle();
        return $result;
    }

    /*
      функция генерирует стиль контейнера компонента
     */

    public function GenerateComponentContainerStyle()
    {
        $result=$this->GenerateStyleByConformity('container');
        //	добавляем стиль шрифта
        //$result = $result.$this->Parent->GetFont()->Generate();
        return $result;
    }

    /*
      функция генерирует стиль компонента
     */

    public function GenerateComponentBodyStyle()
    {
        $result=$this->GenerateStyleByConformity('body');
        //	добавляем 'свободную зону'
        $result=$result.$this->FreeZone;
        //	добавляем стиль границы
        $result=$result.$this->Border->Generate();
        return $result;
    }

    /*
      функция генерирует стиль шрифта
     */

    public function GenerateFontStyle()
    {
        $result=$this->Font->Generate();
        return $result;
    }

    /*
      функция проверяет является переменная числом или нет
     */

    protected function IsNumber($_var)
    {
        return (is_numeric($_var));
    }

    /*
      функция добавляет текст к переменной если она числовая
     */

    protected function AddTextIfNumber($_var, $_after_var_text)
    {
        $result=$_var;
        if($this->IsNumber($_var))
        {
            $result=$result.$_after_var_text;
        }
        return $result;
    }

}

?>