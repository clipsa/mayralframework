<?php

/* * **************************************************************************
  Description: табы
  Version: 1.0.0

  Changes info:
  = 04.09.2008	(Kecheor):	создан
  + 02.07.2011	(Ivanov Kirill):	BeforeTabChanged, OnTabChanged
 * ************************************************************************** */

namespace Mayral\Classes\VisualComponents;

use Mayral\Classes\Basic\String as String;
use Mayral\Classes\Basic\ObjectsPool as ObjectsPool;

class TabControl extends BasicContainer
{

    protected $TabTemplate;
    protected $ActiveTabTemplate;
    public $BeforeTabChanged='';
    public $OnTabChanged='';
    private $ActiveTab;

    public function __construct($_name, $_parent)
    {
        parent::__construct($_name, $_parent);

        $this->TabTemplate=new String($this);
        $this->ActiveTabTemplate=new String($this);

        $this->SetTemplateFileName('Templates/Components/'.'TabControl.html');
    }

    /**
     * Парсинг шаблонов
     *
     * @return OString Основной шаблон
     *
     */
    protected function LoadTemplate($_type)
    {
        $result=new String($this);
        $result->Text=parent::LoadTemplate($_type);
        //Шаблон активной вкладки
        $this->TabTemplate->Text=$result->GetChangeTextBetween('<!--Tab-->', '<!--/Tab-->', '{TABS}');
        //Шаблон строки с данными
        $this->ActiveTabTemplate->Text=$result->GetChangeTextBetween('<!--ActiveTab-->', '<!--/ActiveTab-->', '');

        return $result->Text;
    }

    /**
     * Добавление html кода вкладок в компонент
     *
     * @return void 
     *
     */
    public function BeforeGenerate()
    {
        parent::BeforeGenerate();
        $this->PreGenerateVar['TABS']=$this->GenTabs();
    }

    /**
     * Создание html кода вкладок
     *
     * @return string html код
     *
     */
    protected function GenTabs()
    {
        $result='';

        foreach($this->Components->Items as $component)
        {
            if($component->Enable)
            {
                $new_tab=new String($this);
                if($component->Name==$this->ActiveTab)
                {
                    $new_tab->Text=$this->ActiveTabTemplate->Text;
                }
                else
                {
                    $new_tab->Text=$this->TabTemplate->Text;
                }
                $new_tab->Replace('{Text}', isset($component->Title)&&$component->Title!=''?$component->Title:$component->Name);
                $new_tab->Replace('{TabName}', $component->Name);
                $new_tab->Replace('{Name}', $this->FullName());

                $this->AddParamsToTab($new_tab, $component);

                $result .= $new_tab->Text;
                unset($new_tab);
            }
        }

        return $result;
    }

    protected function AddParamsToTab($_tab, $_component)
    {
        
    }

    /**
     * Добавление компонента, если он первый, то становится активным, иначе скрытым
     *
     * @param BasicComponent $_component Компонент для добавления
     * @return void 
     *
     */
    public function AddComponent($_component)
    {
        $_component->Style->Top=0;
        $_component->Style->Left=0;
        $_component->Style->Right=0;
        $_component->Style->Bottom=0;

        $this->Components->Add($_component, $_component->Name);
        if(count($this->Components->Items)==1)
        {
            $this->ActiveTab=$_component->Name;
            $_component->Visible=true;
            ObjectsPool::Create()->AddToChangedComponents($_component);
        }
        else
        {
            $_component->Visible=false;
            ObjectsPool::Create()->AddToChangedComponents($_component);
        }
    }

    /**
     * Обработка собственных событий компонента
     *
     * @param string $_event_name Имя произошедшего события
     * @return string Имя метода используемого для обработки
     *
     */
    function CustomEvent($_event_name)
    {
        if($_event_name=='tabclick')
        {
            $action_function_name='ChangeTab';
        }
        if($_event_name=='tabclose')
        {
            $action_function_name='CloseTab';
        }
        return $action_function_name;
    }

    /**
     * Обработка события смены вкладки
     *
     * @param string $_sender Отправитель события
     * @param string $_event Имя события
     * @param string $_event_args Аргументы событи
     * @return void 
     *
     */
    public function ChangeTab($_sender, $_event, $_event_args)
    {
        if($this->BeforeTabChanged!=='')
        {
            $BeforeTabChanged_function=$this->BeforeTabChanged;
            $this->GetParentForm()->$BeforeTabChanged_function($_sender, $_event, $_event_args);
        }

        $this->ActiveTab=$_event_args;
        foreach($this->Components->Items as $component)
        {
            if($component->Name==$this->ActiveTab)
            {
                $component->Visible=true;
            }
            else
            {
                $component->Visible=false;
            }
        }

        if($this->OnTabChanged!=='')
        {
            $OnTabChanged_function=$this->OnTabChanged;
            $this->GetParentForm()->$OnTabChanged_function($_sender, $_event, $this->ActiveTab);
        }

        ObjectsPool::Create()->AddToChangedComponents($this);
    }

    public function CloseTab($_sender, $_event, $_event_args)
    {
        $this->Components->Del($_event_args);
        $this->ActiveTab=current($this->Components->Items);
        ObjectsPool::Create()->AddToChangedComponents($this);
    }

    public function ActiveTabName()
    {
        return $this->ActiveTab;
    }

}

?>