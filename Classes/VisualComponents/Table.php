<?php

/* * **************************************************************************
  Version: 1.0.0
  Description: класс таблицы

  Changes info:
  + 07.06.2008:	Ivanov Кирилл		создан
  + 04.03.2009:	Zinchenko Sergey	таблица в несколько страниц
  + 04.03.2009:	Zinchenko Sergey	ChangePage, ChangeSortUp, ChangeSortDown, RowClick
 * ************************************************************************** */

namespace Mayral\Classes\VisualComponents;

use Mayral\Classes\Basic\String as String;

class Table extends BasicContainer
{

    /**
     * Источник данных для таблицы
     *
     * @var mixed 
     *
     */
    public $DataTable;

    /**
     * Флаг, показывать ли заголовки у столбцов
     *
     * @var boolean 
     *
     */
    public $ShowHeader=true;
    public $RowsPerPage=10;
    public $CurrentPage=0;

    /**
     * Номер выбраной строки
     *
     * @var int 
     *
     */
    public $SelectedRow=-1;

    /**
     * Имя функции используемой для обработки нажатия кнопки "добавить"
     *
     * @var string 
     *
     */
    public $OnAddRow='ShowViewForm';

    /**
     * Имя функции используемой для обработки нажания кнопки "настроить колонки"
     *
     * @var string 
     *
     */
    public $OnSetColumns='ShowColumnsForm';

    /**
     * Имя функции используемой для обработки нажания кнопки "опции"
     *
     * @var string 
     *
     */
    public $OnOptions='ShowOptionsForm';

    /**
     * Имя функции используемой для обработки изменения текущей страницы
     *
     * @var string 
     *
     */
    public $OnPageChange='ChangePage';

    /**
     * Имя функции используемой для сортировки по убыванию
     *
     * @var string 
     *
     */
    public $OnSortChangeDown='ChangeSortDown';

    /**
     * Имя функции используемой для сортировки по возрастанию
     *
     * @var string 
     *
     */
    public $OnSortChangeUp='ChangeSortUp';

    /**
     * Имя функции используемой для обработки клика по строке
     *
     * @var string 
     *
     */
    public $OnRowClick='RowClick';

    /**
     * Шаблон строки с заголовками
     *
     * @var OString 
     *
     */
    protected $HeaderRowTemplate;

    /**
     * Шаблон ячейки-заголовка
     *
     * @var OString 
     *
     */
    protected $HeaderCellTemplate;

    /**
     * Шаблон строки с данными
     *
     * @var OString 
     *
     */
    protected $RowTemplate;

    /**
     * Шаблон ячейки с данными
     *
     * @var OString 
     *
     */
    protected $TableCellTemplate;

    /**
     * Шаблон кнопки действий таблицы
     *
     * @var OString 
     *
     */
    protected $TableButtonTemplate;

    /**
     * Массив с кнопками вверху таблички
     *
     * @var array 
     *
     */
    protected $ActionButtons=array();

    public function __construct($_name, $_parent, $DataTable)
    {
        parent::__construct($_name, $_parent);
        $this->DataTable=$DataTable;

        $this->HeaderRowTemplate=new \Mayral\Classes\Basic\String($this);
        $this->HeaderCellTemplate=new \Mayral\Classes\Basic\String($this);
        $this->RowTemplate=new \Mayral\Classes\Basic\String($this);
        $this->TableCellTemplate=new \Mayral\Classes\Basic\String($this);
        $this->TableCellTemplate=new \Mayral\Classes\Basic\String($this);


        $this->SetTemplateFileName("Templates/Components/"."Table.html");
    }

    /**
     * Парсинг шаблонов
     *
     * @return OString Основной шаблон таблицы
     *
     */
    protected function LoadTemplate($_type)
    {
        $result=new \Mayral\Classes\Basic\String($this);
        $result->Text=parent::LoadTemplate($_type);
        //Шаблон строки заголовков
        $this->HeaderRowTemplate->Text=$result->GetChangeTextBetween('<!--TableHeaderRowTemplate-->', '<!--/TableHeaderRowTemplate-->', '{HEADER}');
        //Шаблон ячейки заголовков
        $this->HeaderCellTemplate->Text=$this->HeaderRowTemplate->GetChangeTextBetween('<!--TableHeaderCellTemplate-->', '<!--/TableHeaderCellTemplate-->', '{HEADERCELLS}');
        //Шаблон строки с данными
        $this->RowTemplate->Text=$result->GetChangeTextBetween('<!--TableRowTemplate-->', '<!--/TableRowTemplate-->', '{ROWS}');
        //Шаблон ячейки с данными
        $this->TableCellTemplate->Text=$this->RowTemplate->GetChangeTextBetween('<!--TableCellTemplate-->', '<!--/TableCellTemplate-->', '{CELLS}');

        $this->TableButtonTemplate->Text=$result->GetChangeTextBetween('<!--Button-->', '<!--/Button-->', '');

        $this->ActionButtons['EditButton']='<a href="javascript:ServerArgEvent(\''.$this->FullName().'\', \'oneditrow\', \'{Row}\')" alt="Добавить"><img src="Images/components/OTable/edit_button.jpg" border="0"></a>';
        $this->ActionButtons['DeleteButton']='<a href="" alt="Удалить"><img src="Images/components/OTable/check_button.jpg" border="0"></a>';

        return $result->Text;
    }

    /**
     * Метод для генерации компонентов таблицы (заголовков, ячеек данных, кнопок)
     *
     * @return void 
     *
     */
    public function BeforeGenerate()
    {
        parent::BeforeGenerate();

        $header='';
        if($this->ShowHeader&&get_class($this->DataTable)!='ODataTable')
        {
            $header=$this->GenHeader();
        }
        $this->PreGenerateVar['HEADER']=$header;
        $this->PreGenerateVar['ROWS']=$this->GenCells();
        $this->PreGenerateVar['Pages']=$this->GenPages();
        $this->GenTableActionButtons();
    }

    /**
     * Функия генерации номеров страниц
     *
     * @return string html код
     *
     */
    protected function GenPages()
    {
        $result="";

        $pages_count=round($this->DataTable->RowCount/$this->RowsPerPage);

        if($pages_count>1)
        {
            for($i=0; $i<$pages_count; $i++)
            {
                $result.="<a href=\"javascript:ServerArgEvent('{$this->FullName()}', 'onpagechange', '$i')\">".($i+1)."</a>&nbsp;&nbsp;";
            }
        }

        return $result;
    }

    /**
     * Функия генерации заголоков столбцов
     *
     * @return string html код заголовков
     *
     */
    protected function GenHeader()
    {
        $cells='';
        for($c=0; $c<$this->DataTable->ColCount; $c++)
        {
            if(/* isset($this->DataTable->GetColumnByIndex($c)->Meta->visible) && $this->DataTable->GetColumnByIndex($c)->Meta->visible == */true)
            {
                $column_name=$this->DataTable->GetColumnByIndex($c)->Name;
                $new_cell=new \Mayral\Classes\Basic\String($this);
                $new_cell->Text=$this->HeaderCellTemplate->Text;
                $new_cell->Replace('{Text}', $column_name);
                $new_cell->Replace('{SortUp}', 'javascript:ServerArgEvent(\''.$this->FullName().'\', \'onsortup\', \''.$c.'\')');
                $new_cell->Replace('{SortDown}', 'javascript:ServerArgEvent(\''.$this->FullName().'\', \'onsortdown\', \''.$c.'\')');
                $cells .= $new_cell->Text;
                unset($new_cell);
            }
        }
        $result=$this->HeaderRowTemplate->Text;
        $result=str_replace('{HEADERCELLS}', $cells, $result);
        return $result;
    }

    /**
     * Функция генерации кода ячеек
     *
     * @return string html код ячеек с данными
     *
     */
    protected function GenCells()
    {
        $result='';
        $this->DataTable->FirstRow();

        $start=$this->RowsPerPage*$this->CurrentPage;
        $stop=($this->DataTable->RowCount-$start)>$this->RowsPerPage?($start+$this->RowsPerPage):$this->DataTable->RowCount;

        for($r=$start; $r<$stop; $r++)
        {
            $cells='';
            $rowcolor=$r%2==0?'#ffffff':'#f5f1eb';
            $rowcolor=($r==$this->SelectedRow)?'gray':$rowcolor;

            for($c=0; $c<$this->DataTable->ColCount; $c++)
            {
                if(/* isset($this->DataTable->GetColumnByIndex($c)->Meta->visible) && $this->DataTable->GetColumnByIndex($c)->Meta->visible == */true)
                {
                    $cell=new \Mayral\Classes\Basic\String($this);
                    $cell->Text=$this->TableCellTemplate->Text;
                    $cell->Replace('{Text}', $this->DataTable->GetFieldValue($c, $r));
                    //$cell->Replace('{Cell_Color}', $rowcolor);
                    $cell->Replace('{Number}', "$r-$c");
                    $cell->Replace('{Font_Style}', '');
                    $cell->Replace('{Style}', '');
                    $cells .= $cell->Text;
                    unset($cell);
                }
            }
            $row=new \Mayral\Classes\Basic\String($this);
            $row->Text=$this->RowTemplate->Text;
            $row->Replace('{CELLS}', $cells);
            $row->Replace('{ActionButtons}', $this->GenActionButtons($r));
            $row->Replace('{Row}', $r);
            $row->Replace('{Cell_Color}', $rowcolor);
            $row->Replace('{Action}', 'javascript:ServerArgEvent(\''.$this->FullName().'\', \'onrowclick\', \''.$r.'\')');
            $result .= $row->Text;
            unset($row);
        }
        return $result;
    }

    /**
     * Генерирует ячейки с кнопками редактирования строки
     *
     * @param int $_row_id ID для которой генерятся кнопки
     * @return string html код кнопок
     *
     */
    protected function GenActionButtons($_row_id)
    {
        $result='';
        foreach($this->ActionButtons as $button)
        {
            $result .= '<td>'.$button.'</td>';
        }
        return $result;
    }

    /**
     * Создает кнопки в шапке таблицы
     *
     * @return void 
     *
     */
    protected function GenTableActionButtons()
    {
        $this->PreGenerateVar['Add_button']=$this->GenButton('Добавить', 'ServerArgEvent(\'{Name}\', \'onaddrow\', \'\')', 'Images/components/OTable/add_button.jpg', 'add');
        $this->PreGenerateVar['Options_button']=$this->GenButton('Опции', 'ServerArgEvent(\'{Name}\', \'onoptions\', \'\')', 'Images/components/OTable/options_button.jpg', 'options');
        $this->PreGenerateVar['Columns_button']=$this->GenButton('Настройка колонок', 'ServerArgEvent(\'{Name}\', \'onsetcolumns\', \'\')', 'Images/components/OTable/columns_button.jpg', 'columns');
    }

    /**
     * Метод для генерации html кода кнопки в шапке таблицы
     *
     * @param string $_text Текст кнопки и подсказки на картинке
     * @param string $_action Яваскрипт для нажатия
     * @param string $_image Адрес картинки
     * @param string $_alias Алиас для добавления к имени и id ({TableName}.Button.{Alias})
     * @return string html код результата
     *
     */
    protected function GenButton($_text, $_action, $_image, $_alias)
    {
        $result=new \Mayral\Classes\Basic\String($this);
        $result->Text=$this->TableButtonTemplate->Text;
        $result->Replace('{Text}', $_text);
        $result->Replace('{Action}', $_action);
        $result->Replace('{Image}', $_image);
        $result->Replace('{Alias}', $_alias);
        $result->Replace('{Name}', $this->FullName());
        return $result->Text;
    }

    /**
     * Обработка собственных событий компонента
     *
     * @param string $_event_name Имя произошедшего события
     * @return string Имя метода используемого для обработки
     *
     */
    function CustomEvent($_event_name)
    {
        if($_event_name=="onpagechange")
        {
            $action_function_name=$this->OnPageChange;
        }
        if($_event_name=="onsortdown")
        {
            $action_function_name=$this->OnSortChangeDown;
        }
        if($_event_name=="onsortup")
        {
            $action_function_name=$this->OnSortChangeUp;
        }
        if($_event_name=="onrowclick")
        {
            $action_function_name=$this->OnRowClick;
        }
        return $action_function_name;
    }

    public function ChangePage($_sender, $_event, $_event_args=null)
    {
        if($_event_args!==null)
            $this->CurrentPage=$_event_args;
        \Mayral\Classes\Basic\ObjectsPool::Create()->AddToChangedComponents($this);
    }

    public function ChangeSortUp($_sender, $_event, $_event_args=null)
    {
        $this->SelectedRow=-1;
        $this->DataTable->Sort($_event_args);
        \Mayral\Classes\Basic\ObjectsPool::Create()->AddToChangedComponents($this);
    }

    public function ChangeSortDown($_sender, $_event, $_event_args=null)
    {
        $this->SelectedRow=-1;
        $this->DataTable->Sort($_event_args, false);
        \Mayral\Classes\Basic\ObjectsPool::Create()->AddToChangedComponents($this);
    }

    public function RowClick($_sender, $_event, $_event_args=null)
    {
        if($this->SelectedRow!=$_event_args)
            $this->SelectedRow=$_event_args;
        else
            $this->SelectedRow=-1;
        \Mayral\Classes\Basic\ObjectsPool::Create()->AddToChangedComponents($this);
    }

}

?>