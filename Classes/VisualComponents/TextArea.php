<?php

/* * **************************************************************************
  Description: простой textarea
  Author: Ivanov Kirill
  Created: 11.07.2008
  Version: 1.0.0

  Changes info:
  = 11.07.2008	(Ivanov Kirill):	создан
 * ************************************************************************** */

namespace Mayral\Classes\VisualComponents;

class TextArea extends BasicValueComponent
{

    public function __construct($_name, $_parent)
    {
        parent::__construct($_name, $_parent);
        $this->SetTemplateFileName('Templates/Components/'.'TextArea.html');
    }

}

?>