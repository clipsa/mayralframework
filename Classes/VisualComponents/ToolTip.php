<?php

/* * **************************************************************************
  Description: всплывающая подсказка
  Author: Zinchenko Sergey
  Version: 1.0.0

  Changes info:
  = 12.05.2011	(Zinchenko Sergey):	создан
 * ************************************************************************** */

namespace Mayral\Classes\VisualComponents;

class ToolTip extends BasicRepeater
{

    public function __construct($_name, $_parent)
    {
        parent::__construct($_name, $_parent);
        $this->SetTemplateFileName('Templates/Components/'.'ToolTip.html');
    }

    /**
     * добавление подсказки
     *
     * @param string $_client_name ID компонента ($component->FullName())
     * @param string $_text текст подсказки
     *
     */
    public function AddTip($_client_name, $_text)
    {
        if($_client_name!=''&&$_text!='')
        {
            $item=(object) array('Property'=>array('id'=>$_client_name, 'text'=>$_text));
            $this->Items->Add($item);
        }
    }

}

?>