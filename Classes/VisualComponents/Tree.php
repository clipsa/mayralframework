<?php

/* * **************************************************************************
  Description: дерево
  Author: Ivanov Kirill
  Version: 1.0.0

  Changes info:
  = 12.08.2008	(Ivanov Kirill):	создан
  + 26.09.2008	(Ivanov Kirill):	передалан
  + 27.09.2008	(Ivanov Kirill):	Expand, Collapse
 * ************************************************************************** */
namespace Mayral\Classes\VisualComponents;

class Tree extends BasicValueComponent
{
    /*
      событие:		пользователь кликает на нод
     */

    public $javascript_OnNodeClick='';
    public $OnNodeClick='';
    protected $NodeTemplate;
    protected $ChildContainerTemplate;
    public $Nodes;
    public $AllNodes;
    protected $LastNodeIndex=0;

    public function __construct($_name, $_parent)
    {
        parent::__construct($_name, $_parent);

        $this->Nodes=new \Mayral\Classes\Lists\BasicList($this);
        $this->AllNodes=array();
        $this->SetTemplateFileName('Templates/Components/'.'Tree.html');

        $this->SendProp['Value']=false;
        $this->Clear();

        $this->Style->Overflow='auto';
    }

    /*
      генерируем элементы
     */

    protected function GenerateNodes($_nodes)
    {
        $result='';
        for($i=0; $i<$_nodes->Count(); $i++)
        {
            $Node=$_nodes->Item($i);
            $result=$result.$this->GenerateNode($Node);
        }
        return $result;
    }

    protected function GenerateNode($_node)
    {
        $Node=$_node;
        $NodeTemplate=$this->NodeTemplate;
        $Child=$this->GenerateNodes($Node->Nodes);
        if($Child!=='')
        {
            $ChildContainer=$this->ChildContainerTemplate;
            $ChildContainer=str_replace('{Index}', $Node->Index, $ChildContainer);
            $Child=str_replace('{nodes}', $Child, $ChildContainer);
        }
        $NodeTemplate=str_replace('{Index}', $Node->Index, $NodeTemplate);
        $NodeTemplate=str_replace('{Text}', $Node->Text, $NodeTemplate);
        $NodeTemplate=str_replace('{child_containter}', $Child, $NodeTemplate);
        $Toggle_visibility='hidden';
        if($Child!=='')
        {
            $Toggle_visibility='visible';
        }

        $NodeIcon='';
        $NodeIcon_display='none';
        if($Node->Icon!=='')
        {
            $NodeIcon=$Node->Icon;
            $NodeIcon_display='inline';
        }
        $ExpColIcon='Images/icons/toggle_small.png';
        $ChildDisplay='none';
        if($Node->Expand)
        {
            $ExpColIcon='Images/icons/toggle_small_collapse.png';
            $ChildDisplay='block';
        }
        $NodeTemplate=str_replace('{ChildDisplay}', $ChildDisplay, $NodeTemplate);
        $NodeTemplate=str_replace('{ExpColIcon}', $ExpColIcon, $NodeTemplate);
        $NodeTemplate=str_replace('{NodeIcon}', $NodeIcon, $NodeTemplate);
        $NodeTemplate=str_replace('{NodeIcon_display}', $NodeIcon_display, $NodeTemplate);
        //$NodeTemplate = str_replace('{NodeEvents}', $NodeClickEventText, $NodeTemplate);
        $NodeTemplate=str_replace('{Toggle_visibility}', $Toggle_visibility, $NodeTemplate);


        return $NodeTemplate;
    }

    /**
     * разворачивает все ноды
     *
     */
    public function Expand()
    {
        foreach($this->AllNodes as $Node)
        {
            $Node->Expand=true;
        }
        \Mayral\Classes\Basic\ObjectsPool::Create()->AddToChangedComponents($this);
    }

    /**
     * сворачивает все ноды
     *
     */
    public function Collapse()
    {
        foreach($this->AllNodes as $Node)
        {
            $Node->Expand=false;
        }
        \Mayral\Classes\Basic\ObjectsPool::Create()->AddToChangedComponents($this);
    }

    protected function PrepareItemValue($_index, $_name, $_value)
    {
        //$_value = htmlspecialchars($_value);
        $_value=nl2br($_value);
        return $_value;
    }

    protected function BeforeGenerate()
    {
        parent::BeforeGenerate();
        //	заполнение массива переменный для подмены в шаблоне
        $Items=$this->GenerateNodes($this->Nodes);
        $Items=$this->ReplacePreGenerateVar($Items);
        $this->PreGenerateVar['Nodes']=$Items;
        $NodeClickEventText='';
        if($this->OnNodeClick!=='')
        {
            $NodeClickEventText='ServerArgEvent(\''.$this->FullName().'\', \'onnodeclick\', \'\');';
            //$NodeClickEventText = $this->GenerateEventString('onclick', $this->javascript_OnNodeClick, 'onnodeclick', $this->OnNodeClick, true, '''', '''.$this->FullName().''');
        }
        $this->PreGenerateVar['OnNodeClick']=$NodeClickEventText;
    }

    //	загружаем темплэйт компонента
    protected function LoadTemplate($_type)
    {
        $result=new \Mayral\Classes\Basic\String($this);
        $result->Text=parent::LoadTemplate($_type);

        $this->NodeTemplate=$result->CutTextBetween('<!--item_start-->', '<!--item_end-->');
        $this->ChildContainerTemplate=$result->CutTextBetween('<!--child_container_start-->', '<!--child_container_end-->');

        return $result->Text;
    }

    /**
     * добавляет узел в дерево
     *
     * @param Node $_node добавляемый узел
     * @param Node $_parent_node узел родитель
     *
     */
    public function AddNode($_node, $_parent_node='')
    {
        $ParentNode=$this->Nodes;
        $_node->Index=$this->LastNodeIndex;
        $_node->Level=0;
        if($_parent_node!=='')
        {
            $ParentNode=$_parent_node->Nodes;
            $_node->Level=$_parent_node->Level+1;
            $_node->ParentNode=$_parent_node;
        }
        $ParentNode->Add($_node);
        $this->AllNodes[$this->LastNodeIndex]=$_node;
        $this->LastNodeIndex++;

        \Mayral\Classes\Basic\ObjectsPool::Create()->AddToChangedComponents($this);
    }

    /**
     * Функция для обработки собственных событий в наследуемых компонентах
     *
     * @param string $_event_name Произошедшее событие
     * @return string Имя функции для исполнения
     *
     */
    protected function CustomEvent($_event_name)
    {
        if($_event_name==='onnodeclick')
        {
            return $this->OnNodeClick;
        }
        return '';
    }

    public function Clear()
    {
        $this->Nodes->Clear();
        unset($this->AddNode);
        $this->AddNode=array();
        $this->LastNodeIndex=0;
    }

}

?>