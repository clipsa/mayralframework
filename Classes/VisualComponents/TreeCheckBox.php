<?php

/* * **************************************************************************
  Description: дерево
  Author: Ivanov Kirill
  Version: 1.0.0

  Changes info:
  = 12.08.2008	(Ivanov Kirill):	создан
  + 26.09.2008	(Ivanov Kirill):	передалан
  + 27.09.2008	(Ivanov Kirill):	Expand, Collapse
 * ************************************************************************** */

namespace Mayral\Classes\VisualComponents;


class TreeCheckBox extends BasicValueComponent
{

    public $Nodes;
    public $AllNodes;
    protected $LastNodeIndex=0;

    public function __construct($_name, $_parent)
    {
        parent::__construct($_name, $_parent);
        $this->Nodes=new \Mayral\Classes\Lists\BasicList($this);
        $this->AllNodes=array();
        $this->SetTemplateFileName('Templates/Components/'.'TreeCheckBox.html');

        $this->SendProp['Value']=false;
        $this->Clear();
    }

    protected function GetNodes($_parent_node)
    {
        $Nodes=array();
        foreach($_parent_node->Nodes->Items as $_node)
        {
            $node=new \stdClass();
            $node->Index=$_node->Index;
            $node->Text=$_node->Text;
            $node->Checked=$_node->Checked;
            $node->Icon=$_node->Icon;
            $node->Nodes=$this->GetNodes($_node);
            $Nodes[]=$node;
        }
        return $Nodes;
    }

    /**
     * добавляет узел в дерево
     *
     * @param Node $_node добавляемый узел
     * @param Node $_parent_node узел родитель
     *
     */
    public function AddNode($_node, $_parent_node='')
    {
        $ParentNode=$this->Nodes;
        $_node->Index=$this->LastNodeIndex;
        $_node->Level=0;
        if($_parent_node!=='')
        {
            $ParentNode=$_parent_node->Nodes;
            $_node->Level=$_parent_node->Level+1;
            $_node->ParentNode=$_parent_node;
        }
        $ParentNode->Add($_node);
        $this->AllNodes[$this->LastNodeIndex]=$_node;
        $this->LastNodeIndex++;

        \Mayral\Classes\Basic\ObjectsPool::Create()->AddToChangedComponents($this);
    }

    public function OnSetValue($_tree_data)
    {
        $treeData=json_decode(stripslashes($_tree_data));
        if(isset($treeData->nodes))
        {
            foreach($treeData->nodes as $_node)
            {
                $this->UpdateNodes($_node);
            }
        }
    }

    protected function UpdateNodes($_node_data)
    {
        $nodeObj=$_node_data;
        $this->AllNodes[$nodeObj->index]->Checked=$nodeObj->checked=='true'||$nodeObj->checked==1?true:false;
        foreach($nodeObj->nodes as $_node)
        {
            $this->UpdateNodes($_node);
        }
    }

    public function GetCheckedItems()
    {
        $nodesArr=array();
        foreach($this->AllNodes as $_node)
        {
            if($_node->Checked)
            {
                $nodesArr[]=$_node;
            }
        }
        return $nodesArr;
    }

    public function Clear()
    {
        $this->Nodes->Clear();
        unset($this->AddNode);
        $this->AddNode=array();
        $this->LastNodeIndex=0;
    }

    public function CustomEvent($_event_name)
    {
        $result=parent::CustomEvent($_event_name);

        if($_event_name==='get_tree_data')
        {
            $result='Event_GetTreeData';
        }

        return $result;
    }

    protected function Event_GetTreeData($_sender, $_event_name, $_args)
    {
        $this->SendGlobalEvent('ClientData', json_encode($this->GetNodes($this)));
    }

}

?>