<?php

/* * **************************************************************************
  Description: менеджер файлов с редактированием
  Author: Kecheor
  Version: 1.0.0

  Changes info:
  =	02.12.2009	(Kecheor):			создан
 * ************************************************************************** */
namespace Mayral\Classes\VisualComponents\VisualComponentsComplex;

class EditFileExplorer extends FileExplorer
{

    public function __construct($_name, $_parent)
    {
        parent::__construct($_name, $_parent);

        $this->DirectoryTree->Style->Top='30';
        $this->FileBox->Style->Top='30';
        $this->FileBox->Style->Bottom='45';
        $this->FileBox->OnItemClick='OnFileClick';

        //Кнопки работы с папкой
        $NewDirButton=new Button('NewDir', $this);
        $NewDirButton->Style->Top='5';
        $NewDirButton->Style->Left='5';
        $NewDirButton->Style->Width='22';
        $NewDirButton->Image='Images/icons/folder__plus.png';
        $NewDirButton->OnClick='OnNewDirClick';

        $RenDirButton=new Button('RenDir', $this);
        $RenDirButton->Style->Top='5';
        $RenDirButton->Style->Left='30';
        $RenDirButton->Style->Width='22';
        $RenDirButton->Image='Images/icons/folder__pencil.png';
        $RenDirButton->OnClick='OnRenDirClick';

        $DelDirButton=new Button('DelDir', $this);
        $DelDirButton->Style->Top='5';
        $DelDirButton->Style->Left='55';
        $DelDirButton->Style->Width='22';
        $DelDirButton->Image='Images/icons/folder__minus.png';
        $DelDirButton->OnClick='OnDelDirClick';


        //Кнопки редактирования файла
        $RenFileButton=new Button('RenFile', $this);
        $RenFileButton->Style->Top='5';
        $RenFileButton->Style->Left='260';
        $RenFileButton->Style->Width='22';
        $RenFileButton->Image='Images/icons/document__pencil.png';
        $RenFileButton->OnClick='OnRenFileClick';
        $RenFileButton->Visible=false;

        $DelFileButton=new Button('DelFile', $this);
        $DelFileButton->Style->Top='5';
        $DelFileButton->Style->Left='285';
        $DelFileButton->Style->Width='22';
        $DelFileButton->Image='Images/icons/document__minus.png';
        $DelFileButton->OnClick='OnDelFileClick';
        $DelFileButton->Visible=false;

        //Кнопки загрузки файла
        $UploadPanel=new Panel('UploadPanel', $this);
        $UploadPanel->Style->Height='35';
        $UploadPanel->Style->Bottom='5';
        $UploadPanel->Style->Left='260';
        $UploadPanel->Style->Right='5';

        $UploadLabel=new Label('UploadLabel', $UploadPanel);
        $UploadLabel->Style->Top='9';
        $UploadLabel->Style->Left='10';
        $UploadLabel->Style->Width='100';
        $UploadLabel->Style->TextAlign='Right';
        $UploadLabel->Text='Загрузить файл';

        $UploadEdit=new Edit('UploadEdit', $UploadPanel);
        $UploadEdit->Type='File';
        $UploadEdit->Style->Top='7';
        $UploadEdit->Style->Left='120';
        $UploadEdit->Style->Right='120';

        $UploadButton=new Button('UploadButton', $UploadPanel);
        $UploadButton->Image='Images/icons/arrow_090.png';
        $UploadButton->Text='Загрузить';
        $UploadButton->Style->Top='4';
        $UploadButton->Style->Width='100';
        $UploadButton->Style->Right='10';
        $UploadButton->javascript_OnClick='ajaxFileUpload(\''.$this->FullName().'\', \''.$UploadEdit->FullName().'\')';
    }

    public function nFileClick($_sender, $_event, $_event_args=null)
    {
        $this->SetFileOpButtonsVisibility();
    }

    protected function SetFileOpButtonsVisibility()
    {
        $this->RenFile->Visible=(strlen($this->FileBox->SelectedFile)!=0);
        $this->DelFile->Visible=(strlen($this->FileBox->SelectedFile)!=0);
    }

    //Работа с папками кнопками над деревом
    //Создание
    public function nNewDirClick($_sender, $_event, $_event_args=null)
    {
        $this->FolderCreate($this->DirectoryTree->SelectedDirectory);
    }

    //Редактирование
    public function nRenDirClick($_sender, $_event, $_event_args=null)
    {
        $this->FolderRename($this->DirectoryTree->SelectedDirectory);
    }

    //Удаление папки
    public function nDelDirClick($_sender, $_event, $_event_args=null)
    {
        $f=new MessageDialog('MessageDialog', $this);
        $f->Style->Width='400';
        if(count(scandir($this->DirectoryTree->SelectedDirectory))>2)
        {
            $f->Work('Вы действительно хотите удалить папку \''.$this->DirectoryTree->SelectedDirectory.'\' вместе со всем содержимым?', 'ParseConfirmFolderDeleteDialogResult', $this->DirectoryTree->SelectedDirectory);
        }
        else
        {
            $f->Work('Вы действительно хотите удалить папку \''.$this->DirectoryTree->SelectedDirectory.'\'?', 'ParseConfirmFolderDeleteDialogResult', $this->DirectoryTree->SelectedDirectory);
        }
    }

    public function ParseConfirmFolderDeleteDialogResult($_result, $_argument)
    {
        if($_result==='yes')
        {
            $this->FolderDelete($_argument);
            $this->RefreshDirectoryTree();
            $this->RefreshFileBox();
        }
    }

    //Загрузка файла
    protected function CustomEvent($_event_name, $_event_args='')
    {
        $Result=parent::CustomEvent($_event_name, $_event_args);
        if($_event_name=='FileUpload'&&!empty($_FILES))
        {
            $Result='DoUpload';
        }
        return $Result;
    }

    public function DoUpload($_sender, $_event, $_event_args=null)
    {
        $this->FileUpload($this->DirectoryTree->SelectedDirectory);
        $this->RefreshFileBox();
        exit();
    }

    //Работа с файлами из панели текущего файла
    //Редактирование
    public function nRenFileClick($_sender, $_event, $_event_args=null)
    {
        $this->FileRename($this->FileBox->SelectedFile);
    }

    //Удаление файла
    public function nDelFileClick($_sender, $_event, $_event_args=null)
    {
        if(strlen($this->FileBox->SelectedFile)!=0&&file_exists($this->FileBox->SelectedFile))
        {
            $f=new MessageDialog('MessageDialog', $this);
            $f->Work('Вы действительно хотите удалить этот файл?', 'ParseConfirmFileDeleteDialogResult', $this->FileBox->SelectedFile);
        }
    }

    public function ParseConfirmFileDeleteDialogResult($_result, $_argument)
    {
        if($_result==='yes')
        {
            $this->FileDelete($_argument);
            $this->RefreshFileBox();
            $this->SetFileOpButtonsVisibility();
        }
    }

    //Обновление копонентов после изменения
    public function RefreshDirectoryTree()
    {
        $this->DirectoryTree->RefreshTree();
        $this->DirectoryTree->Collapse();
        \Mayral\Classes\Basic\ObjectsPool::Create()->AddToChangedComponents($this->DirectoryTree);
    }

    public function RefreshFileBox()
    {
        parent::RefreshFileBox();
        $this->SetFileOpButtonsVisibility();
    }

    //Функции работы с папками
    public function FolderCreate($_parent_folder)
    {
        if(is_writable($_parent_folder))
        {
            $f=new FormView_folder('FormView_folder', $this, $_parent_folder, '');
            $f->Title='Введите имя новой папки';
            $f->SetCenterPosition();
            $f->Work();
        }
        else
        {
            //$f=new MessageWarning('MessageDialog', $this);
            //$f->Work('Недостаточно прав для создания папки');
            $this->SendGlobalEvent('ShowMessage', 'Недостаточно прав для создания папки');
        }
    }

    public function FolderRename($_folder)
    {
        $parent_folder=substr($_folder, 0, strrpos($_folder, '/'));
        $folder_name=substr($_folder, strrpos($_folder, '/')+1);
        if(is_writable($_folder))
        {
            $f=new FormView_folder('FormView_folder', $this, $parent_folder, $folder_name);
            $f->SetCenterPosition();
            $f->Work();
        }
        else
        {
            //$f=new MessageWarning('MessageDialog', $this);
            //$f->Work('Недостаточно прав для переименования папки');
            $this->SendGlobalEvent('ShowMessage', 'Недостаточно прав для переименования папки');
        }
    }

    public function FolderDelete($_folder)
    {
        if(is_writable($_folder))
        {
            $DirListing=scandir($_folder);
            if(count($DirListing)>2)
            {
                foreach($DirListing as $diritem)
                {
                    if($diritem!='.'&&$diritem!='..')
                    {
                        if(is_writable($_folder.'/'.$diritem))
                        {
                            if(is_dir($_folder.'/'.$diritem))
                            {
                                $this->FolderDelete($_folder.'/'.$diritem);
                            }
                            if(is_file($_folder.'/'.$diritem))
                            {
                                $this->FileDelete($_folder.'/'.$diritem);
                            }
                        }
                        else
                        {
                            break;
                        }
                    }
                }
            }
            rmdir($_folder);
        }
        else
        {
            //$f=new MessageWarning('MessageDialog', $this);
            //$f->Work('Недостаточно прав для удаления папки');
            $this->SendGlobalEvent('ShowMessage', 'Недостаточно прав для удаления папки');
        }
    }

    //Функции работы с файлами
    public function FileUpload($_folder)
    {
        foreach($_FILES as $newfile)
        {
            if(empty($newfile['error']))
            {
                move_uploaded_file($newfile['tmp_name'], $_folder.'/'.$newfile['name']);
                chmod($_folder.'/'.$newfile['name'], 0666);
            }
        }
    }

    public function FileRename($_file)
    {
        $parent_folder=substr($_file, 0, strrpos($_file, '/'));
        $file_name=substr($_file, strrpos($_file, '/')+1);
        if(is_writable($_file))
        {
            $f=new FormView_file('FormView_file', $this, $parent_folder, $file_name);
            $f->SetCenterPosition();
            $f->Work();
        }
        else
        {
            //$f=new MessageWarning('MessageDialog', $this);
            //$f->Work('Недостаточно прав для переименования файла');
            $this->SendGlobalEvent('ShowMessage', 'Недостаточно прав для переименования файла');
        }
    }

    public function FileDelete($_file)
    {
        $r=unlink($_file);

        if($r==false)
        {
            //$f=new MessageWarning('MessageDialog', $this);
            //$f->Work('Недостаточно прав для переименования файла');
            $this->SendGlobalEvent('ShowMessage', 'Недостаточно прав для удаления файла');
        }
        return $r;
    }

}

?>