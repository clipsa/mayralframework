<?php

/* * **************************************************************************
  Description: файл-менеджер
  Author: Ivanov Kirill
  Version: 1.0.0

  Changes info:
  = 27.11.2009	(Ivanov Kirill):	создан
  + 02.04.2010    (Kecheor):			родитель всегда главная форма, но результат возвращается в ParentForm
 * ************************************************************************** */

namespace Mayral\Classes\VisualComponents\VisualComponentsComplex;

class FormSelectFile extends Form
{

    protected $ModalFunction='';
    protected $ParentForm;

    public function __construct($_name, $_parent)
    {
        $this->ParentForm=$_parent;
        parent::__construct($_name, \Project\Factory::GetInstance()->GetWorkSpace()->MainForm);
        $this->CreateFromXML('Classes/VisualComponents/VisualComponentsComplex/EditFileExplorer/LayoutFormSelectFile.xml');
    }

    public function Work($_path, $_function_name='')
    {
        $this->ModalFunction=$_function_name;
        $this->SetCenterPosition();
        $this->SetPath($_path);
        $this->ShowModal();
    }

    public function SetPath($_path)
    {
        $this->FileExplorer->SetPath($_path);
        $this->DirectoryTree->Collapse();
    }

    public function ButtonCancel_OnClick($_sender, $_event, $_event_args=null)
    {
        $this->Close();
    }

    public function ButtonOk_OnClick($_sender, $_event, $_event_args=null)
    {
        $ModalFunction=$this->ModalFunction;
        if($ModalFunction!=='')
        {
            $this->ParentForm->$ModalFunction('ok', $this->FileBox->SelectedFileUrl);
        }
        $this->Close();
    }

}

?>