<?php

/* * **************************************************************************
  Description: форма редактирования файла
  Author: Kecheor
  Version: 1.0.0

  Changes info:
  =	02.12.2009	(Kecheor):			создан
 * ************************************************************************** */

namespace Mayral\Classes\VisualComponents\VisualComponentsComplex;

class FormView_file extends Form
{

    protected $Path='';
    protected $File='';

    public function __construct($_name, $_parent, $_path, $_file)
    {
        parent::__construct($_name, $_parent);
        $this->CreateFromXML('Classes/VisualComponents/VisualComponentsComplex/EditFileExplorer/LayoutFormView_file.xml');
        $this->IconSrc='Images/icons/document.png';

        $this->Path=$_path;
        $this->File=$_file;
    }

    public function Work()
    {
        if(strlen($this->File)!=0)
        {
            $this->Edit_FILENAME->Value=$this->File;
        }
        $this->ShowModal();
    }

    public function ButtonOk_OnClick($_sender, $_event, $_event_args=null)
    {
        if($this->Save())
        {
            $this->Parent->RefreshFileBox();
            $this->Close();
        }
    }

    public function ButtonCancel_OnClick($_sender, $_event, $_event_args=null)
    {
        $this->Close();
    }

    protected function Save()
    {
        if(strlen($this->Edit_FILENAME->Value)!=0)
        {
            $r=rename($this->Path.'/'.$this->File, $this->Path.'/'.$this->Edit_FILENAME->Value);
            return $r;
        }
    }

}

?>