<?php

/* * **************************************************************************
  Description: форма редактирования папки
  Author: Kecheor
  Version: 1.0.0

  Changes info:
  =	02.12.2009	(Kecheor):			создан
 * ************************************************************************** */

namespace Mayral\Classes\VisualComponents\VisualComponentsComplex;

class FormView_folder extends Form
{

    protected $Path='';
    protected $Folder='';

    public function __construct($_name, $_parent, $_path, $_folder)
    {
        parent::__construct($_name, $_parent);
        $this->CreateFromXML('Classes/VisualComponents/VisualComponentsComplex/EditFileExplorer/LayoutFormView_folder.xml');
        $this->IconSrc='Images/icons/folder.png';

        $this->Path=$_path;
        $this->Folder=$_folder;
    }

    public function Work()
    {
        if(strlen($this->Folder)!=0)
        {
            $this->Edit_FOLDERNAME->Value=$this->Folder;
        }
        $this->ShowModal();
    }

    public function ButtonOk_OnClick($_sender, $_event, $_event_args=null)
    {
        if($this->Save())
        {
            $this->Parent->RefreshDirectoryTree();
            $this->Parent->RefreshFileBox();
            $this->Close();
        }
    }

    public function ButtonCancel_OnClick($_sender, $_event, $_event_args=null)
    {
        $this->Close();
    }

    protected function Save()
    {
        if(strlen($this->Edit_FOLDERNAME->Value)!=0)
        {
            if(strlen($this->Folder)==0)
            {
                return mkdir($this->Path.'/'.$this->Edit_FOLDERNAME->Value, 0666, true);
            }
            else
            {
                $r=rename($this->Path.'/'.$this->Folder, $this->Path.'/'.$this->Edit_FOLDERNAME->Value);
                return $r;
            }
        }
    }

    protected function CalcRigthsValue()
    {
        $value=array(0, 0, 0);
        if($this->Check_OwnerRead->Value!=0)
        {
            $value[0] += 400;
        }
        if($this->Check_OwnerWrite->Value!=0)
        {
            $value[0] += 200;
        }
        if($this->Check_OwnerExecute->Value!=0)
        {
            $value[0] += 100;
        }

        if($this->Check_GroupRead->Value!=0)
        {
            $value[1] += 40;
        }
        if($this->Check_GroupWrite->Value!=0)
        {
            $value[1] += 20;
        }
        if($this->Check_GroupExecute->Value!=0)
        {
            $value[1] += 10;
        }

        if($this->Check_OtherRead->Value!=0)
        {
            $value[2] += 4;
        }
        if($this->Check_OtherWrite->Value!=0)
        {
            $value[2] += 2;
        }
        if($this->Check_OtherExecute->Value!=0)
        {
            $value[2] += 1;
        }

        return '0'.$value[0].$value[1].$value[2];
    }

    protected function ShowRights($_value)
    {
        $owner=substr($_value, 1, 1);
        switch($owner)
        {
            case 1:
                $this->Check_OwnerExecute->Value=1;
                break;
            case 2:
                $this->Check_OwnerWrite->Value=1;
                break;
            case 3:
                $this->Check_OwnerExecute->Value=1;
                $this->Check_OwnerWrite->Value=1;
                break;
            case 4:
                $this->Check_OwnerRead->Value=1;
                break;
            case 5:
                $this->Check_OwnerRead->Value=1;
                $this->Check_OwnerExecute->Value=1;
                break;
            case 6:
                $this->Check_OwnerWrite->Value=1;
                $this->Check_OwnerRead->Value=1;
                break;
            case 7:
                $this->Check_OwnerExecute->Value=1;
                $this->Check_OwnerWrite->Value=1;
                $this->Check_OwnerRead->Value=1;
                break;
        }

        $group=substr($_value, 2, 1);
        switch($group)
        {
            case 1:
                $this->Check_GroupExecute->Value=1;
                break;
            case 2:
                $this->Check_GroupWrite->Value=1;
                break;
            case 3:
                $this->Check_GroupExecute->Value=1;
                $this->Check_GroupWrite->Value=1;
                break;
            case 4:
                $this->Check_GroupRead->Value=1;
                break;
            case 5:
                $this->Check_GroupRead->Value=1;
                $this->Check_GroupExecute->Value=1;
                break;
            case 6:
                $this->Check_GroupWrite->Value=1;
                $this->Check_GroupRead->Value=1;
                break;
            case 7:
                $this->Check_GroupExecute->Value=1;
                $this->Check_GroupWrite->Value=1;
                $this->Check_GroupRead->Value=1;
                break;
        }

        $other=substr($_value, 3, 1);
        switch($other)
        {
            case 1:
                $this->Check_OtherExecute->Value=1;
                break;
            case 2:
                $this->Check_OtherWrite->Value=1;
                break;
            case 3:
                $this->Check_OtherExecute->Value=1;
                $this->Check_OtherWrite->Value=1;
                break;
            case 4:
                $this->Check_OtherRead->Value=1;
                break;
            case 5:
                $this->Check_OtherRead->Value=1;
                $this->Check_OtherExecute->Value=1;
                break;
            case 6:
                $this->Check_OtherWrite->Value=1;
                $this->Check_OtherRead->Value=1;
                break;
            case 7:
                $this->Check_OtherExecute->Value=1;
                $this->Check_OtherWrite->Value=1;
                $this->Check_OtherRead->Value=1;
                break;
        }
    }

}

?>