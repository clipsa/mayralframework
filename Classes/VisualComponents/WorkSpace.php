<?php

/* * **************************************************************************
  Version: 1.1.0
  Description: класс рабочего пространства

  Changes info:
  + 07.06.2008:	Ivanov Kirill	создан
  = 10.07.2008:	Ivanov Kirill	наследован от OBasicClass
  + 13.11.2009:	Ivanov Kirill	ErrorList
  + 04.02.2010:	Ivanov Kirill	изменен принцип работы с измененными компонентами
 * ************************************************************************** */

namespace Mayral\Classes\VisualComponents;

class WorkSpace extends BasicComponent implements \Mayral\Classes\Interfaces\IActionListener
{

    public $MainForm;
    public $ApplicationTitle;
    protected $ErrorList;
    protected $_Data;
    protected $Notifies;

    public function __construct()
    {
        parent::__construct('', '');

        $this->ErrorList=new \Mayral\Classes\Lists\BasicList($this);

        $this->MainForm=new \Mayral\Classes\VisualComponents\BasicContainer('MainForm', '');
        $this->AllTempaltesFileName->Del(0);
        $this->SetTemplateFileName('Templates/Components/WorkSpace.html');
        $this->Notifies=array();
    }

    //	получение событий от клиента
    protected function GetEventFromClient()
    {
        if(isset($_REQUEST['event'])&&isset($_REQUEST['event_component']))
        {
            $component_name=$_REQUEST['event_component'];
            $component=\Mayral\Classes\Basic\ObjectsPool::Create()->GetObjectFromObjectsPool($component_name);
            if($component!=null)
            {
                //	проверяем можно ли в данном компоненте вызвать событие, для этого он должен быть видим и доступен
                if($component->ClientAccessible())
                {
                    $component->Event($_REQUEST['event'], isset($_REQUEST['event_arg'])?$_REQUEST['event_arg']:null);
                }
            }
        }
    }

    /*
      узнаем изменялся ли уже компонент
     */

    protected function ComponentChanged($_component)
    {
        return $_component->IsChanged;
    }

    //	получение значений компонентов от клиента
    protected function GetValuesFromClient()
    {
        /* 	
          переносим значение компонента вызвавшего событие в конец списка,
          чтобы он событие вызывалось только после того, как будут обработаны остальные значения
          p.s. это полумера, видимо надо вводить стек обработки
         */
        if(isset($_REQUEST['event_component']) && isset($_REQUEST[$_REQUEST['event_component']]))
        {
            $event_component=$_REQUEST['event_component'];
            $event_component_value=$_REQUEST[$_REQUEST['event_component']];
            unset($_REQUEST[$_REQUEST['event_component']]);
            $_REQUEST[$event_component]=$event_component_value;
        }
        foreach($_REQUEST as $key=> $value)
        {
            $component=\Mayral\Classes\Basic\ObjectsPool::Create()->GetObjectFromObjectsPool($key);
            if($component!=null)
            {
                if($component->ClientAccessible()&&!$this->ComponentChanged($component))
                {
                    //$value=stripslashes($value);
                    //$value=addcslashes($value, ''');
                    //	оч спорное решение - ServerValue разумеется отсутствует у компонента, сделано для того, чтобы при изменение со стороны клиента не было перерисовки
                    //	другой вариант - ввести блокировку на добавление в измененные
                    $component->ServerValue=$value;
                }
            }
        }
    }

    //	действия перед генерацией компонента
    protected function BeforeGenerate()
    {
        //	заполнение массива переменный для подмены в шаблоне
        $this->PreGenerateVar['MainForm']=$this->MainForm->Generate();
        $this->PreGenerateVar['ApplicationTitle']=$this->ApplicationTitle;
    }

    protected function GenerateChangedComponents()
    {
        $result=array();
        \Mayral\Classes\Basic\ObjectsPool::Create()->OptimizeChanges();
        foreach(\Mayral\Classes\Basic\ObjectsPool::Create()->Objects as $key=> $component)
        {
            if($component instanceof \Mayral\Classes\Interfaces\IComponent && $component->IsChanged)
            {
                $ParentName='none';
                if($component->Parent!=='')
                {
                    $ParentName=$component->Parent->FullName();
                }
                $result[]=array('parent'=>$ParentName.'_component_holder', 'name'=>$component->FullName().'_container', 'content'=>$component->Generate());

                $component->IsChanged=false;
            }
        }
        return $result;
    }

    //	генерация ответа на запрос
    public function Generate()
    {
        $result='';
        if(isset($_REQUEST['action']) && $_REQUEST['action']=='single_send')
        {
            if($this->GetData()==='')
            {
                $result['components']=$this->GenerateChangedComponents();
                $result['notifies']=$this->Notifies;
                $this->Notifies=array();
                $result['system']=array();
                if($this->KillMark)
                {
                    $result['system'][]=array('action'=>'reload');
                }
                $result=json_encode($result);
            }
            else
            {
                $result=$this->GetData();
            }

            $AllErrorText='';
            if($this->ErrorList->Count()>0)
            {
                for($i=0; $i<$this->ErrorList->Count(); $i++)
                {
                    $ErrorText=$this->ErrorList->Item($i);
                    $AllErrorText .= $ErrorText.'\r\n';
                }
            }
        }
        else
        {
            $result=parent::Generate();
            $result=stripslashes($result);
        }

        return $result;
    }

    //	работа рабочего пространства
    public function Work()
    {
        $this->ErrorList->Clear();
        $this->ClearData();
        $this->PrepareVars();

        $this->GetValuesFromClient();
        $this->GetEventFromClient();
        $result=$this->Generate();

        return $result;
    }

    //	подготавливает переданные переменные для работы
    //	не нужно после ввода utf
    protected function PrepareVars()
    {
        
    }

    /**
     * добавляет ошибку в ErrorList
     *
     * @param mixed $_error_message This is a description
     * @return mixed This is the return value description
     *
     */
    public function Error($_error_message)
    {
        $this->ErrorList->Add($_error_message);
    }

    public function MarkToKill()
    {
        if(isset($this->MainForm))
        {
            $this->MainForm->MarkToKill();
        }
        $this->KillMark=true;
    }

    protected function SetData($_data)
    {
        $this->_Data=$_data;
    }

    protected function GetData()
    {
        return $this->_Data;
    }

    protected function ClearData()
    {
        $this->SetData('');
    }

    public function ActionPerform($_event_name, $_event_argument)
    {
        if($_event_name==='ClientData')
        {
            if($this->GetData()!=='')
            {
               //Error('Trying to send multidata aswer');
            }
            $this->SetData($_event_argument);
        }
        
        if($_event_name==='ShowToast')
        {
            $this->Notifies[]=$_event_argument;
        }
    }

}

?>