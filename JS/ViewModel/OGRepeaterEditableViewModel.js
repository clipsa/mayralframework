function OGRepeaterEditableViewModel(_name,_form_name,_table_info,_table_data)
{
	var self =this;
	self.Name = _name;
	self.FormName = _form_name;
	self._InitProcess = true;
	self.table = new OTable(_table_info,_table_data);
	/**/
	$('#'+self.FormName).on('sendtabledata',function(){
		self.Request.SaveTableData(self.getTableData(), function(_response){
			if(_response.save_table_data_result)
			{
				ServerEventWithFormData(self.Name, 'save_form_data', self.FormName, '');
			}
		});
	});
	/**/
	self.Request={
		SaveTableData: function(_data,_callback){
			var Params={
				'data':	_data
			};
			ServerArgEvent(self.Name,'save_table_data',Params,function(_data){
				_callback($.parseJSON(_data));
			});
		},
		GetDataRequest: function(_params,_callback){
			ServerArgEvent(self.Name,'data_request',{
				'ValueName':_params['id'],
				'hash':_params['hash']
			},function(_data){
				_callback($.parseJSON(_data));
			});
		}
	};
	/**/
	self.getTableData = function(){
		var data=[];
		
		for(var i in self.table.row())
		{
			var cell=[];
			for(var j in self.table.row()[i].cell())
			{
				cell.push({
					'property':self.table.row()[i].cell()[j].id,
					'value':self.table.row()[i].cell()[j].value()
				});
			}
			data.push({
				'index':self.table.row()[i].index,
				'cell':cell,
				'is_deleted':self.table.row()[i].isDeleted()
			});
		}
		if(self.table.getProp('Appendable'))
		{
			data.pop();
		}
		return data;
	};
	self.deleteRow = function(){
		if(this.index==null)
		{
			self.table.row.remove(this);
		}
		else
		{
			this.isDeleted(true);
		}
	};
	/**/
	ko.bindingHandlers.autocomplete = {
		init: function (element, valueAccessor, allBindingsAccessor)
		{
			var observable = valueAccessor();
			var autocompleteRequestFunc = allBindingsAccessor().autocompleteRequestFunc||function(){};
			var autocompleteRequestParams = allBindingsAccessor().autocompleteRequestParams||{};
			/**/
			var Suggest = function (_src_data, _filter, _limit)
			{
				var src = _src_data;
				var limit = _limit || 0;
				var filter = _filter;
				var AddedToResults = 0;
				var result = [];
				for ( var i = 0, len = src.length; i < len && (AddedToResults < limit || limit === 0); i++ )
				{
					if ( filter.test(src[i].label) )
					{
						result.push($.extend(src[i], {
							label: src[i].label,
							value:  src[i].value,
							id:  src[i].id
						}));
						AddedToResults++;
					}
				}
				return result;
			}
			$(element).autocomplete({
				minLength: 3,
				autoFocus: true,
				source:    function (request, response)
				{
					var inst = this;
					if ( inst.lock === undefined )
					{
						inst.lock = false;
					}
					if ( inst.lock )
					{
						return;
					}
					var hash = request.term.substr(0, inst.options.minLength).toLowerCase();
					if ( inst._Cache == undefined )
					{
						inst._Cache = {};
					}
					if ( inst._Cache[hash] != undefined )
					{
						if ( inst._Cache[hash].length > 0 )
						{
							completeSuggest(request.term, inst._Cache[hash]);
						}
						return;
					}
					inst.lock = true;
					autocompleteRequestFunc($.extend({
						'hash':hash
					},ko.toJS(autocompleteRequestParams)), function (_data, _function, _request)
					{

						inst._Cache[hash] = _data;

						completeSuggest(request.term, inst._Cache[hash]);

						inst.lock = false;
					});
					return;
					function completeSuggest(_term, _data)
					{
						var filter = new RegExp('([ ]|^)' + _term, 'i');
						var result = Suggest(_data, filter, 10);
						response(result);
					}

				},
				change:    function (event, ui)
				{
					if ( ui.item == null )
					{
						if ( observable() != undefined )
						{
							$(element).val(observable().label);
						}
						else
						{
							$(element).val('');
						}
					}
				},
				select:    function (event, ui)
				{
					$(element).val(ui.item.label);
					return false;
				}/*,
				appendTo:  $(element).parent().find('div.ml-autocomplete-edit-list'),
				position:  {
					my:        "left top",
					at:        "left top",
					collision: "none",
					of:        $(element).parent().find('div.ml-autocomplete-edit-list')
				}*/
			});
			$(element).data("autocomplete")._renderItem = function (ul, item)
			{
				return $("<li></li>").data("item.autocomplete",item).append('<a><span class="bl-app-font-alt">' + item.label + '</span></a>').appendTo(ul);
			}
			/**/

			//handle the field changing
			ko.utils.registerEventHandler(element, "autocompleteselect", function (event, ui)
			{
				observable(ui.item);
			});
			//			$(element).val(observable().label);
			//handle disposal (if KO removes by the template binding)
			ko.utils.domNodeDisposal.addDisposeCallback(element, function ()
			{
				$(element).autocomplete("destroy");
			});

		}
	};
	ko.bindingHandlers.datepicker = {
		init: function(element, valueAccessor) {
			var options = $.extend({
				//				altFormat: 'dd-mm-yy',
				dateFormat: 'dd.mm.yy',
				dayNames: ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Субота'],
				dayNamesMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
				firstDay: 1,
				monthNamesShort: ['янв','фев','мар','апр','мая','июн','июл','авг','сен','окт','ноя','дек'],
				monthNames: ['Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь']
			}, valueAccessor());

			$(element).datepicker(options);
		}
	};
	ko.bindingHandlers.OGRepeaterCell = {
		init: function(element, valueAccessor, allBindingsAccessor) {
			var col = valueAccessor()||{};
			//			var col = allBindingsAccessor().OGRepeaterCol||{};
			//			console.log(cell);
			//			console.log(col);
			switch(col.type)
			{
				case 'edit':
					$(element).html($('<input/>').attr('data-bind','value:$data.value'));
					break;
				case 'autocomplete':
					$(element).html($('<input/>').attr('data-bind','autocomplete:$data.value,autocompleteRequestFunc:$root.Request.GetDataRequest,autocompleteRequestParams:{id:col.id},value:$data.value().label')).append($('<div/>').addClass('ml-autocomplete-edit-list'));
					break;
				case 'listbox':
					$(element).html($('<select/>').attr('data-bind',"options:col.getProp('Items'),optionsText:'Name',optionsValue:'Value',value:$data.value,optionsCaption: 'Выбрать...'"));
					break;
				case 'datepicker':
					$(element).html($('<input/>').attr('data-bind',"datepicker:{},value:$data.value"));
					break;
				default:
					$(element).html($('<span/>').attr('data-bind','text:$data.value'));
			}
			
		}
	};
	self.Round = function(_num,_sign){
		var Num = _num;
		var Exp = _sign||2;
		var Mult = Math.pow(10, Exp);
		return Math.round(Num*Mult)/Mult;
	};
//	(function(){
//		
//		})();
};

OGRepeaterEditableViewModel.prototype.bindModel=function(){
	var self = this;
	$('#'+self.Name).data('OGRepeaterEditableViewModel',self);
	
	ko.applyBindings(self,$('#'+self.Name).get(0));
	
	$.extend($('#'+self.Name).get(0),{
		'getValue':function(){
			return $(this).data('OGRepeaterEditableViewModel').getTableData();
		}
	});
	
};
function OTable(_prop_data,_row_data)
{
	var thisTable = this;
	thisTable.prop = {};
	thisTable.col=ko.observableArray();
	thisTable.row=ko.observableArray();

	thisTable.addEmptyRow = ko.computed(function(){
		var row = this.row();
		
		if(!this.getProp('Appendable')) return false;
		
		if(row.length===0) return true;
		for(var i in row[row.length-1].cell())
		{
			var value = row[row.length-1].cell()[i].value();
			
			if(typeof(value)==="object")
			{
				for(var prop in value)
				{
					if(value[prop]!=undefined) return true;
				}
			}
			else
			{
				if(value!=undefined) return true;
			}
		}
		return false;
	},thisTable).extend({
		throttle: 100
	});
	thisTable.addEmptyRow.subscribe(function(_add_empty){
		if(_add_empty) this.pushRow();
	},thisTable);

	//	thisTable.colData = _prop_data.Columns;
	/**/
	for(var tableProp in _prop_data)
	{
		if(tableProp ==='Columns')
		{
			thisTable.colData = _prop_data.Columns;
			continue;
		}
		thisTable.setProp(tableProp, _prop_data[tableProp]);
	}
	/**/
	thisTable.rowData = _row_data;
	
	for(var i in thisTable.colData)
	{
		var col = new OCol(thisTable.colData[i].ValueName,thisTable.colData[i].ViewName, thisTable.colData[i].EditType);
		for(var prop in thisTable.colData[i])
		{
			if(prop==='ValueName'||prop==='ViewName'||prop==='EditType') continue;
			col.setProp(prop, thisTable.colData[i][prop]);
		}
		thisTable.col.push(col);
	}
	
	for(var j in thisTable.rowData)
	{
		thisTable.pushRow(j,thisTable.rowData[j]);
	}
	if(thisTable.row().length==0 && thisTable.getProp('Appendable'))
	{
		thisTable.pushRow();	
	}
};
OTable.prototype.pushRow = function(_index,_data)
{
	var thisTable = this;
	var rowData = _data||[];
	var indexRow = _index||null;
	var idRow = rowData.ID||null;
	var row = new ORow(idRow,indexRow);
	for(var j in thisTable.col())
	{
		var col = thisTable.col()[j];
		var cell = new OCell(col.id);
		switch(col.type)
		{
			case 'autocomplete':
				cell.value({
					'id':rowData[col.id],
					'label':rowData[col.getProp('LabelName')],
					'value':rowData[col.getProp('LabelName')]
				});
				break;
			case 'listbox':
				col.setProp(rowData['Items']);
			default:
				cell.value(rowData[col.id]);
		}
		var dependentColumns = col.getProp('DependentColumns')||[];
		if(dependentColumns.length>0)
		{
			for(var k=0; k<dependentColumns.length;k++)
			{
				cell.value.subscribe(function(_value){
					this.row.getValue(this.key).value(_value[this.key]);
				},{
					'row':row,
					'key':dependentColumns[k]
				});
			}
		}
		row.cell.push(cell);
	}
	thisTable.row.push(row);
};
OTable.prototype.getProp = function(_prop)
{
	return this.prop[_prop]||undefined;
};
OTable.prototype.setProp = function(_prop,_val)
{
	this.prop[_prop]=_val;
};
function OCol(_id,_name,_type)
{
	this.id=_id;
	this.name=_name||_id;
	this.type=_type;
	this.prop={};
};
OCol.prototype.getProp = function(_prop)
{
	return this.prop[_prop]||undefined;
}
OCol.prototype.setProp = function(_prop,_val)
{
	this.prop[_prop]=_val;
}
function ORow(_id,_index)
{
	this.id=_id;
	this.index=_index||null;
	this.cell=ko.observableArray();
	this.isDeleted = ko.observable(false);
}
ORow.prototype.getValue = function(_id)
{
	var self = this;
	for(var i in self.cell())
	{
		if(self.cell()[i].id==_id)
			return self.cell()[i];
	}
	return null;
}
function OCell(_id)
{
	this.id = _id||null;
	this.value = ko.observable();
//	this.value.subscribe(function(_aa){
////		console.log(_aa);
//	});
}