function OTreeCheckBoxViewModel(_name,_tree_data)
{
	var self =this;
	self.Name = _name;
	self.tree = new OTree(_tree_data);

	self.getTreeData = function () {
	    //  var treeData = ko.toJSON(self.tree);
	    var treeData = { nodes: self.getLevelData(self.tree.nodes()) }; //self.getLevelData(self.tree.nodes());
	    //console.log(treeData);
	    return ko.toJSON(treeData);
	};
	self.getLevelData = function (_nodes) {
	    var nodes = _nodes;
	    var result = [];
	    for (var i_node = 0; i_node < nodes.length; i_node++) {
	        var node = nodes[i_node];
	        result.push({
	            index: node.index,
	            checked: node.checked(),
	            nodes: self.getLevelData(node.nodes())
	        });
	    }
	    return result;
	};
	
	self.expanderOnClick = function(_node,_event)
	{
		_node.expanded(!_node.expanded());
	};
};

OTreeCheckBoxViewModel.prototype.bindModel=function()
{
	var self = this;
	$('#'+self.Name).data('OTreeCheckBoxViewModel',self);
	
	ko.applyBindings(self,$('#'+self.Name).get(0));
	
	$.extend($('#'+self.Name).get(0),{
		'getValue':function(){
			return $(this).data('OTreeCheckBoxViewModel').getTreeData();
		}
	});
	
};

function OTree(_tree_data)
{
	this.nodes=ko.observableArray();
	for(var i=0; i<_tree_data.length; i++)
	{
		this.nodes.push(new ONode(_tree_data[i]));
	}
};
function ONode(_node_data)
{
	var self = this;
	self.text = _node_data.Text;
	self.index = _node_data.Index;
	self.icon = _node_data.Icon;
	self.expanded = ko.observable(false);
	self.nodes = ko.observableArray();
	self.checked = ko.observable(_node_data.Checked);
	self.checked.subscribe(function(_new_val){
		var check = _new_val;
		for(var i=0;i<this.nodes().length;i++)
		{
			this.nodes()[i].checked(check);
		}
	},self);
	/**/
	for(var i=0;i< _node_data.Nodes.length;i++)
	{
		self.nodes.push(new ONode(_node_data.Nodes[i]));	
	}
};