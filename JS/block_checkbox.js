$(document).ready(function(){
    $(document).on("click", ".OBlockCheckBox", function(){
        $(".OBlockCheckBox").removeClass("selected");
        $(".OBlockCheckBox input").val(0);
        $(this).toggleClass("selected");
        if($(this).find("input").val()==1)
        {
            $(this).find("input").val(0);
        }
        else
        {
            $(this).find("input").val(1);
        }
    });
    
    $(this).on('AjaxComplete', function(){
        $(".OBlockCheckBox input").each(function(){
            if($(this).val()==1)
            {
                $(this).parent().addClass("selected");
            }
        }); 
    });
});