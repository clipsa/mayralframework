/****************************************************************************
 Description: 
 Author: Ivanov Kirill
 Created: 13.07.2008
 Version: 1.0.0
 
 Changes info: 
 = 13.07.2008	(Ivanov Kirill):	создан
 + 29.08.2008    (Kecheor):          метод ServerArgEvent
 + 04.09.2008    (Kecheor):          добавлена проверка передается ли объект или id в вызове события
 + 17.09.2008    (Ivanov Kirill):    outerHTML
 ****************************************************************************/
function StartWait(_block_on_event) {
    if (_block_on_event) {
        $('body').append("<div class='m_fade'><img src='Images/metro_ui/preloader-w8-cycle-black.gif' /></div>");
    }
    else {
        $("#status_div").fadeIn("fast");
    }
}
function StopWait() {
    $("#status_div").fadeOut("slow");
    $(".m_fade").remove();
}
function WorkSpaceSubmit() {
    document.forms['WorkSpace'].submit();
}

/*
 обработка ответа сервера
 */
function ParseServerResponse(_response) {
    var result = $.parseJSON(_response);

    if (result.notifies)
    {
        var notifies = result.notifies;
        for (var i = 0; i < notifies.length; i++)
        {
            $.Notify({
                caption: notifies[i].caption,
                content: notifies[i].content,
                shadow: true
            });
        }
    }

    if (result.system)
    {
        var system = result.system;
        for (var i = 0; i < system.length; i++)
        {
            switch (system[i].action)
            {
                case 'reload':
                    {
                        setTimeout('window.location.reload()', 3000);
                        return;
                    }
                    break;
            }
        }
    }

    if (result.components)
    {
        var components = result.components;
        for (var i = 0; i < components.length; i++)
        {
            if ($("#" + components[i].name).length) {
                if (components[i].content == "") {
                    components[i].content = " ";
                }
                $("#" + components[i].name).outerHTML(components[i].content);
            }
            else {
                if ($("#" + components[i].parent).length) {
                    $("#" + components[i].parent).append(components[i].content);
                }
            }
        }
    }

    StopWait();
    AfterAjax();
    $(document).trigger('AjaxComplete');
    return _response;
}

/*
 функция отправляет сообщение о событии на сервер 
 */
function ServerEvent(_component, _event, _block_on_event) {
    if (typeof (_component) != "object") {
        _component = document.getElementById(_component);
    }
    var value = _component.value;

    if (_component.type == "checkbox" && !_component.checked) {
        value = 0;
    }

    var c_id = _component.id;
    StartWait(_block_on_event);
    $.post("index.php", {
        action: "single_send",
        event_arg: value,
        event: _event,
        event_component: c_id
    }, function(data) {
        ParseServerResponse(data);
    });
}
/*
 функция отправляет сообщения о событии на сервер и содержания родительской формы
 */
function ServerEventWithFormData(_component, _event, _container_id, _arg, _block_on_event) {

    if (typeof (_component) != "object") {
        _component = document.getElementById(_component);
    }

    StartWait(_block_on_event);

    var r_data = new Object();
    r_data.action = "single_send";
    $(r_data).attr("event", _event);
    $(r_data).attr("event_component", _component.id);
    $(r_data).attr("event_arg", _arg);
    $("#" + _container_id + "").find("div.OGRepeaterEditable").each(function() {
        $(r_data).prop($(this).prop("id"), this.getValue());
    });
    $("#" + _container_id + "").find(":input, textarea").each(function() {
        $(r_data).prop($(this).prop("id"), $(this).prop("value"));
    });
    $("#" + _container_id + "").find("textarea").each(function() {
        $(r_data).prop($(this).prop("id"), $(this).val());
    });
    $("#" + _container_id + "").find("div.OTreeCheckBox").each(function() {
        $(r_data).prop($(this).prop("id"), this.getValue());
    });
    $("#" + _container_id + "").find("table.striped").each(function() {
        var val = new Array();
        $(this).find('input[type=checkbox]:checked').each(function() {
            val.push($(this).val());
        });
        $(r_data).prop($(this).parent().parent().prop("id"), val);
    });

    for (ckitem in CKEDITOR.instances) {
        //if (CKEDITOR.instances[item]) {
        $(r_data).prop(ckitem, CKEDITOR.instances[ckitem].getData());
        //}
    }

    $(".sortable").each(function() {
        var new_arr = new Array();
        $(this).find("li").each(function(_index) {
            new_arr.push([$(this).attr("data-object"), _index]);
        });
        $(r_data).prop($(this).attr("id"), new_arr);
    });

    $("#" + _container_id + "").find(":input[type='checkbox']").each(function() {
        $(r_data).prop($(this).prop("id"), $(this).prop("checked"));
    });
    $("#" + _container_id + "").find(":input[type='radio']").each(function() {
        $(r_data).prop($(this).prop("id"), $(this).prop("checked"));
    });

    $.post("index.php", r_data, function(data) {
        ParseServerResponse(data);
    });
}
function ServerArgEvent(_component, _event, _arg, _block_on_event, _callback) {
    StartWait(_block_on_event);
    if (typeof (_component) != "object") {
        _component = document.getElementById(_component);
    }
    var c_id = _component.id;

    var value = _component.value;

    if (_component.type == "checkbox" && !_component.checked) {
        value = 0;
    }
    var r_data = new Object();
    r_data = {
        action: "single_send",
        event_arg: _arg,
        event: _event,
        event_component: c_id
    };
    $(r_data).attr(c_id, value);
    $.post("index.php", r_data, function(data) {
        data = ParseServerResponse(data);
        if (_callback != null) {
            _callback(data);
        }
    });
}

function SendContainerData(_container_id) {
    var r_data = new Object();
    r_data.action = "single_send";
    $("#" + _container_id + "").find(":input").each(function() {
        $(r_data).attr($(this).attr("id"), $(this).attr("value"));
    });
    $.post("index.php", r_data, function(data) {
        ParseServerResponse(data);
    });
}
function run_js(name) {
    $("#" + name + " script").each(function() {
        $(this).attr("src", $(this).attr("src"));
    });
}
function ping() {
    var r_data = new Object();
    r_data = {
        action: "single_send"
    };
    $.post("index.php", r_data, function(data) {
        ParseServerResponse(data);
    });

    setTimeout("ping()", 60000);
}

//$(document).ready(function() {ping();});

jQuery.fn.outerHTML = function(s) {
    return (s)
            ? this.before(s).remove()
            : jQuery("<p>").append(this.eq(0).clone()).html();
}

//для компонента OToolTip
component_ids = new Array();

function AddToolTip(_id, _text) {
    for (var i = 0; i < component_ids.length; i++) {
        if (component_ids[i][0] == _id) {
            return false;
        }
    }
    component_ids.push(new Array(_id, _text));
}

function ShowTip(_event_params, _index) {
    var tip_div = $("#tip_div");
    if (tip_div.length == 0) {
        $("body").append("<div class='OToolTip' id='tip_div'></div>");
        tip_div = $("#tip_div");
    }
    tip_div.html(component_ids[_index][1]);
    tip_div.css({
        left: _event_params.pageX,
        top: _event_params.pageY
    });
    tip_div.fadeIn("fast");
}

function GenerateTips() {
    for (var i = 0; i < component_ids.length; i++) {
        var obj = $("#" + component_ids[i][0]).parent();
        if (obj.length > 0) {
            obj.attr("tip_index", i);
            obj.mouseenter(function(e) {
                ShowTip(e, $(this).attr("tip_index"));
            });
            obj.mouseleave(function() {
                $("#tip_div").fadeOut("fast");
            });
        }
    }
}

function AfterAjax() {
    GenerateTips();

    $('[data-role=dropdown]').dropdown({
        effect: 'slide'
    });
    $('[data-transform=input-control]').inputTransform();
    $('[data-role=input-control], .input-control').inputControl();
}

$(document).on("click", "input:checkbox[name='checkall']", function(){
    $(this).closest("div.OuterInnerBorder").find("input:checkbox[name!='checkall']").eq(0).click();
    $(this).closest("div.OuterInnerBorder").find("input:checkbox[name!='checkall']").prop('checked', $(this).prop("checked"));
});