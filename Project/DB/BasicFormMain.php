<?php

/* * **************************************************************************
  Description: главная форма (базовая - без формы логина)
  Author: Ivanov Kirill
  Version: 1.0.0

  Changes info:
  = 12.04.2011	(Ivanov Kirill):	создан
 * ************************************************************************** */

namespace Project\DB;

class BasicFormMain extends \Mayral\Classes\VisualComponents\Form implements \Mayral\Classes\Interfaces\IActionListener
{

    public $UserInfo;
    public $UserGroupOption = array();

    public function __construct($_name, $_parent)
    {
        global $Options;

        parent::__construct($_name, $_parent);
        $this->CreateFromXML('Project/DB/Layout/LayoutBasicFormMain.xml');
        $this->HideMainControls();

        $this->UserGroupOption[] = 'Администрирование';

        $this->UserInfo = new \Project\DB\DBClass\CurrentUser('', $this, 'MainDBConnection');

        $this->CloseButton->Visible = false;

        $this->NavigatorTop->DeleteLastTemplate();
        $this->NavigatorTop->SetTemplateFileName('Project/DB/Template/TreeNavigator.html');

        $this->NavigatorTop->Title = 'Mayral DBPattern';
    }

    protected function Fill_Navigator()
    {
        $this->Navigator->Items->Clear();

        if ($this->UserInfo->GroupOptionValue('Администрирование') === 'yes')
        {
            $group = $this->Navigator->Add('Администрирование', 'icon-user-3', 'item');

            $this->Navigator->Add('Группы пользователей', '', 'item', array('name' => 'user_group', 'show' => 'controller', 'parent' => $this->PanelMain/* , 'need_close_all' => true */), null, $group);
            $this->Navigator->Add('Пользователи', '', 'item', array('name' => 'user', 'show' => 'controller', 'parent' => $this->PanelMain/* , 'need_close_all' => true */), null, $group);
        }
    }

    public function ButtonLogoff_OnClick($_sender, $_event, $_event_args = null)
    {
        $this->Logout();
    }

    public function Logout()
    {
        $this->SendGlobalEvent('UserLogout');
        $this->SendGlobalEvent('ShowToast', array('caption' => 'Уже уходите?', 'content' => 'Mayral будет скучать :('));
        \Project\Factory::GetInstance()->GetWorkSpace()->MarkToKill();
    }

    protected function HideMainControls()
    {
        $this->Navigator->Visible = false;
        $this->PanelMain->Visible = false;
        $this->NavigatorTop->Visible = false;
    }

    protected function CustomEvent($_event_name, $_event_args = '')
    {
        $Result = '';
        if ($_event_name == 'on_navigator_click')
        {
            $Result = 'Event_on_navigator_click';
        }
        if ($_event_name == 'on_topnavigator_click')
        {
            $Result = 'Event_on_topnavigator_click';
        }
        return $Result;
    }

    //	вызов форм при клике на пункт навигатора
    protected function Event_on_navigator_click($_sender, $_event, $_event_args = null)
    {
        $this->FormController->CloseAll();
        $this->NavigatorWork($this->Navigator, $_event_args);
    }

    protected function Event_on_topnavigator_click($_sender, $_event, $_event_args = null)
    {
        $this->SendGlobalEvent('OnTopNavigatorClick', $this->NavigatorTop->GetItem($_event_args)->Object);
    }

    protected function NavigatorWork($_navigator, $_event_args)
    {
        $Item = $_navigator->GetItem($_event_args);

        if ($Item !== '')
        {
            $this->AddToController($Item->Object);
        }
    }

    protected function AddToController($_params)
    {
        $this->RemoveButtons();

        $this->FormController->HideAll();
        if (($_params['show'] === 'controller' || $_params['show'] === 'view_record_form') && !$this->FormController->SetActiveForm($_params['name']))
        {
            $result = $this->SendGlobalEvent('ShowForm', $_params);
            $form = array_pop($result);
            if ($_params['show'] === 'controller' && $form != null)
            {
                $form->Work();
            }
            $this->FormController->Add($form);
        }
    }

    public function ActionPerform($_event_name, $_event_argument)
    {
        if ($_event_name === 'DB_connect_fail')
        {
            $date = date('d.m.Y H:i:s', time());
            $this->SendGlobalEvent('ShowMessage', $date . ': не удалось установить соединение с базой данных.');
        }

        if ($_event_name == 'AddToFormController')
        {
            $_event_argument['parent'] = \Project\Factory::GetInstance()->GetWorkSpace()->MainForm->PanelMain;
            $this->AddToController($_event_argument);
        }

        if ($_event_name == 'AddButtons' && is_array($_event_argument) && count($_event_argument) > 0)
        {
            foreach ($_event_argument as $key => $val)
            {
                if (($item_index = $this->NavigatorTop->IsExist($val['button_text'])) === false)
                {
                    $this->NavigatorTop->Add($val['button_text'], $val['icon_class'], 'item', $val['button_arguments']);
                }
                else
                {
                    $this->NavigatorTop->GetItem($item_index)->Object = $val['button_arguments'];
                }
            }
        }

        if ($_event_name == 'RemoveButtons')
        {
            $this->RemoveButtons($_event_argument['form_name']);
        }

//        if ($_event_name == 'FormIsClosed')
//        {
//            $this->FormController->CloseTab($this->FormController->FormIndex($_event_argument));
//            $this->RemoveButtons($_event_argument);
//        }
    }

    protected function RemoveButtons($_form_name = '')
    {
        $cnt = $this->NavigatorTop->Items->Count();
        for ($i = $cnt - 1; $i >= 0; $i--)
        {
            if ((@$this->NavigatorTop->GetItem($i)->Object['form_name'] == $_form_name || $_form_name == '') && $this->NavigatorTop->GetItem($i)->Property['TYPE'] != 'static_item')
            {
                $this->NavigatorTop->RemoveItem($i);
            }
        }
    }

}

?>