<?php

/* * **************************************************************************
  Description: GRepeater с множественным выбором строк
  Author: Zinchenko Sergey
  Version: 1.1.0

  Changes info:
  = 13.09.2012	(Zinchenko Sergey):	создан
  + 25.02.2013  (Zinchenko Sergey):	обработка клика по строке
 * ************************************************************************** */

namespace Project\DB\Components;

class GRepeaterMultiselectComponent extends \Mayral\Classes\VisualComponents\GRepeater
{
    /*
     * 
     */

    public $OnRowClick;

    public function __construct($_name, $_parent)
    {
        parent::__construct($_name, $_parent);

        $this->SetTemplateFileName('Project/DB/Template/GRepeater_table_multiselect.html');
    }

    public function CustomEvent($_event_name)
    {
        $result=parent::CustomEvent($_event_name);

        if($_event_name=='on_row_click')
        {
            $result=$this->OnRowClick;
        }

        return $result;
    }

}

?>
