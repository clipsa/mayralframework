<?php

/* * **************************************************************************
  Description: навигатор
  Author: Ivanov Kirill
  Version: 1.0.0

  Changes info:
  = 06.07.2009	(Ivanov Kirill):	создан
 * ************************************************************************** */
namespace Project\DB\Components;

class Navigator extends \Mayral\Classes\VisualComponents\BasicRepeater
{

    protected $CategoryTeamplate;

    public function __construct($_name, $_parent)
    {
        parent::__construct($_name, $_parent);

        $this->SetTemplateFileName('Project/DB/Template/Navigator.html');
    }

    public function Add($_item_text, $_class='', $_type='item', $_object='', $_additional_info=array())
    {
        $this->Items->Add(new NavigatorItem($this, $_item_text, $_class, $_type, $_object, $_additional_info));
        \Mayral\Classes\Basic\ObjectsPool::Create()->AddToChangedComponents($this);
    }

    public function OnItemChange($_item)
    {
        $this->Parent->OnTaskListItemChange();
    }

    protected function GetItemTemplate($_index)
    {
        $Item=$this->Items->Item($_index);
        $ItemTemplate=$this->ItemTemplate;
        if($Item->Property['TYPE']==='category')
        {
            $ItemTemplate=$this->CategoryTeamplate;
        }
        return $ItemTemplate;
    }

    //	загружаем темплэйт компонента
    protected function LoadTemplate($_type)
    {
        $result=new \Mayral\Classes\Basic\String($this);
        $result->Text=parent::LoadTemplate($_type);

        $this->CategoryTeamplate=$result->CutTextBetween('<!--category_item_start-->', '<!--category_item_end-->');

        return $result->Text;
    }

}

?>