<?php

/* * **************************************************************************
  Description: навигатор
  Author: Ivanov Kirill
  Version: 1.0.0

  Changes info:
  = 06.07.2009	(Ivanov Kirill):	создан
 * ************************************************************************** */

namespace Project\DB\Components;

class NavigatorItem extends \Mayral\Classes\Basic\BasicClass
{

    public $Property;
    public $Object;

    public function __construct($_parent, $_text, $_class, $_type, $_object, $_additional_info=array())
    {
        parent::__construct('', $_parent);
        $this->Property=array();
        $this->Property['TEXT']=$_text;
        $this->Property['TYPE']=$_type;
        $this->Property['ItemClass']=$_class;
        if(is_array($_additional_info))
        {
            foreach($_additional_info as $key=> $val)
            {
                $this->Property[$key]=$val;
            }
        }

        $this->Object=$_object;
    }

    public function Clear()
    {
        unset($this->Property);
        $this->Property=array();
    }

    public function PropertyValue($_name)
    {
        $Result='';
        if(isset($this->Property[$_name]))
        {
            $Result=$this->Property[$_name];
        }
        return $Result;
    }

}

?>