<?php

/* * **************************************************************************
  Description: навигатор с детьми
  Author: Zinchenko Sergey
  Created: 22.01.2013
  Version: 1.0.0

  Changes info:
  = 22.01.2013	(Zinchenko Sergey):	создан
 * ************************************************************************** */
namespace Project\DB\Components;

use Mayral\Classes\Lists;
use Mayral\Classes\Basic;

class TreeNavigator extends Navigator
{

    protected $SubItemsTemplate;
    protected $SubItemTemplate;
    protected $AllItems;
    public $EventName;
    public $Title;

    public function __construct($_name, $_parent)
    {
        parent::__construct($_name, $_parent);

        $this->AllItems=new Lists\BasicList($this);

        $this->Style->Overflow='';
    }

    public function Add($_item_text, $_class='', $_type='item', $_object='', $_additional_info=array(), TreeNavigatorItem $_parent_item=null)
    {

        $item=new TreeNavigatorItem($this, $_item_text, $_class, $_type, $_object, $_additional_info, $this->AllItems->Count());

        if(!is_null($_parent_item))
        {
            $_parent_item->AddItem($item);
        }
        else
        {
            $this->Items->Add($item);
        }
        $this->AllItems->Add($item);

        \Mayral\Classes\Basic\ObjectsPool::Create()->AddToChangedComponents($this);
        return $item;
    }

    protected function LoadTemplate($_type)
    {
        $result=parent::LoadTemplate($_type);

        $tpl=new Basic\String($this, $result);

        $this->SubItemsTemplate=$tpl->CutTextBetween('<!--subitem_start-->', '<!--subitem_end-->');

        $subitem=new Basic\String($this, $this->SubItemsTemplate);
        $this->SubItemTemplate=$subitem->CutTextBetween('<!--subitem_item_start-->', '<!--subitem_item_end-->');
        $this->SubItemsTemplate=$subitem->Text;

        return $tpl->Text;
    }

    protected function BeforeGenerate()
    {
        parent::BeforeGenerate();
        //	формируем массив прегенерации
        $this->PreGenerateVar['Title']=$this->Title;
    }

    protected function GenerateItem($_index)
    {
        $result='';

        $Item=$this->Items->Item($_index)->Property;
        $ItemTemplate=$this->GetItemTemplate($_index);
        $ItemTemplate=str_replace('{INDEX}', $this->Items->Item($_index)->Index, $ItemTemplate);
        foreach($Item as $name=> $value)
        {
            $scripted_name='{'.$name.'}';
            $ItemTemplate=str_replace($scripted_name, $this->PrepareItemValue($_index, $name, $value), $ItemTemplate);
            $ItemTemplate=str_replace('{EventName}', $this->EventName, $ItemTemplate);
        }

        $result=$ItemTemplate;

        $subitems=$this->Items->Item($_index)->GetItems();
        $subitems_str='';
        if($subitems->Count()>0)
        {
            for($i=0; $i<$subitems->Count(); $i++)
            {
                $Item=$subitems->Item($i)->Property;
                $ItemTemplate=$this->SubItemTemplate;
                $ItemTemplate=str_replace('{INDEX}', $subitems->Item($i)->Index, $ItemTemplate);
                foreach($Item as $name=> $value)
                {
                    $scripted_name='{'.$name.'}';
                    $ItemTemplate=str_replace($scripted_name, $this->PrepareItemValue($_index, $name, $value), $ItemTemplate);
                }
                $subitems_str.=str_replace('{EventName}', $this->EventName, $ItemTemplate);
            }
            $subitems_str=str_replace('{Items}', $subitems_str, $this->SubItemsTemplate);
        }
        $result=str_replace('{SubItems}', $subitems_str, $result);

        return $result;
    }

    public function GetItem($_index)
    {
        $result=null;

        $result=$this->AllItems->Item($_index);

        return $result;
    }

    public function IsExist($_item_text)
    {
        $result=false;
        $cnt=$this->AllItems->Count();
        for($i=0; $i<$cnt; $i++)
        {
            $item=$this->AllItems->Item($i);
            if($item->PropertyValue('TEXT')==$_item_text)
            {
                $result=$i;
                break;
            }
        }
        return $result;
    }

    public function RemoveItem($_index)
    {
        $this->Items->Del($_index);
        $this->AllItems->Del($_index);
        \Mayral\Classes\Basic\ObjectsPool::Create()->AddToChangedComponents($this);
    }

}

?>