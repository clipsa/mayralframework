<?php

/* * **************************************************************************
  Description: эл-т навигатора с детьми
  Author: Zinchenko Sergey
  Created: 22.01.2013
  Version: 1.0.0

  Changes info:
  = 22.01.2013	(Zinchenko Sergey):	создан
 * ************************************************************************** */

namespace Project\DB\Components;

use Mayral\Classes\Lists;

class TreeNavigatorItem extends NavigatorItem
{

    protected $Items;
    public $Index;

    public function __construct($_parent, $_text, $_class, $_type, $_object, $_additional_info=array(), $_index)
    {
        parent::__construct($_parent, $_text, $_class, $_type, $_object, $_additional_info);
        $this->Items=new Lists\BasicList($this);
        $this->Index=$_index;
    }

    public function AddItem(TreeNavigatorItem $_item)
    {
        $this->Items->Add($_item);
    }

    public function GetItems()
    {
        return $this->Items;
    }

    public function Equal(TreeNavigatorItem $_compare_item)
    {
        $result=false;

        if($this->Property==$_compare_item->Property)
        {
            $result=true;
        }

        return $result;
    }

}

?>