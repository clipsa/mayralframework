<?php

/* * **************************************************************************
  Description: текущий пользователь системы
  Author: Ivanov Kirill
  Version: 1.0.0

  Changes info:
  = 04.06.2009	(Ivanov Kirill):	создан
 * ************************************************************************** */

namespace Project\DB\DBClass;

use Mayral\Classes\DB;

class CurrentUser extends DB\DBObject
{

    protected $GroupOptions;
    protected $UserGroups;

    public function __construct($_name, $_parent, $_connection)
    {
        parent::__construct($_name, $_parent, $_connection);

        $this->SelectQueryAlias='Select_User';
        $this->GroupOptions=new \Mayral\Classes\Lists\AssociationList($this);
        $this->UserGroups=array();
    }

    public function Read($ID)
    {
        parent::Read($ID);
        
        $ds=new DB\DBDataSet('', $this, \Mayral\Classes\DB\SQLConnection::GetInstance());
        if($ds->Open(\Project\Factory::GetInstance()->GetQueryCollection()->QueryText('Select_UserGroup_Options', array('ID_USER'=>$ID)))&&$ds->RowCount>0)
        {
            do
            {
                $this->UserGroups[]=$ds->GetFieldValue('ID');
                $Params=explode('[ITEM_END]', $ds->GetFieldValue('OPTIONS'));
                for($i=0; $i<count($Params); $i++)
                {
                    $ParamStr=$Params[$i];
                    $Param=explode('[VALUE]', $ParamStr);

                    $current_value=$this->GroupOptions->Item($Param[0]);
                    if(isset($Param[1])&&$Param[0]&&($current_value=='no'||$current_value==''))
                    {
                        $this->GroupOptions->Add($Param[1], $Param[0]);
                    }
                }
            } while($ds->NextRow());
        }
    }

    public function GroupOptionValue($_name)
    {
        return $this->GroupOptions->Item($_name);
    }

    public function GetUserGroups()
    {
        return $this->UserGroups;
    }

}

?>