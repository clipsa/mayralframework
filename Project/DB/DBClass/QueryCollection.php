<?php

/* * **************************************************************************
  Description: коллекция запросов
  Author: Ivanov Kirill
  Version: 1.0.0

  Changes info:
  = 22.06.2009	(Ivanov Kirill):	создан
 * ************************************************************************** */
namespace Project\DB\DBClass;

class QueryCollection extends \Mayral\Classes\DB\QueryCollection
{

    public function __construct()
    {
        parent::__construct();
        //	все подряд
        $this->AddQuery("Select_MaxID", "SELECT MAX(ID) AS RESULT FROM {table_name}");


        //	группы пользователей
        $this->AddQuery("Select_All_UserGroup", "SELECT * FROM table_user_group");
        $this->AddQuery("Select_UserGroup", "SELECT * FROM table_user_group WHERE ID={ID}");
        $this->AddQuery("Update_UserGroup", "UPDATE table_user_group SET NAME='{NAME}', OPTIONS='{OPTIONS}', ID_USER_CHIEF={ID_USER_CHIEF} WHERE ID={ID}");
        $this->AddQuery("Insert_UserGroup", "INSERT INTO table_user_group (NAME, OPTIONS, ID_USER_CHIEF) VALUES ('{NAME}', '{OPTIONS}', {ID_USER_CHIEF})");
        $this->AddQuery("Delete_UserGroup", "DELETE FROM table_user_group WHERE ID={ID}");

        //	пользователи
        $this->GenerateStandartQuerys("table_user", "NAME,EMAIL,ULOGIN,UPASSWORD");
        $this->AddQuery("Select_All_User", "SELECT table_user.*, (SELECT CONCAT_WS(', ', table_user_group.NAME) FROM table_user_user_group_link LEFT JOIN table_user_group ON table_user_group.ID=table_user_user_group_link.ID_USER_GROUP WHERE ID_USER=table_user.ID GROUP BY table_user_user_group_link.ID_USER) AS USER_GROUP FROM table_user");
        $this->AddQuery("Select_User", "SELECT table_user.* FROM table_user WHERE ID={ID}");
        $this->AddQuery("Login_User", "SELECT ID FROM table_user WHERE ULOGIN='{ULOGIN}' AND UPASSWORD=MD5('{UPASSWORD}')");
        $this->AddQuery("Update_User", "UPDATE table_user SET NAME='{NAME}', EMAIL='{EMAIL}', ULOGIN='{ULOGIN}', UPASSWORD='{UPASSWORD}' WHERE ID={ID}");
        $this->AddQuery("Insert_User", "INSERT INTO table_user (NAME, EMAIL, ULOGIN, UPASSWORD) VALUES ('{NAME}', '{EMAIL}', '{ULOGIN}', '{UPASSWORD}')");
        $this->AddQuery("Delete_User", "DELETE FROM table_user WHERE ID={ID}");
        $this->AddQuery('Select_UserGroup_Options', 'SELECT table_user_group.ID, table_user_group.OPTIONS FROM table_user_user_group_link LEFT JOIN table_user_group ON table_user_group.ID=table_user_user_group_link.ID_USER_GROUP WHERE ID_USER={ID_USER}');
        $this->AddQuery('Select_UserGroups', 'SELECT table_user_group.*, IF(table_user_user_group_link.ID IS NULL, 0, 1) AS CHECKED FROM table_user_group LEFT JOIN table_user_user_group_link ON table_user_user_group_link.ID_USER_GROUP=table_user_group.ID AND table_user_user_group_link.ID_USER={ID_USER}');

        $this->GenerateStandartQuerys('table_user_user_group_link', 'ID_USER, ID_USER_GROUP');
        $this->AddQuery('Delete_All_table_user_user_group_link', 'DELETE FROM table_user_user_group_link WHERE ID_USER={ID_USER}');
        $this->AddQuery('Select_All_UsersInGroup', 'SELECT * FROM table_user WHERE ID IN (SELECT ID_USER FROM table_user_user_group_link WHERE ID_USER_GROUP IN ({ID_USER_GROUP}))');
    }

    public function GenerateStandartQuerys($_table_name, $_field_list, $_postfix='')
    {
        $Fields=array();
        $Fields=explode(",", $_field_list);
        $FieldList="";
        $ValueList="";
        $UpdateList="";
        for($i=0; $i<count($Fields); $i++)
        {
            $FieldInfo=explode("=", trim($Fields[$i]));
            $FieldName=$FieldInfo[0];
            $this->AddQuery("Select_".$_table_name.$_postfix."_".$FieldName, "SELECT $FieldName AS RESULT FROM $_table_name WHERE ID={ID}");
            $this->AddQuery("Update_".$_table_name.$_postfix."_".$FieldName, "UPDATE $_table_name SET $FieldName='{".$FieldName."}' WHERE ID={ID}");
            $FieldValue="'{".$FieldName."}'";
            if(isset($FieldInfo[1]))
            {
                $FieldValue=$FieldInfo[1];
            }
            if($FieldList!="")
            {
                $FieldList .= ", ";
                $ValueList .= ", ";
                $UpdateList .= ", ";
            }
            $FieldList .= $FieldName;
            $ValueList .= $FieldValue;
            $UpdateList .= $FieldName."=".$FieldValue;
        }

        $this->AddQuery("Select_All_".$_table_name.$_postfix, "SELECT * FROM ".$_table_name/* ." {ORDER_BY}" */);
        $this->AddQuery("Select_".$_table_name.$_postfix, "SELECT * FROM ".$_table_name." WHERE ID={ID}");
        $this->AddQuery("Update_".$_table_name.$_postfix, "UPDATE ".$_table_name." SET ".$UpdateList." WHERE ID={ID}");
        $this->AddQuery("Insert_".$_table_name.$_postfix, "INSERT INTO ".$_table_name." (".$FieldList.") VALUES (".$ValueList.")");
        $this->AddQuery("Delete_".$_table_name.$_postfix, "DELETE FROM ".$_table_name." WHERE ID={ID}");
    }

}

?>