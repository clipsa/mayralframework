<?php

/* * **************************************************************************
  Description: пользователь
  Author: Ivanov Kirill
  Version: 1.0.0

  Changes info:
  = 04.06.2009	(Ivanov Kirill):	создан
 * ************************************************************************** */
namespace Project\DB\DBClass;

class User extends \Mayral\Classes\DB\DBObject
{

    public function __construct($_name, $_parent, $_connection)
    {
        parent::__construct($_name, $_parent, $_connection);

        $this->SelectQueryAlias = 'Select_User';
        $this->UpdateQueryAlias = 'Update_User';
        $this->InsertQueryAlias = 'Insert_User';
    }

}

?>