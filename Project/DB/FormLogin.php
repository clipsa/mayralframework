<?php

/* * **************************************************************************
  Description: форма авторизации
  Author: Ivanov Kirill
  Version: 1.0.0

  Changes info:
  = 22.05.2009	(Ivanov Kirill):	создан
 * ************************************************************************** */

namespace Project\DB;

class FormLogin extends \Mayral\Classes\VisualComponents\Form
{

    public function __construct($_name, $_parent)
    {
        parent::__construct($_name, $_parent);
        $this->CreateFromXML('Project/DB/Layout/LayoutFormLogin.xml');
        $this->CloseButton->Visible=false;

        $this->EditPassword->javascript_OnKeyPress="if(event.keyCode==13) ".$this->ButtonLogin->FullName().".onclick();";
    }

    public function ButtonLogin_OnClick($_sender, $_event, $_event_args=null)
    {
        $Login=$this->EditLogin->Value;
        $Password=$this->EditPassword->Value;
        $this->EditLogin->Value="";
        $this->EditPassword->Value="";
        if(!$this->Parent->Login($Login, $Password))
        {
            \Mayral\Classes\Basic\ObjectsPool::Create()->AddToChangedComponents($this->EditLogin);
            \Mayral\Classes\Basic\ObjectsPool::Create()->AddToChangedComponents($this->EditPassword);
        }
    }

}

?>