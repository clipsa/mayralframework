<?php

/* * **************************************************************************
  Description: главная форма
  Author: Ivanov Kirill
  Version: 1.0.0

  Changes info:
  = 26.05.2009	(Ivanov Kirill):	создан
 * ************************************************************************** */

namespace Project\DB;

class FormMain extends BasicFormMain
{

    public function __construct($_name, $_parent)
    {
        parent::__construct($_name, $_parent);
        $this->CreateFromXML('Project/DB/Layout/LayoutFormMain.xml');

        $this->FormLogin->Show();
    }

    public function Login($_login, $_password)
    {
        \Mayral\Classes\DB\SQLConnection::GetInstance()->sql_connect();

        $Ds=new \Mayral\Classes\DB\DBDataSet('', $this, \Mayral\Classes\DB\SQLConnection::GetInstance());
        $Ds->Open(\Project\Factory::GetInstance()->GetQueryCollection()->QueryText('Login_User', array('ULOGIN'=>$_login, 'UPASSWORD'=>$_password)));
        $UserExist=($Ds->RowCount>0);
        if($UserExist)
        {
            $this->UserInfo->Read($Ds->GetFieldValue('ID'));
            $this->FormLogin->Hide();
            $this->Navigator->Visible=true;
            $this->NavigatorTop->Visible=true;
            $this->PanelMain->Visible=true;

            $this->NavigatorTop->Add('', 'icon-exit', 'static_item', 'logout', array('TopItemClass'=>'place-right'));
            $this->NavigatorTop->Add('Пользователь: '.$this->UserInfo->PropertyValue('NAME'), 'icon-user', 'static_item', null, array('TopItemClass'=>'place-right'));

            $this->Fill_Navigator();
            $this->SendGlobalEvent('UserLogin');
            $this->SendGlobalEvent('ShowToast', array('caption'=>'Здравствуйте', 'content'=>'Mayral рад Вас видеть :)'));

            \Mayral\Classes\Basic\ObjectsPool::Create()->AddToChangedComponents($this);
        }
        $Ds->Close();
        return $UserExist;
    }

    public function ActionPerform($_event_name, $_event_argument)
    {
        $result=parent::ActionPerform($_event_name, $_event_argument);

        if($_event_name=='OnTopNavigatorClick')
        {
            if($_event_argument=='logout')
            {
                $this->Logout();
            }
        }

        return $result;
    }

}

?>