<?php

/* * **************************************************************************
  Description: форма списка данных
  Author: Ivanov Kirill
  Version: 1.0.2

  Changes info:
  = 25.09.2009	(Ivanov Kirill):	создан
  = 14.10.2009	(Ivanov Kirill):	protected function FillDataList() (был public)
  = 15.08.2012  (Zinchenko Sergey): переход на глобальные события
 * ************************************************************************** */

namespace Project\DB\Forms;

class FormData extends \Mayral\Classes\VisualComponents\Form implements \Mayral\Classes\Interfaces\IActionListener
{

    public $RecordIcon='Project/DB/Images/record_2.png';
    public $DataAlias;
    public $ViewFormAlias;
    protected $SelectQueryAlias;
    protected $DeleteQueryAlias;

    public function __construct($_name, $_parent)
    {
        parent::__construct($_name, $_parent);
        $this->CreateFromXML('Project/DB/Layout/LayoutFormData.xml');
    }

    public function Work()
    {
        $this->FillDataList();
        $this->Show();

        return $this;
    }

    protected function FillDataList()
    {
        
    }

    public function ButtonRefresh_OnClick($_sender, $_event, $_event_args=null)
    {
        $this->FillDataList();
    }

    protected function CustomEvent($_event_name, $_event_args='')
    {
        $Result='';
        if($_event_name=='on_edit_click')
        {
            $Result='Event_on_edit_click';
        }
        if($_event_name=='on_delete_click')
        {
            $Result='Event_on_delete_click';
        }
        return $Result;
    }

    protected function GetSelectedItemID($_event_args)
    {
        return -1;
    }

    public function Event_on_delete_click($_sender, $_event, $_event_args=null)
    {
        $ItemID=$this->GetSelectedItemID($_event_args);
        if($ItemID!=-1)
        {
            $f=new \Mayral\Classes\VisualComponents\MessageBox\MessageDialog('MessageDialog', $this);
            $f->Work('Вы действительно хотите удалить эту запись?', 'ParseMessageDialogResult', $ItemID);
        }
    }

    public function ParseMessageDialogResult($_result, $_argument)
    {
        if($_result==='yes')
        {
            \Mayral\Classes\DB\SQLConnection::GetInstance()->sql_query(\Project\Factory::GetInstance()->GetQueryCollection()->QueryText($this->DeleteQueryAlias, array('ID'=>$_argument)));
            $this->SendGlobalEvent('database_data_changed', $this->DataAlias);
        }
    }

    protected function Event_on_edit_click($_sender, $_event, $_event_args=null)
    {
        $ItemID=$this->GetSelectedItemID($_event_args);
        if($ItemID!=-1)
        {
            $this->SendGlobalEvent('AddToFormController', $this->SetViewArguments(array('name'=>$this->ViewFormAlias, 'show'=>'view_record_form', 'record_id'=>$ItemID, 'event_name'=>$_event)));
        }
    }

    public function ButtonAddRecord_OnClick($_sender, $_event, $_event_args=null)
    {
        $this->SendGlobalEvent('AddToFormController', $this->SetViewArguments(array('name'=>$this->ViewFormAlias, 'show'=>'view_record_form', 'record_id'=>-1)));
    }

    protected function SetViewArguments($_args)
    {
        return $_args;
    }

    public function ActionPerform($_event_name, $_event_argument)
    {
        if($_event_name==='database_data_changed'&&$_event_argument===$this->DataAlias)
        {
            $this->FillDataList();
        }
    }

}

?>