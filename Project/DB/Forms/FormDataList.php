<?php

/* * **************************************************************************
  Description: форма списка данных (abstract)
  Author: Zinchenko Sergey (на основе FormDataListView от Ivanov Kirill)
  Version: 1.0.2

  Changes info:
  = 16.07.2011    (Zinchenko Sergey):	создан
  + 30.11.2011    (Efimov Evgenij): PrepareItem(), вызываемая перед выводом данных в список
  = 07.09.2012    (Zinchenko Sergey): переделан постраничный вывод
 * ************************************************************************** */

namespace Project\DB\Forms;

use Mayral\Classes\DB;

class FormDataList extends FormData
{

    protected $SortingField='';
    protected $Up=false;

    //	возвращает строку запроса для DataList
    protected function GetDataListQuery()
    {
        return \Project\Factory::GetInstance()->GetQueryCollection()->QueryText($this->SelectQueryAlias, array('ORDER_BY'=>$this->Sort()));
    }

    protected function PrepareItem(&$_item)
    {
        
    }

    protected function GetSelectedItemID($_event_args)
    {
        $Result=-1;
        $Item=$this->DataList->Items->Item($_event_args);
        if($Item!=="")
        {
            $Result=$Item->PropertyValue("ID");
        }
        return $Result;
    }

    //	заполняет DataList
    protected function FillDataList()
    {
        $this->DataList->Items->Clear();
        $ds=new DB\DBDataSet("", $this, DB\SQLConnection::GetInstance());

        $query=$this->GetDataListQuery();
        $total_rows=$this->CountQueryItems($query);

        $ds->StartRowIndex=$this->DataList->ItemsPerPage*($this->DataList->CurrentPage-1);
        $ds->RowsInResult=$this->DataList->ItemsPerPage;
        $ds->Open($query);
        $this->DataList->TotalRowsCount=$total_rows;

        for($i=0; $i<min($this->DataList->ItemsPerPage, $total_rows-$ds->StartRowIndex); $i++)
        {
            $ds->SetRow($i);
            $Item=new DB\DBObject("", $this, "MainDBConnection");
            $Item->ReadFromDataSet($ds);
            $Item->SetPropertyValue('RECORD_IMAGE', $this->RecordIcon);
            $this->PrepareItem($Item);
            $this->DataList->Items->Add($Item);
        }
        \Mayral\Classes\Basic\ObjectsPool::Create()->AddToChangedComponents($this->DataList);
    }

    protected function CountQueryItems($_query)
    {
        $query_result=DB\SQLConnection::GetInstance()->sql_query($_query);
        return DB\SQLConnection::GetInstance()->sql_num_rows($query_result);
    }

    protected function ChangePage($_sender, $_event, $_event_args=null)
    {
        if($_event_args!==null)
        {
            $this->DataList->CurrentPage=$_event_args;
        }
        $this->FillDataList();
    }

    public function ChangeSortUp($_sender, $_event, $_event_args=null)
    {
        $this->SortingField=$this->DataList->GetColumnById($_event_args);
        $this->Up=true;
        $this->FillDataList();
    }

    public function ChangeSortDown($_sender, $_event, $_event_args=null)
    {
        $this->SortingField=$this->DataList->GetColumnById($_event_args);
        $this->Up=false;
        $this->FillDataList();
    }

    protected function Sort()
    {
        $order_by=($this->SortingField==''?'':'ORDER BY '.$this->SortingField);
        if($order_by!='')
        {
            $order_by.=($this->Up?'':' DESC');
        }
        return $order_by;
    }

}

?>