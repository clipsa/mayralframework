<?php

/* * **************************************************************************
  Description: форма списка данных
  Author: Ivanov Kirill
  Version: 1.0.2

  Changes info:
  = 15.06.2009	(Ivanov Kirill):	создан
  = 14.10.2009	(Ivanov Kirill):	protected function FillDataList() (был public)
  + 14.10.2009	(Ivanov Kirill):	GetDataListQuery
  = 16.07.2011	(Zinchenko Sergey):	унаследован от FormDataList
 * ************************************************************************** */

namespace Project\DB\Forms;

class FormDataListView extends FormDataList
{

    public function __construct($_name, $_parent, $_item_template_file_name="pattern/db/template/FormDataListView_DataList.html")
    {
        parent::__construct($_name, $_parent);
        $this->CreateFromXML('Project/DB/Layout/LayoutFormDataListView.xml');
        $this->DataList->SetTemplateFileName($_item_template_file_name);
    }

}

?>