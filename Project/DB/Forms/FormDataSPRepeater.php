<?php

/* * **************************************************************************
  Description: форма списка
  Author: Ivanov Kirill
  Version: 1.0.0

  Changes info:
  = 07.12.2009	(Ivanov Kirill):	создан
 * ************************************************************************** */

namespace Project\DB\Forms;

use Mayral\Classes\DB;

class FormDataSPRepeater extends FormData
{

    public function __construct($_name, $_parent)
    {
        parent::__construct($_name, $_parent);
        $this->CreateFromXML("Project/DB/Layout/LayoutFormDataSPRepeater.xml");
    }

    //	заполняет DataList
    protected function FillDataList()
    {
        $this->DataList->Items->Clear();
        $ds=new DB\DBDataSet("", $this, \Mayral\Classes\DB\SQLConnection::GetInstance()->GetInstance("MainDBConnection"));
        $ds->Open($this->GetDataListQuery());
        for($i=0; $i<$ds->RowCount; $i++)
        {
            $ds->SetRow($i);
            $Item=new DB\DBObject("", $this, "MainDBConnection");
            $Item->ReadFromDataSet($ds);
            $this->DataList->Items->Add($Item);
        }
        \Mayral\Classes\Basic\ObjectsPool::Create()->AddToChangedComponents($this->DataList);
    }

    //	возвращает строку запроса для DataList
    protected function GetDataListQuery()
    {
        return \Project\Factory::GetInstance()->GetQueryCollection()->QueryText($this->SelectQueryAlias);
    }

    protected function GetSelectedItemID($_event_args)
    {
        $Result=-1;
        $Item=$this->DataList->Items->Item($_event_args);
        if($Item!=="")
        {
            $Result=$Item->PropertyValue("ID");
        }
        return $Result;
    }

}

?>