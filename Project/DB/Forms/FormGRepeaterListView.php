<?php

/* * **************************************************************************
  Description: форма списка данных
  Author: Kecheor
  Version: 1.0.3

  Changes info:
  = ?			 (Kecheor): создан
  = 16.07.2011	(Zinchenko Sergey): родитель
  + 25.11.2013	(Zinchenko Sergey): копирование
 * ************************************************************************** */

namespace Project\DB\Forms;

class FormGRepeaterListView extends FormDataList
{

    public function __construct($_name, $_parent)
    {
        parent::__construct($_name, $_parent);
        $this->CreateFromXML('Project/DB/Layout/LayoutFormGRepeater.xml');
    }

    public function UpdateButtons()
    {
        $this->SendGlobalEvent('AddButtons', array(
            array('button_text' => 'Добавить', 'icon_class' => 'icon-plus', 'button_arguments' => array('form_name' => $this->Name, 'record_index' => -1, 'action_name' => 'add'))
                )
        );
    }

    protected function Event_row_OnClick($_sender, $_event_name, $_args = null)
    {
        $this->SendGlobalEvent('AddButtons', $this->PrepareButtons($_args));
    }

    protected function PrepareButtons($_selected_record)
    {
        return array(
            array('button_text' => 'Редактировать', 'icon_class' => 'icon-pencil', 'button_arguments' => array('form_name' => $this->Name, 'record_index' => intval($_selected_record), 'action_name' => 'edit')),
            array('button_text' => 'Удалить', 'icon_class' => 'icon-remove', 'button_arguments' => array('form_name' => $this->Name, 'record_index' => intval($_selected_record), 'action_name' => 'remove')),
            array('button_text' => 'Копировать', 'icon_class' => 'icon-copy', 'button_arguments' => array('form_name' => $this->Name, 'action_name' => 'copy'))
        );
    }

    public function Event_on_delete_click($_sender, $_event, $_event_args = null)
    {
        if (is_array($_event_args))
        {
            $f = new \Mayral\Classes\VisualComponents\MessageBox\MessageDialog('MessageDialog', $this);
            $f->Work('Вы действительно хотите удалить эти записи?', 'ParseMessageDialogResult', $_event_args);
        }
        else
        {
            parent::Event_on_delete_click($_sender, $_event, $_event_args);
        }
    }

    public function ParseMessageDialogResult($_result, $_argument)
    {
        if (is_array($_argument))
        {
            if ($_result === 'yes')
            {
                foreach ($_argument as $key => $val)
                {
                    \Mayral\Classes\DB\SQLConnection::GetInstance()->sql_query(\Project\Factory::GetInstance()->GetQueryCollection()->QueryText($this->DeleteQueryAlias, array('ID' => $this->GetSelectedItemID($val))));
                }
                $this->SendGlobalEvent('database_data_changed', $this->DataAlias);
            }
        }
        else
        {
            parent::ParseMessageDialogResult($_result, $_argument);
        }
    }

    public function Work()
    {
        parent::Work();

        $this->UpdateButtons();

        return $this;
    }

    public function Close()
    {
        parent::Close();
        $this->SendGlobalEvent('RemoveButtons', array('form_name' => $this->Name));
    }

    public function ActionPerform($_event_name, $_event_argument)
    {
        $result = parent::ActionPerform($_event_name, $_event_argument);

        if ($_event_name === 'OnTopNavigatorClick' && isset($_event_argument['form_name']) && $_event_argument['form_name'] === $this->Name)
        {
            switch ($_event_argument['action_name'])
            {
                case 'add':
                    {
                        $this->ButtonAddRecord_OnClick($this, '', null);
                    }break;
                case 'copy':
                case 'edit':
                    {
                        $record_index = $this->DataList->Value;
                        if (is_array($record_index))
                        {
                            $record_index = $record_index[0];
                        }
                        $this->Event_on_edit_click($this, $_event_argument['action_name'], $record_index);
                    }break;
                case 'remove':
                    {
                        $this->Event_on_delete_click($this, '', $this->DataList->Value);
                    }break;
            }
        }
        return $result;
    }

    protected function CopyRecord($_index)
    {
        $ItemID = $this->GetSelectedItemID($_event_args);
        if ($ItemID != -1)
        {
            $this->SendGlobalEvent('AddToFormController', $this->SetViewArguments(array('name' => $this->ViewFormAlias, 'show' => 'view_record_form', 'record_id' => $ItemID)));
        }
    }

    function __set($prop_name, $prop_value)
    {
        $result = parent::__set($prop_name, $prop_value);
        if ($prop_name == 'Visible' && ($prop_value === true || $prop_value == 1))
        {
            $this->UpdateButtons();
        }

        return $result;
    }

}

?>