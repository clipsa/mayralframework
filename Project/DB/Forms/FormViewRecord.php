<?php

/* * **************************************************************************
  Description: прототип формы редактирования записи
  Author: Ivanov Kirill
  Version: 1.1.2

  Changes info:
  = 15.06.2009	(Ivanov Kirill):	создан
  + 26.06.2009	(Ivanov Kirill):	AfterPost
  - 13.10.2009	(Ivanov Kirill):	FormDLV (заменено на глобальное событие)
  + 27.09.2009	(Ivanov Kirill):	Assigner
  + 15.01.2013	(Zinchenko Sergey): вставка пустой записи
  + 25.11.2013	(Zinchenko Sergey): Copy()
 * ************************************************************************** */

namespace Project\DB\Forms;

class FormViewRecord extends \Mayral\Classes\VisualComponents\Form implements \Mayral\Classes\Interfaces\IActionListener
{

    protected $Record;
    public $DataAlias;

    /**
     * Алиас запроса для вставки пустой записи
     *
     * @var string 
     *
     */
    public $InsertEmptyAlias = '';

    /**
     * Флаг-удалять или нет запись (если вставлена пустая, но нажата отмена)
     *
     * @var bool 
     *
     */
    protected $NeedToDelete = false;

    /**
     * для связывание записи с компонентами
     *
     * @var ORVCAssigner 
     *
     */
    protected $Assigner;

    public function __construct($_name, $_parent)
    {
        parent::__construct($_name, $_parent);
        $this->CreateFromXML('Project/DB/Layout/LayoutFormViewRecord.xml');

        $this->Assigner = new \Mayral\Classes\DB\RVCAssigner('Assigner', $this);
        $this->Assigner->Record = &$this->Record;

        $this->Draggable = true;
    }

    public function Work($_id = -1)
    {
        $this->Record->Clear();
        if ($_id !== -1)
        {
            $this->Record->Read($_id);
        }
        else
        {
            $this->InsertEmpty();
            $this->SetDefaultValues();
        }
        $this->Show();

        $this->AfterOpen();
        $this->Assigner->ToComponents();
        $this->AfterAssign();

        return $this;
    }

    protected function AfterAssign()
    {
        
    }

    public function CloseButton_OnClick($_sender, $_event, $_event_args = null)
    {
        if ($this->NeedToDelete)
        {
            $this->Record->Delete();
        }
        $this->Close();
    }

    public function ButtonCancel_OnClick($_sender, $_event, $_event_args = null)
    {
        if ($this->NeedToDelete)
        {
            $this->Record->Delete();
        }
        $this->Close();
    }

    public function ButtonOk_OnClick($_sender, $_event, $_event_args = null)
    {
        $result = false;
        if ($this->Assigner->ToRecord() && $this->BeforePost())
        {
            $this->PrepareRecordProperty();
            $this->Record->Write();
            $this->AfterPost();

            $this->SendGlobalEvent('database_data_changed', $this->DataAlias);

            $this->Close();

            $result = true;
        }
        return $result;
    }

    protected function AfterOpen()
    {
        
    }

    protected function BeforePost()
    {
        return true;
    }

    protected function AfterPost()
    {
        
    }

    protected function SetDefaultValues()
    {
        
    }

    protected function PrepareUserValue($_value)
    {
        return $_value;
    }

    protected function PrepareRecordProperty()
    {
        foreach ($this->Record->GetProperty() as $name => $value)
        {
            $this->Record->SetPropertyValue($name, $this->PrepareUserValue($value));
        }
    }

    protected function LabelInlightOn($_label)
    {
        $_label->Style->Color = 'red';
        $_label->Style->FontWeight = 'bold';
        \Mayral\Classes\Basic\ObjectsPool::Create()->AddToChangedComponents($_label);
    }

    protected function LabelInlightOff($_label)
    {
        $_label->Style->Color = '';
        $_label->Style->FontWeight = '';
        \Mayral\Classes\Basic\ObjectsPool::Create()->AddToChangedComponents($_label);
    }

    public function ActionPerform($_event_name, $_event_argument)
    {
        
    }

    /**
     * Вставка пустой записи
     *
     * @return bool Успешена вставка или нет
     *
     */
    protected function InsertEmpty()
    {
        $result = false;
        if ($this->InsertEmptyAlias != '')
        {
            $result = \Mayral\Classes\DB\SQLConnection::GetInstance()->sql_query($this->PrepareInsertEmptyQuery()) !== false;
            if ($result)
            {
                $this->Record->Read(\Mayral\Classes\DB\SQLConnection::GetInstance()->sql_insert_id());
                $this->NeedToDelete = true;
            }
        }
        return $result;
    }

    /**
     * Подготовка запроса для вставки пустой записи
     *
     * @return string готовый запрос
     *
     */
    protected function PrepareInsertEmptyQuery()
    {
        $result = \Project\Factory::GetInstance()->GetQueryCollection()->QueryText($this->InsertEmptyAlias);

        return $result;
    }

//    public function Close()
//    {
//        parent::Close();
//        $this->SendGlobalEvent('FormIsClosed', $this->Name);
//    }

    public function Copy()
    {
        $this->Record->Copy();
    }

}

?>