<?php

/* * **************************************************************************
  Description: форма списка пользователей
  Author: Ivanov Kirill
  Version: 1.0.0

  Changes info:
  = 15.06.2009	(Ivanov Kirill):	создан
 * ************************************************************************** */
namespace Project\DB\Forms\User;

class FormDLV_User extends \Project\DB\Forms\FormGRepeaterListView
{

    public function __construct($_name, $_parent)
    {
        parent::__construct($_name, $_parent);
        $this->DataAlias='table_user';

        $this->ViewFormAlias='table_user_view';

        $this->SelectQueryAlias='Select_All_User';
        $this->DeleteQueryAlias='Delete_User';
        $this->CreateFromXML('Project/DB/Layout/LayoutFormDLV_User.xml');
        $this->IconSrc='Images/icons/user.png';

        $this->DataList->AddColumn(new \Mayral\Classes\VisualComponents\RepeaterColumn('Название', 'NAME', true));
    }

    public function FillDataList()
    {
        parent::FillDataList();
        $Items=$this->DataList->Items;
        for($i=0; $i<$Items->Count(); $i++)
        {
            $Item=$Items->Item($i);
            $Item->SetPropertyValue('RECORD_IMAGE', 'Project/DB/Images/user3.png');
            if($Item->PropertyValue('ID_USER_GROUP')==1)
            {
                $Item->SetPropertyValue('RECORD_IMAGE', 'Project/DB/Images/user_admin2.png');
            }
        }
    }

}

?>