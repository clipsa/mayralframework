<?php

/* * **************************************************************************
  Description: форма редактирования пользователя
  Author: Ivanov Kirill
  Version: 1.0.0

  Changes info:
  = 15.06.2009	(Ivanov Kirill):	создан
 * ************************************************************************** */

namespace Project\DB\Forms\User;

use Mayral\Classes\DB;

class FormView_User extends \Project\DB\Forms\FormViewRecord
{

    protected $UserClientsIds; //Айдишники клиентов, привязанных к юзеру, через запятую

    public function __construct($_name, $_parent)
    {
        parent::__construct($_name, $_parent);
        $this->DataAlias='table_user';
        $this->CreateFromXML('Project/DB/Layout/LayoutFormView_User.xml');
        $this->IconSrc='Images/icons/user.png';

        $this->Record=new \Project\DB\DBClass\User('', $this, 'MainDBConnection');
    }

    protected function AfterOpen()
    {
        $this->Edit_NAME->Value=$this->Record->PropertyValue('NAME');
        $this->Edit_ULOGIN->Value=$this->Record->PropertyValue('ULOGIN');
        $this->Edit_UPASSWORD->Value='';
        $this->Edit_EMAIL->Value=$this->Record->PropertyValue('EMAIL');

        $this->FillUserGroups();
    }

    protected function BeforePost()
    {
//        if($this->Edit_UPASSWORD->Value===''&&$this->Record->PropertyValue('UPASSWORD')=='')
//        {
//            $this->LabelPassword->Style->Color='red';
//            $this->LabelPassword->Style->FontWeight='bold';
//            \Mayral\Classes\Basic\ObjectsPool::Create()->AddToChangedComponents($this->LabelPassword);
//            return false;
//        }
        $this->Record->SetPropertyValue('NAME', $this->Edit_NAME->Value);
        $this->Record->SetPropertyValue('ULOGIN', $this->Edit_ULOGIN->Value);
        if($this->Edit_UPASSWORD->Value!=='')
        {
            $this->Record->SetPropertyValue('UPASSWORD', md5($this->Edit_UPASSWORD->Value));
        }
        $this->Record->SetPropertyValue('EMAIL', $this->Edit_EMAIL->Value);

        return true;
    }

    protected function AfterPost()
    {
        parent::AfterPost();

        $this->SaveUserGroups();
    }

    protected function FillUserGroups()
    {
        $ds=new DB\DBDataSet("", $this, DB\SQLConnection::GetInstance());
        if($ds->Open(\Project\Factory::GetInstance()->GetQueryCollection()->QueryText('Select_UserGroups', array('ID_USER'=>$this->Record->Id())))&&$ds->RowCount>0)
        {
            do
            {
                $this->CLB_USER_GROUPS->Add($ds->GetFieldValue('NAME'), $ds->GetFieldValue('CHECKED')==1, $ds->GetFieldValue('ID'));
            } while($ds->NextRow());
        }
    }

    protected function SaveUserGroups()
    {
        $checked=$this->CLB_USER_GROUPS->GetCheckedObjects();
        DB\SQLConnection::GetInstance()->sql_query(\Project\Factory::GetInstance()->GetQueryCollection()->QueryText('Delete_All_table_user_user_group_link', array('ID_USER'=>$this->Record->Id())));
        for($i=0; $i<count($checked); $i++)
        {
            DB\SQLConnection::GetInstance()->sql_query(\Project\Factory::GetInstance()->GetQueryCollection()->QueryText('Insert_table_user_user_group_link', array('ID_USER'=>$this->Record->Id(), 'ID_USER_GROUP'=>$checked[$i])));
        }
    }

}

?>