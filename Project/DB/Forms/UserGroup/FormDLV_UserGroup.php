<?php

/* * **************************************************************************
  Description: форма списка пользователей
  Author: Ivanov Kirill
  Version: 1.0.0

  Changes info:
  = 22.06.2009	(Ivanov Kirill):	создан
 * ************************************************************************** */
namespace Project\DB\Forms\UserGroup;

class FormDLV_UserGroup extends \Project\DB\Forms\FormGRepeaterListView
{

    public function __construct($_name, $_parent)
    {
        parent::__construct($_name, $_parent);
        $this->DataAlias="table_user_group";
        $this->Title="Группы пользователей";
        $this->IconSrc="Images/icons/users.png";

        $this->ViewFormAlias="table_user_group_view";

        $this->SelectQueryAlias="Select_All_UserGroup";
        $this->DeleteQueryAlias="Delete_UserGroup";

        $this->DataList->AddColumn(new \Mayral\Classes\VisualComponents\RepeaterColumn('Название', 'NAME', true));
    }

}

?>