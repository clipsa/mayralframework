<?php

/* * **************************************************************************
  Description: форма редактирования групп пользователей
  Author: Ivanov Kirill
  Version: 1.0.0

  Changes info:
  = 22.06.2009	(Ivanov Kirill):	создан
 * ************************************************************************** */
namespace Project\DB\Forms\UserGroup;

class FormView_UserGroup extends \Project\DB\Forms\FormViewRecord
{

    public function __construct($_name, $_parent)
    {
        parent::__construct($_name, $_parent);
        $this->DataAlias='table_user_group';
        $this->CreateFromXML('Project/DB/Layout/LayoutFormView_UserGroup.xml');

        $this->Record=new \Mayral\Classes\DB\DBObject('', $this, 'MainDBConnection');
        $this->Record->SelectQueryAlias='Select_UserGroup';
        $this->Record->UpdateQueryAlias='Update_UserGroup';
        $this->Record->InsertQueryAlias='Insert_UserGroup';
    }

    protected function AfterOpen()
    {
        $this->Edit_NAME->Value=$this->Record->PropertyValue('NAME');
        $Options=new \Mayral\Classes\Lists\AssociationList($this);
        $Options->ReadFromString($this->Record->PropertyValue('OPTIONS'));
        $this->DLB_ID_USER_CHIEF->Value=$this->Record->PropertyValue('ID_USER_CHIEF');

        $GroupOption=\Project\Factory::GetInstance()->GetWorkSpace()->MainForm->UserGroupOption;
        for($i=0; $i<count($GroupOption); $i++)
        {
            $OptionName=$GroupOption[$i];
            $this->CheckListBox_Option->Add($OptionName, $Options->Item($OptionName)==='yes');
        }
    }

    protected function BeforePost()
    {
        $this->Record->SetPropertyValue('NAME', $this->Edit_NAME->Value);

        $Options=new \Mayral\Classes\Lists\AssociationList($this);
        $CheckListBox_Option=$this->CheckListBox_Option;
        for($i=0; $i<$CheckListBox_Option->Count(); $i++)
        {
            $Value='no';
            $IsChecked=$CheckListBox_Option->Checked($i);
            if($IsChecked)
            {
                $Value='yes';
            }
            $Options->Add($Value, $CheckListBox_Option->ItemText($i));
        }
        $OptionString=$Options->WriteToString();

        $this->Record->SetPropertyValue('OPTIONS', $OptionString);
        $this->Record->SetPropertyValue('ID_USER_CHIEF', $this->DLB_ID_USER_CHIEF->Value);

        return true;
    }

}

?>