<?php

/* * **************************************************************************
  Description: обработчик глобальных событий
  Version: 1.1.0

  Changes info:
  = 30.08.2011	(Ivanov Kirill):	создан
  = 08.08.2012  (Zinchenko Sergey):     немного переделано
 * ************************************************************************** */

namespace Project\DB\Listeners;

use Project\DB\Forms;

class EventListener extends \Mayral\Classes\Basic\MayralObject implements \Mayral\Classes\Interfaces\IActionListener
{

    public function ActionPerform($_event_name, $_event_argument)
    {
        $Result='';

        if($_event_name==='ShowForm'||$_event_name==='NeedForm')
        {
            $Parent=isset($_event_argument['parent'])?$_event_argument['parent']:\Project\Factory::GetInstance()->GetWorkSpace()->MainForm->PanelMain;
            $ParentForm=$Parent->GetParentForm();
            $FormName=isset($_event_argument['name'])?$_event_argument['name']:$_event_argument['form'];

            $Result=$this->BuildForm($FormName, $Parent, $ParentForm, $_event_argument);

            if($Result instanceof \Project\DB\Forms\FormViewRecord && isset($_event_argument['event_name']) && $_event_argument['event_name']=='copy')
            {
                $Result->Copy();
            }

            if($_event_name==='ShowForm' && $Result!='')
            {
                if($_event_argument['show']==='asitis')
                {
                    $Result->Show();
                }
                if($_event_argument['show']==='modal')
                {
                    $Result->SetCenterPosition();
                    $Result->ShowModal();
                }
                if($_event_argument['show']==='view_record_form')
                {
                    $Result->Work($_event_argument['record_id']);
                    if(isset($_event_argument['event_name']) && $_event_argument['event_name']=='copy')
                    {
                        $Result->Copy();
                    }
                }
                if($_event_argument['show']==='oncenter')
                {
                    $Result->SetCenterPosition();
                    $Result->Show();
                }
            }
        }

        return $Result;
    }

    protected function BuildForm($_name, $_parent, $_parent_form, $_args)
    {
        $result=null;

        if($_name=='FileExplorer')
        {
            $Result=new \Mayral\Classes\VisualComponents\ElFinderForm($_name, $_parent, $_args['global_event_name']==''?'files_selected_elfinder':$_args['global_event_name']);
            $Result->Work();
        }

        if($_name=='user_group')
        {
            $result=new Forms\UserGroup\FormDLV_UserGroup($_name, $_parent);
        }

        if($_name=='user')
        {
            $result=new Forms\User\FormDLV_User($_name, $_parent);
        }

        if($_name=='query_browser')
        {
            $result=new Forms\FormQueryBrowserContainer($_name, $_parent);
        }

        if($_name=='table_user_view')
        {
            $result=new Forms\User\FormView_User($_name, $_parent);
        }

        if($_name=='table_user_group_view')
        {
            $result=new Forms\UserGroup\FormView_UserGroup($_name, $_parent);
        }

        return $result;
    }

}

?>