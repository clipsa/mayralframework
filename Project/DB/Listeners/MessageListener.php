<?php

/* * **************************************************************************
  Description: обработчик событий связанных с сообщениями
  Version: 1.0.0

  Changes info:
  = 13.09.2012	(Zinchenko Sergey):	создан
 * ************************************************************************** */

namespace Project\DB\Listeners;

use Mayral\Classes\VisualComponents\MessageBox;

class MessageListener extends \Mayral\Classes\Basic\MayralObject implements \Mayral\Classes\Interfaces\IActionListener
{

    public function ActionPerform($_event_name, $_event_argument)
    {
        $Result = '';

        if ($_event_name === 'ShowMessage')
        {
            $f = new MessageBox\MessageWarning('MessageWarning', \Project\Factory::GetInstance()->GetWorkSpace()->MainForm);
            $f->Work($_event_argument);
        }

        if ($_event_name === 'ShowDialog')
        {
            $f = new MessageBox\MessageDialog('MessageDialog', isset($_event_argument['parent']) && $_event_argument['parent'] != '' ? $_event_argument['parent'] : \Project\Factory::GetInstance()->GetWorkSpace()->MainForm);
            $f->Work($_event_argument['message_text'], $_event_argument['callback'], $_event_argument['argument'], $_event_argument['title'], $_event_argument['global_event_name']);
        }

        return $Result;
    }

}

?>