<?php

/* * **************************************************************************
  Description: тест приложения с примерами
  Author: Ivanov Kirill
  Created: 09.07.2008
  Version: 1.0.0

  Changes info:
  = 09.07.2008	(Ivanov Kirill):	создан
 * ************************************************************************** */

namespace Project\DB;

class WorkSpace extends \Mayral\Classes\VisualComponents\WorkSpace
{

    public function __construct()
    {
        parent::__construct();
        $this->ApplicationTitle='Database Application pattern';

        $this->SetMainForm();

        $l=new Listeners\EventListener();
        $l->AddToObjectsPool();

        $ml=new Listeners\MessageListener();
        $ml->AddToObjectsPool();
    }

    protected function SetMainForm()
    {
        $this->MainForm=new FormMain('MainForm', '');
        $this->MainForm->Show();
    }

}


?>