<?php

namespace Project;

class Factory extends \Mayral\Classes\Interfaces\ADBWorkSpaceFactory
{

    private static $Instance;

    private function __construct()
    {
        $this->ProjectPath='Project/DB/';
    }

    public function CreateWorkSpace()
    {
        $result = new DB\WorkSpace();

        return $result;
    }

    public static function GetInstance()
    {
        if (is_null(self::$Instance))
        {
            self::$Instance = new Factory();
        }
        return self::$Instance;
    }

    public function CreateQueryCollection()
    {
        return new DB\DBClass\QueryCollection();
    }

}

?>