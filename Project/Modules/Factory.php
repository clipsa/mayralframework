<?php

namespace Project\Modules;

class Factory
{

    public static function LoadModules()
    {
        $files = scandir(dirname(__FILE__));
        for ($i = 0; $i < count($files); $i++)
        {
            if ($files[$i] != '.' && $files[$i] != '..' && is_dir('Project/Modules/' . $files[$i]))
            {
                $namespace = '\\Project\\Modules\\' . $files[$i] . '\\ModuleCreate';
                $namespace::Create();
            }
        }
    }

    public static function LoadModuleParams($_alias_or_id)
    {
        $result = array();
        $module_id = 0;
        if (is_numeric($_namespace_or_id))
        {
            $module_id = $_namespace_or_id;
        }
        else
        {
            $module_id = \Mayral\Classes\DB\DBDataSet::GetRESULT(\Project\Factory::GetInstance()->GetQueryCollection()->QueryText('GetModuleId', array('ALIAS' => $_alias_or_id)));
        }
        $ds = new \Mayral\Classes\DB\DBDataSet('', '', \Mayral\Classes\DB\SQLConnection::GetInstance());
        if ($module_id > 0 && $ds->Open(\Project\Factory::GetInstance()->GetQueryCollection()->QueryText('Select_All_ByModule', array('ID_MODULE' => $module_id))))
        {
            do
            {
                $result[$ds->GetFieldValue('NAME')]=$ds->GetFieldValue('PARAM_VALUE');
            }
            while ($ds->NextRow());
        }
        return $result;
    }

}

?>