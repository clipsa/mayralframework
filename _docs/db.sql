-- --------------------------------------------------------
-- Хост:                         192.168.0.222
-- Версия сервера:               5.5.23 - MySQL Community Server (GPL)
-- ОС Сервера:                   Win64
-- HeidiSQL Версия:              8.1.0.4545
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Дамп структуры для таблица nordea_db.table_user
CREATE TABLE IF NOT EXISTS `table_user` (
  `ID` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Ключ',
  `NAME` varchar(45) NOT NULL COMMENT 'Имя',
  `EMAIL` varchar(45) NOT NULL COMMENT 'E-mail',
  `ULOGIN` varchar(45) NOT NULL COMMENT 'Логин',
  `UPASSWORD` varchar(45) NOT NULL COMMENT 'Пароль',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Пользователи системы';

-- Дамп данных таблицы nordea_db.table_user: ~1 rows (приблизительно)
/*!40000 ALTER TABLE `table_user` DISABLE KEYS */;
INSERT INTO `table_user` (`ID`, `NAME`, `EMAIL`, `ULOGIN`, `UPASSWORD`) VALUES
	(1, 'Администратор', '', '1', 'c4ca4238a0b923820dcc509a6f75849b');
/*!40000 ALTER TABLE `table_user` ENABLE KEYS */;


-- Дамп структуры для таблица nordea_db.table_user_group
CREATE TABLE IF NOT EXISTS `table_user_group` (
  `ID` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Ключ',
  `NAME` varchar(45) NOT NULL COMMENT 'Название',
  `OPTIONS` text NOT NULL COMMENT 'Настройки',
  `ID_USER_CHIEF` int(11) unsigned DEFAULT NULL COMMENT 'Ключ пользователя - руководителя',
  PRIMARY KEY (`ID`),
  KEY `FK_table_user_group_table_user` (`ID_USER_CHIEF`),
  CONSTRAINT `FK_table_user_group_table_user` FOREIGN KEY (`ID_USER_CHIEF`) REFERENCES `table_user` (`ID`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Группы пользователей';

-- Дамп данных таблицы nordea_db.table_user_group: ~1 rows (приблизительно)
/*!40000 ALTER TABLE `table_user_group` DISABLE KEYS */;
INSERT INTO `table_user_group` (`ID`, `NAME`, `OPTIONS`, `ID_USER_CHIEF`) VALUES
	(1, 'Администратор', 'Администрирование[VALUE]yes[ITEM_END]Справочники[VALUE]yes[ITEM_END]События[VALUE]yes', 1);
/*!40000 ALTER TABLE `table_user_group` ENABLE KEYS */;


-- Дамп структуры для таблица nordea_db.table_user_user_group_link
CREATE TABLE IF NOT EXISTS `table_user_user_group_link` (
  `ID` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Ключ',
  `ID_USER` int(11) unsigned NOT NULL COMMENT 'Ключ пользователя',
  `ID_USER_GROUP` int(11) unsigned NOT NULL COMMENT 'Группа пользователей',
  PRIMARY KEY (`ID`),
  KEY `FK_table_user_user_group_link_table_user` (`ID_USER`),
  KEY `FK_table_user_user_group_link_table_user_group` (`ID_USER_GROUP`),
  CONSTRAINT `FK_table_user_user_group_link_table_user` FOREIGN KEY (`ID_USER`) REFERENCES `table_user` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_table_user_user_group_link_table_user_group` FOREIGN KEY (`ID_USER_GROUP`) REFERENCES `table_user_group` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Связь "пользователь - группа пользователей"';

-- Дамп данных таблицы nordea_db.table_user_user_group_link: ~1 rows (приблизительно)
/*!40000 ALTER TABLE `table_user_user_group_link` DISABLE KEYS */;
INSERT INTO `table_user_user_group_link` (`ID`, `ID_USER`, `ID_USER_GROUP`) VALUES
	(1, 1, 1);
/*!40000 ALTER TABLE `table_user_user_group_link` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
