<?php

require_once('Classes/Autoloader/Handler.php');
require_once('Classes/Autoloader/Basic.php');
require_once('Classes/Autoloader/Autoloader.php');

\Mayral\Classes\Autoloader\Autoloader::Register(new \Mayral\Classes\Autoloader\Basic());

$Debug=true;

header('Content-type: text/html; charset=utf-8');
header('Cache-Control: no-store, no-cache, must-revalidate');
header('Cache-Control: post-check=0, pre-check=0', false);

ob_start();

if($Debug)
{
    error_reporting(E_ALL);
}
else
{
    error_reporting(0);
}
if(file_exists('options.php'))
{
    require('options.php');
}

global $Options;

//настройки базы данных проекта
\Mayral\Classes\DB\SQLConnection::$Host=$Options['DBHost'];
\Mayral\Classes\DB\SQLConnection::$Login=$Options['DBUser'];
\Mayral\Classes\DB\SQLConnection::$Password=$Options['DBPassword'];
\Mayral\Classes\DB\SQLConnection::$DBName=$Options['DBName'];
\Mayral\Classes\DB\SQLConnection::$CurrentDriver=$Options['Driver'];


session_start();

//	обновление объектов
$ObjectPool=\Mayral\Classes\Basic\ObjectsPool::Create();
$WorkSpace=\Project\Factory::GetInstance()->GetWorkSpace();

$ObjectPool->Refresh();

//	вызов рабочего пространства
$result=$WorkSpace->Work();
$kill_all=$WorkSpace->KillMark;

//	удаление объектов помеченных на уничтожение
$ObjectPool->DeleteMarkedToKill();

\Mayral\Classes\DB\SQLConnection::Close();

file_put_contents('Logs/error_log.log', ob_get_clean(), FILE_APPEND);

//удаление рабочего пространства (закрытие сессии)
if($kill_all)
{
    session_destroy();
    //echo 'reload';
}
//else
//{
    echo $result;
//}
?>